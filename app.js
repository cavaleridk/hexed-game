var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongo = require('mongodb');
var mongoose = require('mongoose');

var routes = require('./routes/index');
var users = require('./routes/users');
var playingfield = require('./routes/playingfield');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var _HexGame = require('./server/HexGame.js');
var HexGame = new _HexGame.HexGame({});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/play', playingfield);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


// 
// Hver socket er en spiller
// 
// Kan lave socket.player_id for at skelne
io.on('connection', function(socket){
    console.log('client connected');
    socket.player = null;

    socket.on('JoinTheActionRequest', function(message) {
      console.log("JoinTheActionRequest received!")
      var msgObj = JSON.parse(message);

      if(!socket.player) {
        // Temporary check for playing as #0 every time..
        // if(HexGame.ConnectedPlayers.length > 0) {
        //   socket.player = HexGame.ConnectedPlayers[0];
        //   socket.player.socket = socket;
        //   console.log("Player #" + socket.player.player_id + " continued.");
        // }else{
        //   socket.player = HexGame.CreateNewPlayer();
        //   socket.player.socket = socket;
        //   console.log("Player #" + socket.player.player_id + " created.");
        // }
        socket.player = HexGame.CreateNewPlayer();
        socket.player.socket = socket;
        console.log("Player #" + socket.player.player_id + " created.");
      }

      io.emit('JoinTheActionResponse', JSON.stringify({
        hexMap : {
          columns: socket.player.hexedGrid.columnCount,
          rows: socket.player.hexedGrid.rowCount,
          row_offset: socket.player.hexedGrid.gridSettings.OffsetRows
        },
        player : {
          player_id: socket.player.player_id,
          troop_count: socket.player.troop_count,
          hexedGridSnapshot: socket.player.hexedGrid.grid,
          new_turn: 321,
          food: socket.player.resources.food,
          wood: socket.player.resources.wood,
          stone: socket.player.resources.stone,
          iron: socket.player.resources.iron
          // ELLER ReturnClientFriendlyGrid
          // Måske jeg bare kun gemmer relevant info i snapshottet..
        }
      }));
    });

    socket.on('ClientAttackHexRequest', function(message) {
      console.log("ClientAttackHexRequest received!")
      var msgObj = JSON.parse(message);
      console.log(msgObj);
      
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      // Calculate combat stuff
      // Change hexgrid etc.
      HexGame.CommencePlayerAttack(socket.player, theHex, msgObj.troop_count);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);
    });
    socket.on('ClientOracleHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerOracleCheck(socket.player, theHex);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('RecruitTroopsForHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerRecruitForHex(socket.player, theHex);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('RemoveTroopsFromHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerRemoveTroopFromHex(socket.player, theHex, msgObj.amount);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('AddTroopsFromHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerAddTroopToHex(socket.player, theHex, msgObj.amount);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('BuildAppleYardForHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerBuildAppleYardOnHex(socket.player, theHex);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('BuildWoodCutterForHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerBuildWoodCutterOnHex(socket.player, theHex);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('BuildStoneCutterForHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerBuildStoneCutterOnHex(socket.player, theHex);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('BuildIronSmelterForHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerBuildIronSmelterOnHex(socket.player, theHex);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });
    socket.on('BuildLookoutTowerForHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var playerHex = socket.player.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      var theHex = HexGame.hexedGrid.grid[playerHex.row][playerHex.column];

      // Consume food for the player
      HexGame.ConsumePlayerFood(socket.player);

      HexGame.CommencePlayerBuildLookoutTowerOnHex(socket.player, theHex);

      // Send details about the hex back
      SendClientTurnExecutedResponse(socket, socket.player, theHex);

    });





    socket.on('DebugChangePlayerMessage', function(message) {
      var msgObj = JSON.parse(message);
      if([0, 1, 2].indexOf(msgObj.player_id) > -1) {
        socket.player = HexGame.debugPlayers[msgObj.player_id];
        console.log("Changed to player #" + socket.player.player_id);

        // Send details about player change
        io.emit('DebugChangePlayerResponse', JSON.stringify({
          player : {
            player_id: socket.player.player_id,
            troop_count: socket.player.troop_count,
            hexedGridSnapshot: socket.player.hexedGrid.grid,
            new_turn: 321
            // ELLER ReturnClientFriendlyGrid
            // Måske jeg bare kun gemmer relevant info i snapshottet..
          },
          server : {
            new_turn: HexGame.hexedTimeline.turns.length
          }
        }));
      }
    });
    socket.on('DebugGetServerHexedGrid', function(message) {
      var msgObj = JSON.parse(message);        

      // Send details about player change
      io.emit('DebugGetServerHexedGridResponse', JSON.stringify({
        hexedGrid : HexGame.hexedGrid.grid
      }));
    });
    socket.on('DebugGetServerTimeline', function(message) {
      var msgObj = JSON.parse(message);        

      // Send details about player change
      io.emit('DebugGetServerTimelineResponse', JSON.stringify({
        TimelineTurns : HexGame.hexedTimeline.turns
      }));
    });
    socket.on('DebugGetServerSnaphot', function(message) {
      var msgObj = JSON.parse(message);        

      // Send details about player change
      io.emit('DebugGetServerSnaphotResponse', JSON.stringify({
        hexedGrid : HexGame.hexedTimeline.SnapshotHexGridAtTurn(msgObj.turn)
      }));
    });


    // Send details about the game on load..
    // io.emit('ClientConnectResponse', JSON.stringify({
    //   hexMap : {
    //     columns: HexGame.columnCount,
    //     rows: HexGame.rowCount
    //   },
    //   player : {
    //     player_id: socket.player.player_id,
    //     troop_count: socket.player.troop_count,
    //     hexedGridSnapshot: socket.player.hexedGrid.grid,
    //     new_turn: 321,
    //     food: socket.player.resources.food,
    //     wood: socket.player.resources.wood,
    //     stone: socket.player.resources.stone
    //     // ELLER ReturnClientFriendlyGrid
    //     // Måske jeg bare kun gemmer relevant info i snapshottet..
    //   }
    // }));

    var SendClientTurnExecutedResponse = function(socket, player, theHex){
      socket.emit('ClientTurnExecutedResponse', JSON.stringify({
        player : {
          troop_count: player.troop_count,
          new_turn: 123,
          food: player.resources.food,
          wood: player.resources.wood,
          stone: player.resources.stone
        },
        hexGridData: player.hexedGrid.grid,
        mainAffectedHex: {
          column: theHex.column - player.hexedGrid.gridSettings.OffsetColumns, 
          row: theHex.row - player.hexedGrid.gridSettings.OffsetRows,
          row_offset: socket.player.hexedGrid.gridSettings.OffsetRows
        }
      }));
    };
});


app.set('port', (process.env.PORT || 3000));
http.listen(app.get('port'), function(){
  console.log('Server started on port '+app.get('port'));
});


module.exports = app;
