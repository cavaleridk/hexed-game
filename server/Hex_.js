

var Hex = function(options){
	var self = this;
	self.options = options || {};

	// Skal defaulte til null hvis man ikke kender det
	// Server skal sætte den til en ejer hver gang
	// -100 = neutral
	// Søg gennem script efter rettelser
	self.owner_id = (self.options.owner_id >= 0) ? self.options.owner_id : -100;

	// Default til null
	// Skal tjekke efter null ved action loops i HexGame
	self.troop_count = (self.options.troop_count >= 0) ? self.options.troop_count : null;

	self.building_type = self.options.building_type || null;

	// A value between 0 & 1 which determines how precise this info is, relative
	// to the actual state of the hex
	// 1 = this is the actual state..
	self.info_precision = (self.options.info_precision >= 0) ? self.options.info_precision : 1;

	self.column = (self.options.column >= 0) ? self.options.column : -1;
	self.row = (self.options.row >= 0) ? self.options.row : -1;
};

exports.Hex = Hex;