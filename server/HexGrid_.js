var _hex = require('./Hex.js');

var HexGrid = function(options){
	var self = this;
	self.options = options || {};

	self.columnCount = self.options.columns || 5;
	self.rowCount = self.options.rows || 9;



   // HexGrid[
   //    Row0[
   //      Hex0{owner_id: 1, troops: 7, building: null},
   //      Hex1{owner_id: 2, troops: 25, building: 'house'},
   //      Hex2{owner_id: null(neutral), troops: 2, building: null},
   //    ],
   //    Row1[Hex0{},Hex1{},Hex2{}],
   //    Row2[Hex0{},Hex1{},Hex2{}]
   //  ]
	self.grid = GenerateHexGrid(self.rowCount, self.columnCount);

	self.GenerateBlankGrid = function() {
		var grid = [];
		for (var i = 0; i < self.rowCount; i++) {
			var aRow = [];
			for (var j = 0; j < self.columnCount; j++) {
				aRow.push({});
			}
			grid.push(aRow);
		}
		return grid;
	};

	self.GenerateSnapshot = function(number_of_turns, player_id) {
		var snappedGrid = GenerateHexGrid(self.rowCount, self.columnCount);

		// for (var i = 0; i < number_of_turns; i++) {
		// 	[].forEach.call(TimelineTurns, function (el) {

		// 	}
		// }

		// // For at lave et snapshot af hvordan spillet så ud på en bestemt tur, kan man
	 //    // gøre noget al'a:
	 //    var snappedHexGrid = new HexGrid({'data_to_generate_same_hexgrid_in_turn_0'});
	 //    // TurnNumberToSnapshot = dét antal turns man vil se frem til
	 //    for(var i = 0; i < TurnNumberToSnapshot; i++) {
	 //      TimelineTurns[i].hexGrid.foreach(function(rowKey => eachRow){
	 //        eachRow.foreach(function(columnKey => eachHex){
	 //          if(eachHex != null) {
	 //            snappedHexGrid[rowKey][columnKey] = eachHex;
	 //          }
	 //        });
	 //      });
	 //      TimelineTurns[i].playerActions.foreach(function(...){...});
	 //    }
	 //    return snappedHexGrid;
	};

	
};

function GenerateHexGrid(rows, columns){
	var grid = [];
	for (var i = 0; i < rows; i++) {
		var aRow = [];
		for (var j = 0; j < columns; j++) {
			// Lav hexes med samme default settings
			var hex = new _hex.Hex({column: j, row: i, troop_count: 0}); // @TODO: Hexes skal kunne genereres med samme random data(troops etc)
			aRow.push(hex);
		}
		grid.push(aRow);
	}
	return grid;
};

exports.HexGrid = HexGrid;