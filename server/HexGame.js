var _hexGrid = require('./HexGrid.js');
var _Player = require('./Player.js');
var _hex = require('./Hex.js');
var _buildings = require('./Buildings.js');


var HexGame = function(options){
	var self = this;
	self.options = options || {};
	self.name = self.options.name || 'HexGame';
	// @TODO: Find et eller andet random seed library til at gentage de samme random data
	// Math.random kan ikke seedes
	self.randomSeed = self.options.randomSeed || 1;

	self.columnCount = self.options.columns || 7;
	self.rowCount = self.options.rows || 13;

	self.Buildings = new _buildings.Buildings({});

   // HexGrid[
   //    Row0[
   //      Hex0{owner_id: 1, troops: 7, building: null},
   //      Hex1{owner_id: 2, troops: 25, building: 'house'},
   //      Hex2{owner_id: null(neutral), troops: 2, building: null},
   //    ],
   //    Row1[Hex0{},Hex1{},Hex2{}],
   //    Row2[Hex0{},Hex1{},Hex2{}]
   //  ]
	self.hexedGrid = new _hexGrid.HexGrid({is_server: true, columns: self.columnCount, rows: self.rowCount}); 

	// Temporary solution until the user/player system goes live
	self.debugPlayers = [
		new _Player.Player({player_id: 0, grid: {columns: self.columnCount, rows: self.rowCount}}),
		new _Player.Player({player_id: 1, grid: {columns: self.columnCount, rows: self.rowCount}}),
		new _Player.Player({player_id: 2, grid: {columns: self.columnCount, rows: self.rowCount}})
	];
	// Copy the resources on each hex to their memory
	for (var i = 0; i < self.debugPlayers.length; i++) {
		self.debugPlayers[i].hexedGrid.CopyResourcesFromGrid(self.hexedGrid.grid);
	}

	self.ConnectedPlayers = [];
	self.CreateNewPlayer = function() {
		var availableHexes = self.hexedGrid.ReturnEvenHexesWithoutResources();

		if(availableHexes.length > 0) {
			// Fall back to 0 just in case
			var randomInt = getRandomIntInclusive(0, availableHexes.length-1);
			var startingHex = availableHexes[randomInt];
			var newPlayer = new _Player.Player({player_id: self.ConnectedPlayers.length, grid: {columns: 1, rows: 1}});
			
			startingHex.troop_count = 3;
			startingHex.owner_id = newPlayer.player_id;
			newPlayer.hexedGrid.gridSettings.OffsetRows = startingHex.row;
			newPlayer.hexedGrid.gridSettings.OffsetColumns = startingHex.column;
			console.log("TSTER!!");
			console.log(newPlayer.hexedGrid.grid);

			// Lav offset for players grid
			// Indsæt griddet med offset
			// 	starting hex skal sættes ind i 0, 0
			// 	Hvis surrounding[i] < 0, unshift(hexen) ind i array i rigtige række / kolonne
			// 	ved at lave en ny
			// 	Hvis surrounding[i] >= 0 indsæt med [i][j] eller reaplceingrid

			// Find the surrounding hexes to add to range of vision
			var surroundingHexes = self.hexedGrid.FindSurroundingHexes(startingHex);

			// Start with i=1 to avoid using the starting hex itself
			for (var i = 1; i < surroundingHexes.length; i++) {
				// If this hex actually exist on the map
				if(Object.keys(surroundingHexes[i]).length > 0) {
					// insert
					newPlayer.hexedGrid.ReplaceHexInGrid({row: surroundingHexes[i].row, column: surroundingHexes[i].column, main_resource: null});
				}
			}

			self.hexedGrid.ReplaceHexInGrid(startingHex);
			newPlayer.hexedGrid.ReplaceHexInGrid(startingHex);
			console.log(newPlayer.hexedGrid.grid);

			newPlayer.hexedGrid.CopyResourcesFromGrid(self.hexedGrid.grid);
			console.log(newPlayer.hexedGrid.grid);

			self.ConnectedPlayers.push(newPlayer);
			return newPlayer;

		}else{
			console.log("CreateNewPlayer: WARNING - Not enough empty hexes to give this player!");
		}
	};
	/**
	 * Adds a range of vision from hexadecimal.
	 *
	 * @class      AddRangeOfVisionFromHex (name)
	 * @param      {<type>}  player  The player
	 * @param      {<type>}  hex     The hexadecimal
	 * 
	 * @todo	CLEANUP ! 
	 */
	self.AddRangeOfVisionFromHex = function(player, hex) {
		console.log("Adding range of vision for hex: ");
		console.log(hex);
		if(hex.building_type) {
			switch(hex.building_type.name) {
				// If the building is an Apple Yard
				case self.Buildings.LookoutTower.name :
					var surroundingHexes = self.hexedGrid.FindSurroundingHexes(hex);
					// Start with i=1 to avoid using the starting hex itself
					for (var i = 1; i < surroundingHexes.length; i++) {
						// If this hex actually exist on the map
						if(Object.keys(surroundingHexes[i]).length > 0) {
							var playerHex = player.hexedGrid.FindHexInGrid(surroundingHexes[i]);
							
							// If this hex exists in the players snapshot
							if(playerHex !== null) {
								// If hex is not known already
								if(playerHex === -1) {
									// insert
									player.hexedGrid.ReplaceHexInGrid({row: surroundingHexes[i].row, column: surroundingHexes[i].column, main_resource: surroundingHexes[i].main_resource});
								}
							}else{
								player.hexedGrid.ReplaceHexInGrid({row: surroundingHexes[i].row, column: surroundingHexes[i].column});
							}
							
							// Add second line of sight for each hex
							var surroundingHexesSecondLine = self.hexedGrid.FindSurroundingHexes(surroundingHexes[i]);
							// Start with j=1 to avoid using the starting hex itself
							for (var j = 1; j < surroundingHexesSecondLine.length; j++) {
								// If this hex actually exist on the map
								if(Object.keys(surroundingHexesSecondLine[j]).length > 0) {
									var playerHexSecondLine = player.hexedGrid.FindHexInGrid(surroundingHexesSecondLine[j]);
							
									// If this hex exists in the players snapshot
									if(playerHexSecondLine !== null) {
										// If hex is not known already
										if(playerHexSecondLine === -1) {
											// insert
											player.hexedGrid.ReplaceHexInGrid({row: surroundingHexesSecondLine[j].row, column: surroundingHexesSecondLine[j].column, main_resource: surroundingHexesSecondLine[j].main_resource});
										}
									}else{
										player.hexedGrid.ReplaceHexInGrid({row: surroundingHexesSecondLine[j].row, column: surroundingHexesSecondLine[j].column});
									}
								}
								// Recalculating surrounding hexes in case offset has changed
								surroundingHexesSecondLine = self.hexedGrid.FindSurroundingHexes(surroundingHexes[i]);
							}
						}

						// Recalculate surrounding hexes in case offset has changed
						surroundingHexes = self.hexedGrid.FindSurroundingHexes(hex);
					}

					break;

				default:
					var surroundingHexes = self.hexedGrid.FindSurroundingHexes(hex);
					// Start with i=1 to avoid using the starting hex itself
					for (var i = 1; i < surroundingHexes.length; i++) {
						// If this hex actually exist on the map
						if(Object.keys(surroundingHexes[i]).length > 0) {
							var playerHex = player.hexedGrid.FindHexInGrid(surroundingHexes[i]);
							
							// If this hex exists in the players snapshot
							if(playerHex !== null) {
								// If hex is not known already
								if(playerHex === -1) {
									// insert
									player.hexedGrid.ReplaceHexInGrid({row: surroundingHexes[i].row, column: surroundingHexes[i].column, main_resource: surroundingHexes[i].main_resource});
								}
							}else{
								player.hexedGrid.ReplaceHexInGrid({row: surroundingHexes[i].row, column: surroundingHexes[i].column});
							}
						}

						// Recalculate surrounding hexes in case offset has changed
						surroundingHexes = self.hexedGrid.FindSurroundingHexes(hex);
					}

					break;
			}
		}else{
			var surroundingHexes = self.hexedGrid.FindSurroundingHexes(hex);
			// Start with i=1 to avoid using the starting hex itself
			for (var i = 1; i < surroundingHexes.length; i++) {
				// If this hex actually exist on the map
				if(Object.keys(surroundingHexes[i]).length > 0) {
					var playerHex = player.hexedGrid.FindHexInGrid(surroundingHexes[i]);
					
					// If this hex exists in the players snapshot
					if(playerHex !== null) {
						// If hex is not known already
						if(playerHex === -1) {
							// insert
							player.hexedGrid.ReplaceHexInGrid({row: surroundingHexes[i].row, column: surroundingHexes[i].column, main_resource: surroundingHexes[i].main_resource});
						}
					}else{
						player.hexedGrid.ReplaceHexInGrid({row: surroundingHexes[i].row, column: surroundingHexes[i].column});
					}
				}

				// Recalculate surrounding hexes in case offset has changed
				surroundingHexes = self.hexedGrid.FindSurroundingHexes(hex);
			}
		}

	};


	self.TemporaryResourceGenerationTimer = setInterval(function(){
		// check each player
		for (var i = 0; i < self.ConnectedPlayers.length; i++) {
			var playersHexes = self.hexedGrid.ReturnHexesByOwnerID(self.ConnectedPlayers[i].player_id); 
			var gainedFood = 0;
			var gainedWood = 0;
			var gainedStone = 0;
			var gainedIron = 0;

			// For each of the players hexes
			for (var j = 0; j < playersHexes.length; j++) {
				// If the player has a building on this hex
				if(playersHexes[j].building_type != null) {
					switch(playersHexes[j].building_type.name) {
						// If the building is an Apple Yard
						case self.Buildings.AppleYard.name :
							console.log("ResourceGeneration: Increasing player" + i +" food by 1");
							self.ConnectedPlayers[i].resources.food += 1;
							gainedFood += 1;

							break;

						// If the building is a Woodcutter
						case self.Buildings.WoodCutter.name :
							console.log("ResourceGeneration: Increasing player" + i +" wood by 1");
							self.ConnectedPlayers[i].resources.wood += 1;
							gainedWood += 1;

							break;

						// If the building is a StoneCutter
						case self.Buildings.StoneCutter.name :
							console.log("ResourceGeneration: Increasing player" + i +" stone by 1");
							self.ConnectedPlayers[i].resources.stone += 1;
							gainedStone += 1;

							break;

						// If the building is an Iron Smelter
						case self.Buildings.IronSmelter.name :
							console.log("ResourceGeneration: Increasing player" + i +" iron by 1");
							self.ConnectedPlayers[i].resources.iron += 1;
							gainedIron += 1;

							break;

						default : break;
					}
				}
			}

			// If the player socket is set
			if(self.ConnectedPlayers[i].socket) {
				if(gainedFood > 0 || gainedWood > 0 || gainedStone > 0 || gainedIron > 0) {

					self.ConnectedPlayers[i].socket.emit('ResourceUpdateNotification', JSON.stringify({
				      resources : {
				        food: self.ConnectedPlayers[i].resources.food,
				        wood: self.ConnectedPlayers[i].resources.wood,
				        stone: self.ConnectedPlayers[i].resources.stone,
				        iron: self.ConnectedPlayers[i].resources.iron
				      }
				    }));
				}
			}
		}
	}, 2000);


	self.ConsumePlayerFood = function(player) {
		// Hver armed troop koster noget food at vedligeholde
  //     ☐ Hver borger i en hex koster
  //       ☐ Defenders i en hex koster XX hver (kan evt. være brøkdel af en food??)
  //       ☐ Hver bygning har XX personer, og bygningen koster XX food

  		var consumedFood = 0;
  		// Calculate food consumption based on armed units
  		// 1 food per 5 units (5*0.2=1)
  		// 	(4 units = 1 food, 5 = units = 1 food, 6 units = 2 food)
  		consumedFood += player.troop_count * 0.2;
  		console.log("ConsumedFood: " + consumedFood);
  		
  		// Get the amount of hexes this player controls
  		var playerHexes = player.hexedGrid.ReturnHexesByOwnerID(player.player_id);

  		// Loop through each of this players hex
  		for (var i = 0; i < playerHexes.length; i++) {
  			// Increase food consumption based on units on the hex
  			consumedFood += playerHexes[i].troop_count * 0.2;
  			console.log("ConsumedFood: " + consumedFood);

  			// If the hex has a building
  			if(playerHexes[i].building_type != null) {
  				// Increase food comsumption by the upkeep value
  				consumedFood += playerHexes[i].building_type.upkeep_food;
  			}
  		}

  		// (Calculate food consumption based on units in buildings)
  		// @TODO
  		// 
  		
  		// Avoid wierd behaviours when working with floats..
  		// Cut of any decimals beyond the 0.000X mark
  		// (leading + to remove unnecessary zero's)
		consumedFood = +consumedFood.toFixed(4);
		console.log("Real ConsumedFood: " + consumedFood);

  		// Remove food from the player
  		console.log("Foods: " + player.resources.food);
  		player.resources.food -= Math.ceil(consumedFood);
  		console.log("Foods: " + player.resources.food);
	};

	/**
	 * { function_description }
	 * 
	 * @TODO: 		Skal lige sætte et eller andet report halløj op,
	 * 				som tracker om man har ramt éen eller flere fjender
	 * 				og om man er langt ude i fremtid osv. Sådan man kan
	 * 				forstå hvorfor man har mistet flere tropper end hvad 
	 * 				første angreb forårsagede..
	 * 				
	 * 				Lige nu looper den helt ud i fremtiden, og fjerner alt
	 * 				hukommelse omkring tidligere ejers forhold til en hex.
	 * 				Det er et problem hvis de har gen-erobret den på et tidspunkt
	 * 				i fremtiden. Jeg skal have en måde at huske hvilken form for
	 * 				ændring der er registreret for en hex.
	 * 					Det kan være jeg bare skal have en flag på hver spiller,
	 * 					der fortæller om deres tid er ændret. Så kan den udregne 
	 * 					de nye konsekvenser på næste tur og præsentere det, og undgå
	 * 					at checke hele tidlinjen igennem hver turn :)
	 *
	 * @class      CommencePlayerAttack (name)
	 * @param      {<type>}  player       The player
	 * @param      {<type>}  hex          The hex in current time hexedGrid
	 * @param      {number}  troop_count  The troop count
	 */
	self.CommencePlayerAttack = function(player, hex, troop_count){
		// If the player does not have enough troops for this attack
		if(player.troop_count < troop_count) {
			console.log("Attack: Not enough troops..");
			// Stop the attack
			// Should probably send back some error in socket :)
			return false;
		}

		// Generate clone of hex
		var snapHex = CloneHex(hex);

		console.log("Attack: Player_id: " + player.player_id + ", Hex_owner: " + snapHex.owner_id);
		
		// If the player knows this hex and the player does not already own this hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id != player.player_id) {
			console.log("Attack: The player does not own this hex..");

			// Do simple combat math
			var battleRemainers = troop_count - snapHex.troop_count;
			console.log("Attack: The remaining troops: "+battleRemainers);

			// If the player won the battle
			if(battleRemainers > 0) {
				console.log("Attack: The player won battle");

				// Transfer the surviving troops to the hex
				player.troop_count -= troop_count;
				snapHex.troop_count = battleRemainers;
				console.log("Attack: Player troops: "+player.troop_count+", Hex: "+snapHex.troop_count);

				// If the owner of this hex is a player
				// neutral = -1
				if(snapHex.owner_id >= 0) {
					console.log("Attack: Previous owner was a player..");
					// Locate the last owner
					var lastOwner = self.ConnectedPlayers[snapHex.owner_id];

				    // Create an oracle view on the hex's new stats
					var oracleResults = DoOracleCalculations(snapHex);
					var oracleHex = new _hex.Hex({
						troop_count: oracleResults.estimate,
						column: snapHex.column,
						row: snapHex.row,
						owner_id: player.player_id,
						info_precision: oracleResults.precision
					});

					// Add the oracle perspective to the other players grid
					lastOwner.hexedGrid.ReplaceHexInGrid(oracleHex);
					console.log("Attack: Updated the previous owners grid");

					// Notify the player if they are online
					// 
				}else{
					console.log("Attack: Previous owner was neutral");
				}

				// Set the new owner id for this hex
        		snapHex.owner_id = player.player_id;

				// Record it in the players grid
				player.hexedGrid.ReplaceHexInGrid(snapHex);
				console.log("Attack: The players grid updated..");

				// Increase the players line of sight
				self.AddRangeOfVisionFromHex(player, snapHex);
				console.log("Attack: The players line of sight increased..");


				// Overwrite the hex in the hex grid on the server records
				self.hexedGrid.ReplaceHexInGrid(snapHex);
        		console.log("Attack: The Servers grid updated..");

			// If the hex occupant won the battle or the battle was a draw/tie
			}else{
				// Update the players troop count
				player.troop_count -= troop_count;
				// Default to 1 troop if the remaining is 0
				snapHex.troop_count = Math.abs(battleRemainers);
				if(snapHex.troop_count == 0) {
					snapHex.troop_count = 1;
				}

				// If the owner of this hex is a player
				// neutral = -1
				if(snapHex.owner_id >= 0) {
					// Locate the last owner
					var lastOwner = self.ConnectedPlayers[snapHex.owner_id];

					// Add the updates to the other players grid
					lastOwner.hexedGrid.ReplaceHexInGrid(snapHex);
					console.log("Attack: Updated the previous owners grid");

					// Notify the player if they are online
					// 
				}

				 // Create an oracle view on the hex's new stats
				var oracleResults = DoOracleCalculations(snapHex);
				var oracleHex = new _hex.Hex({
					troop_count: oracleResults.estimate,
					column: snapHex.column,
					row: snapHex.row,
					owner_id: snapHex.owner_id,
					info_precision: oracleResults.precision
				});

				// Record it in the players grid with oracle view
				player.hexedGrid.ReplaceHexInGrid(oracleHex);
				console.log("Attack: The players grid updated..");

				// Overwrite the hex in the hex grid on the server records
				self.hexedGrid.ReplaceHexInGrid(snapHex);
        		console.log("Attack: The Servers grid updated..");
			}

        // If the player is already the owner of this hex
        }else{
        	// Do nothing or register error or something..
        	// 
        	// 
        }

	};

	self.CommencePlayerOracleCheck = function(player, hex) {
		// Generate clone of hex
		var snapHex = CloneHex(hex);

		// If the player knows this hex and this is not the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id != player.player_id) {
			console.log("OracleView: This is not my own hex..");

			// Calculate a temporary estimate for the troop count
			// random between 0 and twice the actual number
			var oracleResults = DoOracleCalculations(snapHex);

			// 0 = bad precision
			// 1 = good precision
			// oracleResults.precision

			// Setup the snaphex with our new knowledge
			// Owner_id is already set from server knowledge
			// @todo: skal nok finde en random effekt for dét også..
			snapHex.troop_count = oracleResults.estimate;

			// Jeg skal også gemme precision i hexen..
			snapHex.info_precision = oracleResults.precision;

			// Record it in the players grid
			player.hexedGrid.ReplaceHexInGrid(snapHex);
			console.log("OracleView: The players grid updated..");

		// If the player is already the owner of this hex
        }else{
        	// Do nothing or register error or something..
        	// 
        	// 
        }
	};

	/**
	 * Not sure if I'm preventing players from recruiting a last troop
	 * before they get attack the same round.. Maybe I should fetch the snapHex
	 * directly from a players timeline instead?..
	 * 
	 * Actually I am going to do that now. If that isn't working, I'll roll back
	 * to the same way as Attack and Oracle ways of getting snapHex.
	 * 
	 */
	self.CommencePlayerRecruitForHex = function(player, hex) {
		// Clone the hex
		var snapHex = CloneHex(hex);

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			console.log("Recruit: The player owns this hex");
			// Increase the amount of troops on this hex by 1.
			// @TODO: make it work on upgrades from buildings etc.
			// 
			snapHex.troop_count += 1;

			// Record it in the players timeline
    		player.hexedGrid.ReplaceHexInGrid(snapHex);
			console.log("Recruit: Troops increase +1 and players grid updated..");

			self.hexedGrid.ReplaceHexInGrid(snapHex);
			console.log("Recruit: Servers grid updated");
		
		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};

	self.CommencePlayerRemoveTroopFromHex = function(player, hex, amount) {
		// Clone the hex
		var snapHex = CloneHex(hex);;

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			// If the hex has enough troops
			if(snapHex.troop_count > amount) {
				console.log("RemoveTroops: The player owns this hex and it has enough troops");
				// Reduce the amount of troops
				snapHex.troop_count -= amount;
				player.troop_count += amount;

				// Record it in the players timeline
				player.hexedGrid.ReplaceHexInGrid(snapHex);
				console.log("RemoveTroops: Troops relocated and players grid updated");
				
				self.hexedGrid.ReplaceHexInGrid(snapHex);
				console.log("RemoveTroops: Servers grid updated");
				
			// if the hex does not have enough troops
			}else{
				// register some sort of error?
				// 
			}

		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};
	self.CommencePlayerAddTroopToHex = function(player, hex, amount) {
		// Clone the hex
		var snapHex = CloneHex(hex);;

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			// If the player has enough troops
			if(player.troop_count >= amount) {
				console.log("AddTroops: The player owns this hex and has enough troops");
				// Reduce the amount of troops
				snapHex.troop_count += amount;
				player.troop_count -= amount;

				// Record it in the players timeline
				player.hexedGrid.ReplaceHexInGrid(snapHex);
				console.log("AddTroops: Troops relocated and players timeline updated");

				self.hexedGrid.ReplaceHexInGrid(snapHex);
				console.log("AddTroops: Servers grid updated");
			
			// if the player does not have enough troops
			}else{
				// register some sort of error?
				// 
			}

		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};
	self.CommencePlayerBuildAppleYardOnHex = function(player, hex) {
		// Clone the hex
		var snapHex = CloneHex(hex);;

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			// If the hex has enough troops
			if(hex.troop_count > self.Buildings.AppleYard.cost_units) {
				console.log("BuildAppleYard: The player owns this hex and it has enough troops");
				// Reduce the amount of troops
				snapHex.troop_count -= self.Buildings.AppleYard.cost_units;
				snapHex.building_type = self.Buildings.AppleYard;

				// Record it in the players timeline
				player.hexedGrid.ReplaceHexInGrid(snapHex);
				console.log("BuildAppleYard: Troops relocated and players timeline updated");

				self.hexedGrid.ReplaceHexInGrid(snapHex);
				console.log("BuildAppleYard: Servers grid updated");
			
			// if the hex does not have enough troops
			}else{
				// register some sort of error?
				// 
			}

		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};
	self.CommencePlayerBuildWoodCutterOnHex = function(player, hex) {
		// Clone the hex
		var snapHex = CloneHex(hex);;

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			// If this hex has the wood resource
			if(snapHex.main_resource == "Wood") {
				// If the hex has enough troops
				if(hex.troop_count > self.Buildings.WoodCutter.cost_units) {
					console.log("BuildWoodCutter: The player owns this hex and it has enough troops");
					// Reduce the amount of troops
					snapHex.troop_count -= self.Buildings.WoodCutter.cost_units;
					snapHex.building_type = self.Buildings.WoodCutter;

					// Record it in the players timeline
					player.hexedGrid.ReplaceHexInGrid(snapHex);
					console.log("BuildWoodCutter: Troops relocated and players timeline updated");

					self.hexedGrid.ReplaceHexInGrid(snapHex);
					console.log("BuildWoodCutter: Servers grid updated");
				
				// if the hex does not have enough troops
				}else{
					// register some sort of error?
					// 
				}
			// if the hex does not have the required resource
			}else{
				// register some sort of error?
				// 
			}

		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};
	self.CommencePlayerBuildStoneCutterOnHex = function(player, hex) {
		// Clone the hex
		var snapHex = CloneHex(hex);

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			// If this hex has the wood resource
			if(snapHex.main_resource == "Stone") {
				// If the hex has enough troops
				if(hex.troop_count > self.Buildings.StoneCutter.cost_units) {
					// If the player has enough wood
					if(player.resources.wood >= self.Buildings.StoneCutter.cost_wood) {
						console.log("BuildStoneCutter: The player owns this hex and it has enough troops");
						// Reduce the amount of troops
						snapHex.troop_count -= self.Buildings.StoneCutter.cost_units;
						player.resources.wood -= self.Buildings.StoneCutter.cost_wood;
						snapHex.building_type = self.Buildings.StoneCutter;

						// Record it in the players timeline
						player.hexedGrid.ReplaceHexInGrid(snapHex);
						console.log("BuildStoneCutter: Troops relocated and players timeline updated");

						self.hexedGrid.ReplaceHexInGrid(snapHex);
						console.log("BuildStoneCutter: Servers grid updated");
				
					// if the player does not have enough wood
					}else{
						// register some sort of error?
						// 
					}

				// if the hex does not have enough troops
				}else{
					// register some sort of error?
					// 
				}
			// if the hex does not have the required resource
			}else{
				// register some sort of error?
				// 
			}

		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};
	self.CommencePlayerBuildIronSmelterOnHex = function(player, hex) {
		// Clone the hex
		var snapHex = CloneHex(hex);

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			// If this hex has the wood resource
			if(snapHex.main_resource == "Iron") {
				// If the hex has enough troops
				if(hex.troop_count > self.Buildings.IronSmelter.cost_units) {
					// If the player has enough resources
					if(player.resources.wood >= self.Buildings.IronSmelter.cost_wood && player.resources.stone >= self.Buildings.IronSmelter.cost_stone) {
						console.log("BuildStoneCutter: The player owns this hex and it has enough troops");
						// Reduce the amount of troops
						snapHex.troop_count -= self.Buildings.IronSmelter.cost_units;
						player.resources.wood -= self.Buildings.IronSmelter.cost_wood;
						player.resources.stone -= self.Buildings.IronSmelter.cost_stone;
						snapHex.building_type = self.Buildings.IronSmelter;

						// Record it in the players timeline
						player.hexedGrid.ReplaceHexInGrid(snapHex);
						console.log("BuildStoneCutter: Troops relocated and players timeline updated");

						self.hexedGrid.ReplaceHexInGrid(snapHex);
						console.log("BuildStoneCutter: Servers grid updated");
				
					// if the player does not have enough resources
					}else{
						// register some sort of error?
						// 
					}

				// if the hex does not have enough troops
				}else{
					// register some sort of error?
					// 
				}
			// if the hex does not have the required resource
			}else{
				// register some sort of error?
				// 
			}

		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};
	self.CommencePlayerBuildLookoutTowerOnHex = function(player, hex) {
		// Clone the hex
		var snapHex = CloneHex(hex);

		// If the player knows this hex and this is the players hex
		if (player.hexedGrid.ExistsInGrid(snapHex) && snapHex.owner_id == player.player_id) {
			// If the hex has enough troops
			if(hex.troop_count > self.Buildings.LookoutTower.cost_units) {
				// If the player has enough resources
				if(player.resources.wood >= self.Buildings.LookoutTower.cost_wood && player.resources.stone >= self.Buildings.LookoutTower.cost_stone) {
					console.log("BuildStoneCutter: The player owns this hex and it has enough troops");
					// Reduce the amount of troops
					snapHex.troop_count -= self.Buildings.LookoutTower.cost_units;
					player.resources.wood -= self.Buildings.LookoutTower.cost_wood;
					player.resources.stone -= self.Buildings.LookoutTower.cost_stone;
					snapHex.building_type = self.Buildings.LookoutTower;

					// Record it in the players timeline
					player.hexedGrid.ReplaceHexInGrid(snapHex);
					console.log("BuildStoneCutter: Troops relocated and players timeline updated");

					self.hexedGrid.ReplaceHexInGrid(snapHex);
					console.log("BuildStoneCutter: Servers grid updated");

					// Increase range of vision with the lookout tower
					self.AddRangeOfVisionFromHex(player, snapHex);
			
				// if the player does not have enough resources
				}else{
					// register some sort of error?
					// 
				}

			// if the hex does not have enough troops
			}else{
				// register some sort of error?
				// 
			}

		// IF this is not the players hex
		}else{
			// register some sort of error?
			// 
		}
	};
	

	/**
	 * Array of all the actions that has happened on each turn.
	 * 
	 * Total amount of turns this game = compiledTurns.length
	 * 
	 * STRUCTURE
	 * [
	 * 	// Turn 1
	 *   [
	 *     [player_id, GameAction],
	 *     [player_id, GameAction]
	 *   ],
	 *  // Turn 2
	 *   [
	 *     [player_id, GameAction]
	 *   ]
	 * ]
	 */
	self.compiledTurns = [];

	self.DoGameAction = function(player_id, action_type) {
		// Actiontypes kan være "attack", "build" etc.?
	};

	var DoOracleCalculations = function(hex) {
		// Calculate a temporary estimate for the troop count
		// random between 0 and twice the actual number
		var randEst = getRandomIntInclusive(0, hex.troop_count * 2);

		// Calculate a precision on this estimate
		var estDist = CalculateDistanceBetweenNumbers(hex.troop_count, randEst);
		// Limit the estimate within the real range
		// this is probably overkill by a long shot, but meh..
		estDist = (estDist <= hex.troop_count) ? estDist : hex.troop_count;

		// Get a point of how good this precision is
		// 0 = bad precision
		// 1 = good precision
		var estPre = ConvertDistanceValueToPrecision(hex.troop_count, estDist);

		return {
			estimate: randEst,
			precision: estPre
		};
	}

	// Returns a random integer between min (included) and max (included)
	// Using Math.round() will give you a non-uniform distribution!
	var getRandomIntInclusive = function(min, max) {
	  min = Math.ceil(min);
	  max = Math.floor(max);
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	var CalculateDistanceBetweenNumbers = function(base, check) {
	  if(check > 0) {
	    if(check < base) {
	      return base % check;
	    }else if(check > base) {
	      var divided = Math.floor((check - base) / base);
	      var conquered = divided * base;

	      return (check % base) + conquered;
	    }else{
	      return 0;
	    }
	  }else{
	    return base;
	  }
	};

	/**
	 * Lige nu er precision en value mellem 0 & 1 som viser opacity
	 * 
	 * @TODO: Skal gøres meget bedre ;)
	 *
	 * @class      ConvertDistanceValueToPrecision (name)
	 * @param      {number}  base      The base
	 * @param      {number}  distance  The distance
	 * @return     {number}  { description_of_the_return_value }
	 */
	var ConvertDistanceValueToPrecision = function(base, distance) {
		// If the distance is 0, return full precision
		if(distance === 0) {
			return 1;
		}
		var decimal = distance / base;
		var opposite = 1 - decimal;
		var precision = 0;

		precision = (opposite > 0.05) ? opposite : 0.05;

		return precision;
	};

	var CloneHex = function (hex) {
		// Clone the hex
		var clonedHex = new _hex.Hex({
			owner_id: hex.owner_id, 
			is_sea: hex.is_sea, 
			main_resource: hex.main_resource, 
			troop_count: hex.troop_count,
			building_type: hex.building_type,
			info_precision: hex.info_precision,
			column: hex.column, 
			row: hex.row
		});

		return clonedHex;
	};
};

exports.HexGame = HexGame;