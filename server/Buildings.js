

var Buildings = function(options){
	var self = this;
	self.options = options || {};

	self.WoodCutter = {
		name: "WoodCutter",
		cost_units: 2,
		// cost_wood: 10,

		upkeep_food: 2
	};

	self.StoneCutter = {
		name: "StoneCutter",
		cost_units: 2,
		cost_wood: 10,

		upkeep_food: 2
	};

	self.IronSmelter = {
		name: "IronSmelter",
		cost_units: 4,
		cost_wood: 30,
		cost_stone: 20,

		upkeep_food: 4
	};

	self.AppleYard = {
		name: "AppleYard",
		cost_units: 2,
		// cost_wood: 10,

		upkeep_food: 0
	};

	self.LookoutTower = {
		name: "LookoutTower",
		cost_units: 3,
		cost_wood: 20,
		cost_stone: 10,

		upkeep_food: 3
	};

	self.ExampleBuilding = {
		// name of the building
		name: "ExampleBuilding",
		// The cost to build it
		cost_units: 2,
		cost_iron: 10,
		cost_wood: 15,
		cost_stone: 15,
		cost_currency: 4201,

		// The cost of keeping this building running
		// can be floats (IE 0.5 || 0.2 etc)
		upkeep_food: 0
	};
};

exports.Buildings = Buildings;