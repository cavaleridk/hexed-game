var _hexGrid = require('./HexGrid.js');
var _hexTimeline = require('./HexTimeline.js');
var _Player = require('./Player.js');
var _hex = require('./Hex.js');


var HexGame = function(options){
	var self = this;
	self.options = options || {};
	self.name = self.options.name || 'HexGame';

	self.columnCount = self.options.columns || 5;
	self.rowCount = self.options.rows || 9;

	// Temporary solution until the user/player system goes live
	self.debugPlayers = [
		new _Player.Player({player_id: 0}),
		new _Player.Player({player_id: 1}),
		new _Player.Player({player_id: 2})
	];

   // HexGrid[
   //    Row0[
   //      Hex0{owner_id: 1, troops: 7, building: null},
   //      Hex1{owner_id: 2, troops: 25, building: 'house'},
   //      Hex2{owner_id: null(neutral), troops: 2, building: null},
   //    ],
   //    Row1[Hex0{},Hex1{},Hex2{}],
   //    Row2[Hex0{},Hex1{},Hex2{}]
   //  ]
	self.hexedGrid = new _hexGrid.HexGrid({}); 

	// (Server)TimelineTurns[ 
 //      // Key på turns == runde nummeret. AKA key=0 == runde 1
 //      Turn0[ 
 //        // HexGrid er en kopi af HexGrid som kun indeholder data om de hexes der er
 //        // blevet opdateret i denne runde
 //        HexGrid[
 //          Row0[
 //            Hex2{owner_id: 2, troops: 5, building: null}
 //          ],
 //          Row1[],
 //          Row2[
 //            Hex0{owner_id: 1, troops: 12, building: 'house'}
 //          ]
 //        ],
 //        // PlayerActions indeholder data om de handlinger hver spiller har lavet, hvor
 //        // Key == spillerens id, i denne runde.
 //        PlayerActions[
 //          player_id1 => PlayerAction{},
 //          player_id3 => PlayerAction{},          
 //          player_id2 => PlayerAction{}        
 //        ]
 //      ],
 //      Turn1[],
 //      Turn2[],
 //      Turn3[]
 //    ]
	self.hexedTimeline = new _hexTimeline.HexTimeline({server: true}); 

	// self.AddHexChangeToTimeline = function(turn_number, hex){
	// 	// Check if the hex has been changed in timeline

	// 		// If it was then

	// 	// If the turn has already been generated before
	// 	if(turn_number <= self.hexedTimeline.turns.length) {
	// 		var newTurn = self.hexedTimeline.turns[turn_number-1];

	// 	// If this turn has not previously been done by anyone
	// 	}else{
	// 		var newTurn = {HexGrid: [], PlayerActions: []};
	// 		newTurn.HexGrid = self.hexedGrid.GenerateBlankGrid();
	// 	}

	// 	if (hex) {
	// 		newTurn.HexGrid[hex.row][hex.column] = hex;
	// 	}
	// };

	/**
	 * RecreatePastEventsForPlayer
	 * 
	 * @TODO: I'll need to record that I have been glitched somehow, and return it 
	 * in the socket for some glitch visualeffect
	 *
	 * @class      RecreatePastEventsForPlayerNew (name)
	 * @param      {<type>}  player  The player
	 * @param      {<type>}  action  The action
	 */
	self.CheckPlayerForGlitch = function (player, action){
		console.log("CheckPlayerForGlitch: Checking player for glitches...");
		var thisTurn = player.hexedTimeline.turns.length-1;
		// Get the glitch history for this turn
		var myGlitchHistory = player.myGlitchHistory[thisTurn] || [];

		if(myGlitchHistory.length < 0) {
			console.log("CheckPlayerForGlitch: A glitch has been detected!");
			// Go through all of the hexes in the glitch (snapshot)
	        for (var row = 0; row < myGlitchHistory.length; row++) {
				for (var column = 0; column < myGlitchHistory[row].length; column++) {
					// Get a reference to this hex
					var hex = myGlitchHistory[row][column];

					// If the hex has been glitched
					if(hex) {
						console.log("CheckPlayerForGlitch: Found a hex that was glitched");
						var clonedHex = CloneHex(hex);

						// if this hex is the same one the player last affected
						if(clonedHex.row == player.myLastAffectedHex.row && clonedHex.column == player.myLastAffectedHex.column) {
							console.log("CheckPlayerForGlitch: This is a Collision Glitch!");
							// Loop through the switch and complete the appropriate callback function
							CollisionGlitchSwitch(action)(player, clonedHex);

						}else{
							var snappedHex = player.hexedGridSnapshot[clonedHex.row][clonedHex.column];

							// If the player lost ownership of this hex
							if(clonedHex.owner_id != player.player_id && snappedHex.owner_id == player.player_id) {
								console.log("CheckPlayerForGlitch: Player lost ownership of the hex..");
								// Do some Oracle estimations
								var insight = DoOracleCalculations(clonedHex);
								clonedHex.troop_count = insight.estimate;
								clonedHex.info_precision = insight.precision;

								console.log("CheckPlayerForGlitch: Calculated new oracle values..");

							// If the amount of troops have changed and the player owned this hex
							}else if(clonedHex.owner_id == player.player_id && clonedHex.troop_count != snappedHex.troop_count) {
								// Do nothing right now..
							}

							console.log("CheckPlayerForGlitch: Saving the glitch in players memory");
							// Overwrite the players snapshot
							player.hexedGridSnapshot[clonedHex.row][clonedHex.column] = clonedHex;
							// overwrite my history with this change
							player.hexedTimeline.RecordHexInTimeline(clonedHex, thisTurn);
						}
					}
				}
			}
		/**
		 * Skal laves om til en else if(der er sket glitches i andre ture som ikke er registreret)
		 * 
		 * Ikke alligevel, jeg sender altid glitchen frem til nyeste tur hvis den er bagud?
		 */
		}else {
			// If no glitches, save my buffered change to snapshot and server
			player.hexedGridSnapshot[player.myLastAffectedHex.row][player.myLastAffectedHex.column] = player.myLastAffectedHex;
		
			// Save the change to the server as well
			self.hexedTimeline.turns[thisTurn].hexGrid[player.myLastAffectedHex.row][player.myLastAffectedHex.column].push(player.myLastAffectedHex);
		}
	};

	var CollisionGlitchSwitch = function(action) {
		var callback = function(){};
        switch(action) {
        	case self.CommencePlayerAddTroopToHex : 
	            callback = function(player, glitchedHexReal) {
	            	console.log("CollisionGlitch: The player was adding troops to hex,");
	            	var snappedHex = player.glitchedHexedGridSnapshot[glitchedHex.row][glitchedHex.column];
	            	var glitchedHex = CloneHex(glitchedHexReal);

	            	// If the player lost ownership of this hex before increasing
					if(glitchedHex.owner_id != player.player_id && snappedHex.owner_id == player.player_id) {
						console.log("CollisionGlitch: when he suddenly lost control of it to another player!");
						// Do some Oracle estimations
						var insight = DoOracleCalculations(glitchedHex);
						glitchedHex.troop_count = insight.estimate;
						glitchedHex.info_precision = insight.precision;

						console.log("CollisionGlitch: Calculated new oracle insight..");

					// If the amount of troops have changed and the player owned this hex
					}else if(glitchedHex.owner_id == player.player_id && glitchedHex.troop_count != snappedHex.troop_count) {
						// Calculate the new amount of troops
						var delta = glitchedHex.troop_count - snappedHex.troop_count;
						glitchedHex.troop_count = player.myLastAffectedHex + delta;
						console.log("CollisionGlitch: only to see that it was direly needed. Some troops had been lost.");
					}

					console.log("CollisionGlitch: Saving to player timeline..");
					// Overwrite the players snapshot
					player.glitchedHexedGridSnapshot[glitchedHex.row][glitchedHex.column] = glitchedHex;
					// overwrite my history with this change
					player.glitchedHexedTimeline.RecordHexInTimeline(glitchedHex, player.ReturnNewestTurnIndex());

					console.log("CollisionGlitch: Saving to server timeline");
					// Save the change to the server as well
					self.hexedTimeline.turns[player.ReturnNewestTurnIndex()].hexGrid[player.myLastAffectedHex.row][player.myLastAffectedHex.column].push(player.myLastAffectedHex);
	            };
	            break;

	        default :

	            break;
          
        }

        return callback;
	};

	self.RecreatePastEventsForPlayer = function(player) {
		console.log("Recreate: Recalculating past events for player...");
		// If the players past has been changed by someone else
		// or if this is the first action
		if(player.myPastHasChangedByThirdParty || player.hexedGridSnapshot.length == 0){
			console.log("Recreate: This player was affected by other actions or it is first action..");
			// Recreate the snapshot from scratch
			player.hexedGridSnapshot = player.hexedTimeline.SnapshotHexGridAtTurn(player.hexedTimeline.turns.length);
		
			// Change status back to not changed
			player.myPastHasChangedByThirdParty = false;
		
		// If the player's past hasn't been changed by anyone else 
		}else if (! player.myPastHasChangedByThirdParty) {
			console.log("Recreate: This player was not affected by other actions.. ");
			// Fetch the last turn in my timeline
			var myLastTurn = player.hexedTimeline.turns[player.hexedTimeline.turns.length-1];
			// var serverTurn = self.hexedTimeline.turns[myTurnNumber-1];

			// Loop through each hex in the array
			for (var row = 0; row < player.hexedGridSnapshot.length; row++) {
	    		for (var column = 0; column < player.hexedGridSnapshot[row].length; column++) {
	    			// If the hex was changed this turn
		    		if(myLastTurn.hexGrid[row][column]) {
		    			console.log("Recreate: But a hex has still been changed in my history this turn..");
						// Create a clone of the hex
						var theNewHex = new _hex.Hex({
							owner_id: myLastTurn.hexGrid[row][column].owner_id, 
							troop_count: myLastTurn.hexGrid[row][column].troop_count,
							info_precision: myLastTurn.hexGrid[row][column].info_precision,
							building_type: myLastTurn.hexGrid[row][column].building_type,
							column: myLastTurn.hexGrid[row][column].column, 
							row: myLastTurn.hexGrid[row][column].row
						});

						// If this used to be my hex, and someone else took it over
						if (theNewHex.owner_id != player.hexedGridSnapshot[row][column].owner_id && player.hexedGridSnapshot[row][column].owner_id == player.player_id) {
							console.log("Recreate: Someone took over my hex!");
							// Generate some oracle logic and add the info to my timeline
	    					console.log("Recreate: Generating oracle knowledge of the new state of affairs..");
	    					var oracleResults = DoOracleCalculations(memoryOfServerHex);

	    					theNewHex.troop_count = oracleResults.estimate;
	    					theNewHex.info_precision = oracleResults.precision;
	    				}

						// Record it in the players timeline player.hexedTimeline.turns.length-1
						player.hexedTimeline.turns[player.hexedTimeline.turns.length-1].hexGrid[theNewHex.row][theNewHex.column] = theNewHex;
						player.hexedGridSnapshot[theNewHex.row][theNewHex.column] = theNewHex;
						// Update the hexedGrid as well
						self.hexedGrid.grid[theNewHex.row][theNewHex.column] = theNewHex;
						console.log("Recreate: The players timeline and hexgrid updated..");
					}
	    		}
	    	}
	    }

	    // Determine if the server has any other events happening simultanously
		// 
		// Do them here if so..

	    console.log("Recreate: Recalculating past events for player from server timeline...");
		// Fetch the last turn in my timeline
		var serverMemoryTurn = self.hexedTimeline.turns[player.hexedTimeline.turns.length-1];
		var memoryOfGrid = self.hexedTimeline.SnapshotHexGridAtTurn(serverMemoryTurn);

		// Loop through each hex in the array
	    console.log("Recreate: Checking if anything has happened that affects this player");
		for (var row = 0; row < memoryOfGrid.length; row++) {
    		for (var column = 0; column < memoryOfGrid[row].length; column++) {
    			var memoryOfServerHex = memoryOfGrid[row][column];
    			
    			// If the hex exists in server memory
	    		if(memoryOfServerHex) {

	    			// Find a memory of the players hex
	    			var memoryOfPlayerHex = player.hexedGridSnapshot[row][column];
	    			
	    			// If the hex also exists in the players memory
	    			// Not sure if I need anything else, so might as well put it up as 
	    			// second argument in above if statment..
	    			if(memoryOfPlayerHex) {
	    				console.log("Recreate: The player and server both have memories!");
	    				// Create a clone of the memory for use in re-locating
	    				var memoryOfPlayerHexClone = new _hex.Hex({
							owner_id: memoryOfPlayerHex.owner_id, 
							troop_count: memoryOfPlayerHex.troop_count,
							info_precision: memoryOfPlayerHex.info_precision,
							building_type: memoryOfPlayerHex.building_type,
							column: memoryOfPlayerHex.column, 
							row: memoryOfPlayerHex.row
						});

	    				// If this used to be my hex, record the change.
	    				if(memoryOfPlayerHex.owner_id == player.player_id) {
	    					console.log("Recreate: The player thinks this is his hex");
			    			// If ownership of this hex has changed
			    			// (likely cause I  got attacked by someone else!)
			    			if (memoryOfServerHex.owner_id != memoryOfPlayerHex.owner_id) {
		    					// change player id to the new id
		    					memoryOfPlayerHexClone.owner_id = memoryOfServerHex.owner_id;
		    					console.log("Recreate: Ownership of this hex has transfered to someone else");

		    					// Generate some oracle logic and add the info to my timeline
		    					console.log("Recreate: Generating oracle knowledge of the new state of affairs..");
		    					var oracleResults = DoOracleCalculations(memoryOfServerHex);

		    					memoryOfPlayerHexClone.troop_count = oracleResults.estimate;
		    					memoryOfPlayerHexClone.info_precision = oracleResults.precision;

		    					// Record it in the players timeline player.hexedTimeline.turns.length-1
        						player.hexedTimeline.turns[player.hexedTimeline.turns.length-1].hexGrid[memoryOfPlayerHexClone.row][memoryOfPlayerHexClone.column] = memoryOfPlayerHexClone;
								player.hexedGridSnapshot[memoryOfPlayerHexClone.row][memoryOfPlayerHexClone.column] = memoryOfPlayerHexClone;
								// Update the hexedGrid as well
        						self.hexedGrid.grid[memoryOfPlayerHexClone.row][memoryOfPlayerHexClone.column] = memoryOfPlayerHexClone;
								console.log("Recreate: The players timeline and hexgrid updated..");
								

		    				// If the hex is still the players, but number of troops changed
		    				}else if(memoryOfPlayerHex.troop_count != memoryOfServerHex.troop_count) {
		    					console.log("Recreate: The number of troops has changed!");
		    					// Overwrite the number of troops
		    					memoryOfPlayerHexClone.troop_count = memoryOfServerHex.troop_count;
		    				
		    					// Record it in the players timeline player.hexedTimeline.turns.length-1
        						player.hexedTimeline.turns[player.hexedTimeline.turns.length-1].hexGrid[memoryOfPlayerHexClone.row][memoryOfPlayerHexClone.column] = memoryOfPlayerHexClone;
								player.hexedGridSnapshot[memoryOfPlayerHexClone.row][memoryOfPlayerHexClone.column] = memoryOfPlayerHexClone;
								// Update the hexedGrid as well
        						self.hexedGrid.grid[memoryOfPlayerHexClone.row][memoryOfPlayerHexClone.column] = memoryOfPlayerHexClone;
								console.log("Recreate: The players timeline and hexgrid updated..");
		    				}

	    				// If this did not use to be the players hex
		    			}else{
		    				// Overwrite the snapshot with the new hex
							// check @TODO above
							player.hexedGridSnapshot[row][column] = memoryOfPlayerHexClone;
		    			}
					}
				}
    		}
    	}
	};

	/**
	 * { function_description }
	 * 
	 * @TODO: 		Skal lige sætte et eller andet report halløj op,
	 * 				som tracker om man har ramt éen eller flere fjender
	 * 				og om man er langt ude i fremtid osv. Sådan man kan
	 * 				forstå hvorfor man har mistet flere tropper end hvad 
	 * 				første angreb forårsagede..
	 * 				
	 * 				Lige nu looper den helt ud i fremtiden, og fjerner alt
	 * 				hukommelse omkring tidligere ejers forhold til en hex.
	 * 				Det er et problem hvis de har gen-erobret den på et tidspunkt
	 * 				i fremtiden. Jeg skal have en måde at huske hvilken form for
	 * 				ændring der er registreret for en hex.
	 * 					Det kan være jeg bare skal have en flag på hver spiller,
	 * 					der fortæller om deres tid er ændret. Så kan den udregne 
	 * 					de nye konsekvenser på næste tur og præsentere det, og undgå
	 * 					at checke hele tidlinjen igennem hver turn :)
	 *
	 * @class      CommencePlayerAttack (name)
	 * @param      {<type>}  player       The player
	 * @param      {<type>}  hex          The hex in current time hexedGrid
	 * @param      {number}  troop_count  The troop count
	 */
	self.CommencePlayerAttack = function(player, hex, troop_count){
		// If the player does not have enough troops for this attack
		if(player.troop_count < troop_count) {
			console.log("Attack: Not enough troops..");
			// Stop the attack
			// Should probably send back some error in socket :)
			return false;
		}

		// If the player is not up to speed with server timeline
		var snapHexReal = {};
		var snapTurn = -1;
		if (player.hexedTimeline.turns.length < self.hexedTimeline.turns.length) {
			console.log("Attack: Locating past hex in server timeline..");
			// Find the snapshotted hex at players time 
			snapHexReal = self.hexedTimeline.SnapshotHexInTimePeriod(hex, 0, player.hexedTimeline.turns.length);
			snapTurn = player.hexedTimeline.turns.length;

		// If the player timeline is at the same turn as the server timeline
		}else{
			console.log("Attack: Using current hexgrid and creating a new turn");
			snapHexReal = hex;
			// Generate a new turn in the turns array
			// Return the new turn/round number (length of array)
			snapTurn = self.hexedTimeline.GenerateNewTurn();
			// Reduce by one to sync with player timeline math and 0-index arrays
			snapTurn--;
		}
		console.log("Real hex: ...");
		console.log(snapHexReal);

		var snapHex = new _hex.Hex({
			owner_id: snapHexReal.owner_id, 
			troop_count: snapHexReal.troop_count,
			building_type: snapHexReal.building_type,
			info_precision: snapHexReal.info_precision,
			column: snapHexReal.column, 
			row: snapHexReal.row
		});

		console.log("Cloned hex: ...");
		console.log(snapHex);
		console.log("Attack: Player_id: " + player.player_id + ", Hex_owner: " + snapHex.owner_id);
		// If the player is not already the owner of this hex
		if(snapHex.owner_id != player.player_id) {
			console.log("Attack: The player does not own this hex..");
			// Generate the new turn (returns length of new timeline)
			var playerNewTurn = player.hexedTimeline.GenerateNewTurn();

			// Do simple combat math
			var battleRemainers = troop_count - snapHex.troop_count;
			console.log("Attack: The remaining troops: "+battleRemainers);

			// If the player won the battle
			if(battleRemainers > 0) {
				console.log("Attack: The player won battle");

				// Transfer the surviving troops to the hex
				player.troop_count -= troop_count;
				snapHex.troop_count = battleRemainers;
				console.log("Attack: Player troops: "+player.troop_count+", Hex: "+snapHex.troop_count);

				// If the owner of this hex is a player
				// neutral = -1
				if(snapHex.owner_id >= 0) {
					console.log("Attack: Previous owner was a player..");
					// Locate the last owner
					var lastOwner = self.debugPlayers[snapHex.owner_id];

					// If this turn is happening in the last owners past
					if(snapTurn < lastOwner.hexedTimeline.turns.length) {
						console.log("Attack: Attack happens in previous owners past");
						// Check if the last owner ever lost this hex in his own future
						var lastRecordedHexTurn = snapTurn;
						// Loop through the future of the last owners timeline
						for (var i = snapTurn; i < lastOwner.hexedTimeline.turns.length; i++) {
							console.log("Attack: Checking round #" + i + " in previous owners past");
							// If the hex exists in the last owner's history
							if(lastOwner.hexedTimeline.turns[i].hexGrid[snapHex.row][snapHex.column]) {
								console.log("Attack: A recorded history was found");
								// If the last owner still had control of the hex
								if(lastOwner.hexedTimeline.turns[i].hexGrid[snapHex.row][snapHex.column].owner_id == lastOwner.player_id) {
									console.log("Attack: Found the hex in previous owners timeline.");
									lastRecordedHexTurn = i;
								}else{
									break;
								}
							}
						}

						// Record the glitch in the owners timeline
						// Remove their ownership for that period in time
						// @NU: Lav en snapHex med oracle detaljer om den nye ejer..
						lastOwner.hexedTimeline.RemoveHexFromPeriodInHistory(snapHex, snapTurn, lastRecordedHexTurn);
						lastOwner.myPastHasChangedByThirdParty = true;
						console.log("Attack: Removed the hex from previous owners timeline");

						// Create an oracle view on the hex's new stats
						var oracleResults = DoOracleCalculations(snapHex);
						var oracleHex = new _hex.Hex({
							troop_count: oracleResults.estimate,
							column: snapHex.column,
							row: snapHex.row,
							owner_id: player.player_id,
							info_precision: oracleResults.precision
						});

						// Add the glitch from oracle perspective to the players own timeline
						lastOwner.hexedTimeline.turns[snapTurn].hexGrid[oracleHex.row][oracleHex.column] = oracleHex;

						// Record the glitch in the servers timeline
						self.hexedTimeline.RemoveHexFromPeriodInHistory(snapHex, snapTurn, lastRecordedHexTurn);
						console.log("Attack: Also removed it from servers timeline...");

					// If this turn is happening at the same turn as the last owners timeline
					}else if(snapTurn == lastOwner.hexedTimeline.turns.length) {

					// If this turn is happening in the last owners future
					}else{

					}
	        		
	        		// var lastOwnerNewTurn = lastOwner.hexedTimeline.GenerateNewTurn();
	        		// lastOwner.hexedTimeline.turns[lastOwnerNewTurn-1].hexGrid[snapHex.row][snapHex.column] = snapHex;

					// Notify the player if they are online
					// 
				}else{
					console.log("Attack: Previous owner was neutral");
				}

				// Set the new owner id for this hex
        		snapHex.owner_id = player.player_id;


				// Go up the chain to future events up to the current timeline
				// to check if this initial amount of troops was enough to ward off any
				// attacks
				// 
				// Means that this past attack intercepted something in the future...
				for (var i = snapTurn+1; i < self.hexedTimeline.turns.length; i++) {
					console.log("Attack: Looking for hex in the future");
					// @TODO: Skal nok lave samme ændring som ved tidligere search på fjender
					var futureHexReal = self.hexedTimeline.SnapshotHexInTimePeriod(hex, snapTurn+1, i);
					// Create blank copy to avoid changing persistent hexes
					var futureHex = new _hex.Hex({
						owner_id: futureHexReal.owner_id, 
						troop_count: futureHexReal.troop_count,
						building_type: futureHexReal.building_type,
						info_precision: futureHexReal.info_precision,
						column: futureHexReal.column, 
						row: futureHexReal.row
					});
					
					// If the ownership of this hex has changed in the future
					if (futureHex.owner_id != player.player_id && futureHex.owner_id != -100) {
						// Get the future owner
						var futureOwner = self.debugPlayers[futureHex.owner_id];
						console.log("Attack: A future hex owner was found");

						// Do simple combat math
						console.log("Attack: Player hex troops: "+snapHex.troop_count+", FutureHex: "+futureHex.troop_count);
						var battleRemainers = snapHex.troop_count - futureHex.troop_count;
						console.log("Attack: The remaining troops in future: "+battleRemainers);

						// If the player won the battle
						if(battleRemainers >= 0) {
							console.log("Attack: Won the battle with future owner");

							// Transfer the surviving troops to the hex
							snapHex.troop_count = (battleRemainers < 0) ? battleRemainers : 1;

							// Check if the last owner ever lost this hex in his own future
							var lastRecordedHexTurn = i;
							// Loop through the future of the last owners timeline
							for (var j = i; j < futureOwner.hexedTimeline.turns.length; j++) {
								// If the hex exists in the last owner's history
								if(futureOwner.hexedTimeline.turns[j].hexGrid[snapHex.row][snapHex.column]) {
									// If the last owner still has control of the hex
									// @TODO: Igen, nok samme ændring :)
									if(futureOwner.hexedTimeline.SnapshotHexInTimePeriod(snapHex, 0, j).owner_id == futureOwner.player_id) {
										console.log("Attack: Found the hex in future owners past timeline.");
										lastRecordedHexTurn = j;
									}else{
										break;
									}
								}
							}

							// Record the glitch in the future owner's timeline
							// Remove their ownership for that period in time
							// i = the turn in this loop roundup
							// @NU: Lav en oracle snap af denne hex..
							futureOwner.hexedTimeline.RemoveHexFromPeriodInHistory(futureHex, i, lastRecordedHexTurn);
							futureOwner.myPastHasChangedByThirdParty = true;

							// Create an oracle view on the hex's new stats
							var oracleResults = DoOracleCalculations(snapHex);
							var oracleHex = new _hex.Hex({
								troop_count: oracleResults.estimate,
								column: snapHex.column,
								row: snapHex.row,
								owner_id: player.player_id,
								info_precision: oracleResults.precision
							});

							// Add the glitch from oracle perspective to the players own timeline
							futureOwner.hexedTimeline.turns[snapTurn].hexGrid[oracleHex.row][oracleHex.column] = oracleHex;

							// Record the glitch in the servers timeline
							self.hexedTimeline.RemoveHexFromPeriodInHistory(snapHex, i, lastRecordedHexTurn);
						

						// If the future hex owner won the battle
						}else{
							console.log("Attack: Did not win battle with future owner..");
							// If future owner won, reduce the surviving troops
							// on this hex for the future owner and report glitch
							// cannot be less than 1 troop remaining
							// (probably I should calculate up the chain as well)
							// Nothing happens to current player
							// Stop the loop
							break;
						}
					}else{
						console.log("Attack: No future owner found for round #" +i);
					}
				}

				// Record it in the players timeline
				console.log(snapHex.row + ", " + snapHex.column);
				console.log(player.hexedTimeline.turns[snapTurn].hexGrid);
        		player.hexedTimeline.turns[snapTurn].hexGrid[snapHex.row][snapHex.column] = snapHex;
        		player.hexedGridSnapshot[snapHex.row][snapHex.column] = snapHex;
				console.log("Attack: The players timeline updated..");


				// Overwrite the hex in the hex grid at this turn in the timeline
        		self.hexedTimeline.turns[snapTurn].hexGrid[snapHex.row][snapHex.column] = snapHex;
        		console.log("Attack: The Servers timeline updated..");

        		// If this turn is the newest turn
        		if(player.hexedTimeline.turns.length == self.hexedTimeline.turns.length) {
        			// Update the hexedGrid as well
        			self.hexedGrid.grid[snapHex.row][snapHex.column] = snapHex;
        			console.log("Attack: This was a new turn");
        		}

			// If the hex occupant won the battle or the battle was a draw/tie
			}else{
				// player.troop_count -= troop_count;
				// 
				// Send troop loss up the chain for this hex
				// @TODO: Skal lige tænke over hvad der så sker..
				// 	Mister man tropper fra reserverne?
				// 	Mister man tropprne på hexen, og hvad så hvis den tidligere
				// 	har været angrebet men man wardede det off???
			}

        // If the player is already the owner of this hex
        }else{
        	// Do nothing or register error or something..
        	// 
        	// 
        }

	};

	self.CommencePlayerOracleCheck = function(player, hex) {
		var snapHexReal = {};
		var snapTurn = -1;
		// If the player is not up to speed with server timeline
		if (player.hexedTimeline.turns.length < self.hexedTimeline.turns.length) {
			console.log("OracleView: Locating past hex in server timeline..");
			// Find the snapshotted hex at players time 
			snapHexReal = self.hexedTimeline.SnapshotHexInTimePeriod(hex, 0, player.hexedTimeline.turns.length);
			snapTurn = player.hexedTimeline.turns.length;

		// If the player timeline is at the same turn as the server timeline
		}else{
			console.log("OracleView: Using current hexgrid and creating a new turn");
			snapHexReal = hex;
			// Generate a new turn in the turns array
			// Return the new turn/round number (length of array)
			snapTurn = self.hexedTimeline.GenerateNewTurn();
			// Reduce by one to sync with player timeline math
			snapTurn--;
		}

		var snapHex = new _hex.Hex({
			owner_id: snapHexReal.owner_id, 
			troop_count: snapHexReal.troop_count,
			building_type: snapHexReal.building_type,
			info_precision: snapHexReal.info_precision,
			column: snapHexReal.column, 
			row: snapHexReal.row
		});

		// If the player is not already the owner of this hex
		if(snapHex.owner_id != player.player_id) {
			console.log("OracleView: This is not my own hex..");

			// Calculate a temporary estimate for the troop count
			// random between 0 and twice the actual number
			var oracleResults = DoOracleCalculations(snapHex);

			// 0 = bad precision
			// 1 = good precision
			// oracleResults.precision

			// Setup the snaphex with our new knowledge
			// Owner_id is already set from server knowledge
			// @todo: skal nok finde en random effekt for dét også..
			snapHex.troop_count = oracleResults.estimate;

			// Jeg skal også gemme precision i hexen..
			snapHex.info_precision = oracleResults.precision;

			// Generate the new turn (returns length of new timeline)
			var playerNewTurn = player.hexedTimeline.GenerateNewTurn();

			// Record it in the players timeline
    		player.hexedTimeline.turns[snapTurn].hexGrid[snapHex.row][snapHex.column] = snapHex;
			player.hexedGridSnapshot[snapHex.row][snapHex.column] = snapHex;
			console.log("OracleView: The players timeline updated..");

			// return {estimate: randEst, precision: estPre};

		// If the player is already the owner of this hex
        }else{
        	// Do nothing or register error or something..
        	// 
        	// 
        }
	};

	/**
	 * Not sure if I'm preventing players from recruiting a last troop
	 * before they get attack the same round.. Maybe I should fetch the snapHex
	 * directly from a players timeline instead?..
	 * 
	 * Actually I am going to do that now. If that isn't working, I'll roll back
	 * to the same way as Attack and Oracle ways of getting snapHex.
	 * 
	 */
	self.CommencePlayerRecruitForHex = function(player, hex) {
		var snapHexReal = {};
		var snapTurn = -1;

		console.log("Recruit: Locating past hex in players timeline..");
		// Find the snapshotted hex at players time 
		snapHexReal = player.hexedGridSnapshot[hex.row][hex.column];
		snapTurn = player.hexedTimeline.turns.length;

		// Clone the hex
		var snapHex = new _hex.Hex({
			owner_id: snapHexReal.owner_id, 
			troop_count: snapHexReal.troop_count,
			building_type: snapHexReal.building_type,
			info_precision: snapHexReal.info_precision,
			column: snapHexReal.column, 
			row: snapHexReal.row
		});

		// If this is the players hex (in his own memory)
		if (snapHex.owner_id == player.player_id) {
			console.log("Recruit: The player owns this hex");
			// Increase the amount of troops on this hex by 1.
			// @TODO: make it work on upgrades from buildings etc.
			// 
			snapHex.troop_count += 1;

			// Generate the new turn (returns length of new timeline)
			var playerNewTurn = player.hexedTimeline.GenerateNewTurn();

			// Record it in the players timeline
    		player.hexedTimeline.turns[snapTurn].hexGrid[snapHex.row][snapHex.column] = snapHex;
			player.hexedGridSnapshot[snapHex.row][snapHex.column] = snapHex;
			console.log("Recruit: Troops increase +1 and players timeline updated..");
		}
	};

	self.CommencePlayerRemoveTroopFromHex = function(player, hex, amount) {
		var snapHexReal = {};
		var snapTurn = -1;

		console.log("RemoveTroops: Locating past hex in players timeline..");
		// Find the snapshotted hex at players time 
		snapHexReal = player.hexedGridSnapshot[hex.row][hex.column];
		snapTurn = player.hexedTimeline.turns.length;

		// Clone the hex
		var snapHex = new _hex.Hex({
			owner_id: snapHexReal.owner_id, 
			troop_count: snapHexReal.troop_count,
			building_type: snapHexReal.building_type,
			info_precision: snapHexReal.info_precision,
			column: snapHexReal.column, 
			row: snapHexReal.row
		});

		// If this is the players hex (in his own memory)
		if (snapHex.owner_id == player.player_id) {
			// If the player has enough troops
			if(snapHex.troop_count > amount) {
				console.log("RemoveTroops: The player owns this hex and it has enough troops");
				// Reduce the amount of troops
				snapHex.troop_count -= amount;
				player.troop_count += amount;

				// Generate the new turn (returns length of new timeline)
				var playerNewTurn = player.hexedTimeline.GenerateNewTurn();

				// Record it in the players timeline
	    		player.hexedTimeline.turns[snapTurn].hexGrid[snapHex.row][snapHex.column] = snapHex;
				player.hexedGridSnapshot[snapHex.row][snapHex.column] = snapHex;
				console.log("RemoveTroops: Troops relocated and players timeline updated");
			}
		}
	};
	self.CommencePlayerAddTroopToHex = function(player, hex, amount) {
		var snapHexReal = {};
		var snapTurn = -1;

		console.log("AddTroops: Locating past hex in the players timeline..");
		// Find the snapshotted hex at players time 
		snapHexReal = player.hexedGridSnapshot[hex.row][hex.column];
		snapTurn = player.hexedTimeline.turns.length;

		// Clone the hex
		var snapHex = CloneHex(snapHexReal);

		// If this is the players hex (in his own memory)
		if (snapHex.owner_id == player.player_id) {
			// If the player has enough troops
			if(player.troop_count >= amount) {
				console.log("AddTroops: The player owns this hex and has enough troops");
				// Reduce the amount of troops
				snapHex.troop_count += amount;
				player.troop_count -= amount;

				// Generate the new turn (returns length of new timeline)
				var playerNewTurn = player.hexedTimeline.GenerateNewTurn();

				// Record it in the players timeline
				player.hexedTimeline.RecordHexInTimeline(snapHex, snapTurn);
				player.myLastAffectedHex = snapHex;
				console.log("AddTroops: Troops relocated and players timeline updated");
			}
		}
	};

	/**
	 * Array of all the actions that has happened on each turn.
	 * 
	 * Total amount of turns this game = compiledTurns.length
	 * 
	 * STRUCTURE
	 * [
	 * 	// Turn 1
	 *   [
	 *     [player_id, GameAction],
	 *     [player_id, GameAction]
	 *   ],
	 *  // Turn 2
	 *   [
	 *     [player_id, GameAction]
	 *   ]
	 * ]
	 */
	self.compiledTurns = [];

	self.DoGameAction = function(player_id, action_type) {
		// Actiontypes kan være "attack", "build" etc.?
	};

	var DoOracleCalculations = function(hex) {
		// Calculate a temporary estimate for the troop count
		// random between 0 and twice the actual number
		var randEst = getRandomIntInclusive(0, hex.troop_count * 2);

		// Calculate a precision on this estimate
		var estDist = CalculateDistanceBetweenNumbers(hex.troop_count, randEst);
		// Limit the estimate within the real range
		// this is probably overkill by a long shot, but meh..
		estDist = (estDist <= hex.troop_count) ? estDist : hex.troop_count;

		// Get a point of how good this precision is
		// 0 = bad precision
		// 1 = good precision
		var estPre = ConvertDistanceValueToPrecision(hex.troop_count, estDist);

		return {
			estimate: randEst,
			precision: estPre
		};
	}

	// Returns a random integer between min (included) and max (included)
	// Using Math.round() will give you a non-uniform distribution!
	var getRandomIntInclusive = function(min, max) {
	  min = Math.ceil(min);
	  max = Math.floor(max);
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	var CalculateDistanceBetweenNumbers = function(base, check) {
	  if(check > 0) {
	    if(check < base) {
	      return base % check;
	    }else if(check > base) {
	      var divided = Math.floor((check - base) / base);
	      var conquered = divided * base;

	      return (check % base) + conquered;
	    }else{
	      return 0;
	    }
	  }else{
	    return base;
	  }
	};

	/**
	 * Lige nu er precision en value mellem 0 & 1 som viser opacity
	 * 
	 * @TODO: Skal gøres meget bedre ;)
	 *
	 * @class      ConvertDistanceValueToPrecision (name)
	 * @param      {number}  base      The base
	 * @param      {number}  distance  The distance
	 * @return     {number}  { description_of_the_return_value }
	 */
	var ConvertDistanceValueToPrecision = function(base, distance) {
		// If the distance is 0, return full precision
		if(distance === 0) {
			return 1;
		}
		var decimal = distance / base;
		var opposite = 1 - decimal;
		var precision = 0;

		precision = (opposite > 0.05) ? opposite : 0.05;

		return precision;
	};

	var CloneHex = function (hex) {
		// Clone the hex
		var clonedHex = new _hex.Hex({
			owner_id: clonedHex.owner_id, 
			troop_count: clonedHex.troop_count,
			building_type: clonedHex.building_type,
			info_precision: clonedHex.info_precision,
			column: clonedHex.column, 
			row: clonedHex.row
		});

		return clonedHex;
	};
};

exports.HexGame = HexGame;