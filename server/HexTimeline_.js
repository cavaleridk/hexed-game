var _hex = require('./Hex.js');

var HexTimeline = function(options){
	var self = this;
	self.options = options || {};
	self.IAmServer = self.options.server || false;

	self.columnCount = self.options.columns || 5;
	self.rowCount = self.options.rows || 9;

	self.turns = self.options.turns || [];

	// hexgriddet her skal hexene være null hvis ingen ændringer er sket på dem
	self.GenerateNewTurn = function(){
		var turn = {hexGrid: [], playerActions: []};

		turn.hexGrid = GenerateEmptyHexGrid(self.rowCount, self.columnCount);

		return self.turns.push(turn);
	};

	self.RecordHexInTimeline = function(hex, turn) {
		self.turns[turn].hexGrid[hex.row][hex.column].push(hex);
	};

	self.SnapshotHexGridAtTurn = function(turn_number){
		// Create an empty grid
		var grid = GenerateEmptyHexGrid(self.rowCount, self.columnCount);

	    // TurnNumberToSnapshot = dét antal turns man vil se frem til
	    for(var i = 0; i <= turn_number; i++) {
	    	var turn = self.turns[i];
	    	// If this turn has been calculated
	    	if(turn) {
		    	for (var row = 0; row < turn.hexGrid.length; row++) {
		    		for (var column = 0; column < turn.hexGrid[row].length; column++) {
		    			// For every time this hex has been changed this turn
			    		for (var changeKey = 0; changeKey < turn.hexGrid[row][column].length; changeKey++) {
							// Overwrite the snapshot with the new hex status
							grid[row][column] = turn.hexGrid[row][column][changeKey];
						}
		    		}
		    	}
		    // If this is the last/current round
		    }else{
		    	console.log("SnapshotHexGridAtTurn: THIS IS THE LAST SERVER ROUND?: #" + i);
		    	break;
		    }
	    }

	    return grid;
	};

	// @TODO: Convert to for(){} loops for optimisation
	self.SnapshotHexInTimePeriod = function(hex, starting_turn_number, ending_turn_number){
		console.log('SnapshotHexInTimePeriod: Started..');
		var snappedHex = null;
		// For every turn up to the desired point
		// (increasing turn_number to be an inclusive search)
		ending_turn_number++;
		for(var i = starting_turn_number; i < ending_turn_number; i++) {
			// Set the hex to the latest memory or null if no memory this turn
			// if (! length <= 0) {overwrite;}
			snappedHex = hexMemory[hexMemory.length - 1] || snappedHex;

			// // For every time this hex has been changed this turn
   			//	for (var changeKey = 0; changeKey < self.turns[i].hexGrid[hex.row][hex.column].length; changeKey++) {
			// 	// Overwrite the snapshot with the new hex status
			// 	snappedHex = self.turns[i].hexGrid[hex.row][hex.column];
			 	console.log('SnapshotHexInTimePeriod: Found my hex!');
			// }
		}
		console.log('SnapshotHexInTimePeriod: Done looping..');
		// If the hex has not been affected by anything yet
		if (snappedHex == null) {
			console.log('SnapshotHexInTimePeriod: I did not find any hex so I generate one');
			// Generate a new copy of the hex with the same starting data
			// @TODO: Need some kind of random generation logic for starting troops etc.
			// 	. midligertidigt giver jeg den 0 troops til alle..
			if (self.IAmServer) {
				snappedHex = new _hex.Hex({column: hex.column, row: hex.row, troop_count: 0});
			}else{
				snappedHex = new _hex.Hex({column: hex.column, row: hex.row});
			}
		}

		return snappedHex;
	};
	


	/**
	 * Removes a hexadecimal from period in history.
	 * 
	 * Deprecated. We aren't removing anything from history no more
	 *
	 * @class      RemoveHexFromPeriodInHistory (name)
	 * @param      {<type>}  hex                   The hexadecimal
	 * @param      {<type>}  starting_turn_number  The starting turn number (inclusive)
	 * @param      {number}  ending_turn_number    The ending turn number (exclusive)
	 */
	self.RemoveHexFromPeriodInHistory = function(hex, starting_turn_number, ending_turn_number) {
		for (var i = starting_turn_number; i <= ending_turn_number; i++) {
			// If the hex exists in history
			if(self.turns[i].hexGrid[hex.row][hex.column]) {
				console.log("RemoveHexFromPeriodInHistory: Found and removed at turn #" + i);
				// Overwrite the snapshot with the new hex status
				self.turns[i].hexGrid[hex.row][hex.column] = null;
			}
		}
	}

};

function GenerateEmptyHexGrid(rows, columns){
	var grid = [];
	for (var i = 0; i < rows; i++) {
		var aRow = [];
		for (var j = 0; j < columns; j++) {
			// Lav null hexes
			var hex = [];
			aRow.push(hex);
		}
		grid.push(aRow);
	}
	return grid;
};

exports.HexTimeline = HexTimeline;