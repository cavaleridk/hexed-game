var _hexGrid = require('./HexGrid.js');


var Player = function(options){
	var self = this;
	self.options = options || {};

	self.socket = self.options.socket || null;
	self.player_id = (self.options.player_id >= 0) ? self.options.player_id : -100;
	self.player_name = self.options.name || 'DefaultPlayerName';
	self.troop_count = self.options.troop_count || 10;

	self.hexedGrid = new _hexGrid.HexGrid({columns: self.options.grid.columns, rows: self.options.grid.rows});


	self.resources = self.options.starting_resources || {
		food: 25,
		wood: 100,
		stone: 100,
		iron: 0
	};

};

exports.Player = Player;