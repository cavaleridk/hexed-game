var _hexTimeline = require('./HexTimeline.js');


var Player = function(options){
	var self = this;
	self.options = options || {};

	self.player_id = (self.options.player_id >= 0) ? self.options.player_id : -100;
	self.player_name = self.options.name || 'DefaultPlayerName';

	self.troop_count = self.options.troop_count || 10;

	self.hexedTimeline = new _hexTimeline.HexTimeline({});
	self.ReturnNewestTurnIndex = function () {
		var index = (self.hexedTimeline.length < 0) ? self.hexedTimeline.length-1 : 0;

		return index;
	};

	self.hexedGridSnapshot = self.hexedTimeline.SnapshotHexGridAtTurn(0);

	self.myLastAffectedHex = null;
	self.myGlitchHistory = [];
	self.myPastHasChangedByThirdParty = false;

	self.ReturnClientFriendlyGrid = function(){

		return self.hexedGridSnapshot;
	}

};

exports.Player = Player;