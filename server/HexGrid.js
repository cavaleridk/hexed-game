var linq = require('linq');
var _hex = require('./Hex.js');

var HexGrid = function(options){
	var self = this;
	self.options = options || {};

	self.columnCount = self.options.columns || 5;
	self.rowCount = self.options.rows || 9;

	self.IAmServer = self.options.is_server || false;

	/**
	 * The following settings are hardcoded in functions below
	 * 		ResourceChanceIron = 40,
	 * 		ResourceChanceStone = 60,
	 * 		ResourceChanceWood = 100
	 */
	self.gridSettings = {
		ChanceForSea : 14,
		SeaSizeMultiplier : 3.5,
		ChanceForResource : 35
	};
	self.gridSettings.ChanceForSea = self.options.ChanceForSea || self.gridSettings.ChanceForSea;
	self.gridSettings.SeaSizeMultiplier = self.options.SeaSizeMultiplier || self.gridSettings.SeaSizeMultiplier;
	self.gridSettings.ChanceForResource = self.options.ChanceForResource || self.gridSettings.ChanceForResource;
	self.gridSettings.ResourceChanceIron = self.options.ResourceChanceIron || self.gridSettings.ResourceChanceIron;
	self.gridSettings.ResourceChanceStone = self.options.ResourceChanceStone || self.gridSettings.ResourceChanceStone;
	self.gridSettings.ResourceChanceWood = self.options.ResourceChanceWood || self.gridSettings.ResourceChanceWood;
	self.gridSettings.OffsetRows = 0;
	self.gridSettings.OffsetColumns = 0;


   // HexGrid[
   //    Row0[
   //      Hex0{owner_id: 1, troops: 7, building: null},
   //      Hex1{owner_id: 2, troops: 25, building: 'house'},
   //      Hex2{owner_id: null(neutral), troops: 2, building: null},
   //    ],
   //    Row1[Hex0{},Hex1{},Hex2{}],
   //    Row2[Hex0{},Hex1{},Hex2{}]
   //  ]
	self.grid = (self.IAmServer) ? GenerateHexGrid(self.rowCount, self.columnCount, self.gridSettings.ChanceForResource) : GenerateBlankGrid(self.rowCount, self.columnCount);

	self.GenerateBlankGrid = function() {
		return GenerateBlankGrid(self.rowCount, self.columnCount);
	};

	self.CopyResourcesFromGrid = function (grid) {
		for (var i = 0; i < self.rowCount; i++) {
			for (var j = 0; j < self.columnCount; j++) {
				if(Object.keys(self.grid[i][j]).length > 0) {
					// IF we have any vision on this hex
					console.log("This hex is known!");
					self.grid[i][j].main_resource = grid[i+self.gridSettings.OffsetRows][j+self.gridSettings.OffsetColumns].main_resource;
				}
			}
		}
	};
	self.ExistsInGrid = function(hex) {
		var r = hex.row;
		var c = hex.column;
		r -= self.gridSettings.OffsetRows;
		c -= self.gridSettings.OffsetColumns;

		if(self.grid[r][c] !== -1) {
			return true;
		}else{
			return false;
		}
	}

	self.FindSurroundingHexes = function(hex) {
	    return [
		    self.ReturnSelf(hex),
		    self.ReturnTopLeft(hex),
		    self.ReturnTop(hex),
		    self.ReturnTopRight(hex),
		    self.ReturnBottomRight(hex),
		    self.ReturnBottom(hex),
		    self.ReturnBottomLeft(hex)
	    ];
  	};
  	self.ReturnSelf = function(hex){
    	return hex;
  	};
	self.ReturnTopLeft = function(hex){
		var row = hex.row;
		// Account for every other column being displaced
		var col = hex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;
		if(row % 2 !== 0) {
			col--;
		}

		if(row > 0) {
			return self.grid[row-1][col] || {};
		}else{
			return {};
		}
		
	};
	self.ReturnTop = function(hex){
		var row = hex.row;
		var col = hex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;

		if(row > 1) {
			return self.grid[row-2][col] || {};
		}else{
			return {};
		}

		
	};
	self.ReturnTopRight = function(hex){
		var row = hex.row;
		// Account for every other column being displaced
		var col = hex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;
		if(row % 2 !== 0) {
			col--;
		}

		if(row > 0) {
			return self.grid[row-1][col+1] || {};
		}else{
			return {};
		}
		
	};
	self.ReturnBottomRight = function(hex){
		var row = hex.row;
		// Account for every other column being displaced
		var col = hex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;
		if(row % 2 !== 0) {
			col--;
		}

		if(row < self.grid.length -1) {
			return self.grid[row+1][col+1] || {};
		}else{
			return {};
		}
		
	};
	self.ReturnBottom = function(hex){
		var row = hex.row;
		var col = hex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;

		if(row < self.grid.length -2) {
			return self.grid[row+2][col] || {};
		}else{
			return {};
		}
		
	};
	self.ReturnBottomLeft = function(hex){
		var row = hex.row;
		// Account for every other column being displaced
		var col = hex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;
		if(row % 2 !== 0) {
			col--;
		}

		if(row < self.grid.length -1) {
			return self.grid[row+1][col] || {};
		}else{
			return {};
		}
		
	};
	self.FindHexInGrid = function(hex) {
		var row = hex.row;
		var col = hex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;

		if(self.grid[row]) {
			if (typeof self.grid[row][col] !== 'undefined') {
				return self.grid[row][col];
			}else{
				return null;
			}
		}else{
			return null;
		}
	};
	self.CheckIfRowExists = function(row_number) {
		row_number -= self.gridSettings.OffsetRows;

		if(self.grid[row_number]) {
			return true;
		}else{
			return false;
		}
	}
	self.ReplaceHexInGrid = function(newHex, insert_nothing) {
		if (typeof insert_nothing === 'undefined') {
			insert_nothing = false;
		}

		var row = newHex.row;
		var col = newHex.column;
		row -= self.gridSettings.OffsetRows;
		col -= self.gridSettings.OffsetColumns;

		// If this is a new row at the start of the array
		if(row < 0) {
			var diff = Math.abs(row);
			if(diff % 2 !== 0 && self.grid % 2 !== 0) {diff++};
			console.log("CREATING NEW ROWS WITH UNSHIFT " + diff);

			for (var i = 0; i < diff; i++) {
				var aRow = [];
				for (var j = 0; j < self.columnCount; j++) {
					aRow.push(-1);
				}
				self.grid.unshift(aRow);

				row++;
				self.rowCount++;
				self.gridSettings.OffsetRows--;
			}

			if(diff % 2 !== 0) {
				var aRow = [];
				for (var j = 0; j < self.columnCount; j++) {
					aRow.push(-1);
				}
				self.grid.unshift(aRow);
				self.rowCount++;
				self.gridSettings.OffsetRows--;
			}
		}else if(row >= self.rowCount) {
			// rowCount-1 to account for 0-index in grid array
			var diff = Math.abs(row - (self.rowCount-1));
			console.log("CREATING NEW ROWS WITH PUSH " + diff);

			for (var i = 0; i < diff; i++) {
				var aRow = [];
				for (var j = 0; j < self.columnCount; j++) {
					aRow.push(-1);
				}
				self.grid.push(aRow);

				self.rowCount++;
			}
		}

		// If this is a new column at the start of the row
		if(col < 0) {
			var diff = Math.abs(col);
			console.log("CREATING NEW COLS WITH UNSHIFT " + diff);

			for (var i = 0; i < self.rowCount; i++) {
				var aRow = self.grid[i];
				for (var j = 0; j < diff; j++) {
					aRow.unshift(-1);
				}
			}

			col += diff;
			self.columnCount += diff;
			self.gridSettings.OffsetColumns -= diff;

		//else if this is a new column and the end of a row
		}else if(col >= self.columnCount) {
			// columnCount-1 to account for 0-index in grid array
			var diff = Math.abs(row - (self.columnCount-1));
			console.log("CREATING NEW COLS WITH PUSH " + diff);

			for (var i = 0; i < self.rowCount; i++) {
				var aRow = self.grid[i];
				for (var j = 0; j < diff; j++) {
					aRow.push(-1);
				}
			}

			self.columnCount += diff;
		}

		if(insert_nothing) {
			self.grid[row][col] = -1;
		}else{
			self.grid[row][col] = newHex;
		}
	};

	self.ReturnHexesByOwnerID = function(ownerID) {
		var associatedHexes = [];
		// For each row in the grid
		for (var row = 0; row < self.grid.length; row++) {
			// For each column in the row
    		for (var column = 0; column < self.grid[row].length; column++) {
    			// If this hex is by the right owner
    			if(self.grid[row][column].owner_id == ownerID) {
    				// Store the hex in the associated array
    				associatedHexes.push(self.grid[row][column]);
    			}
    		}
    	}

    	return associatedHexes;
	};
	self.ReturnEvenHexesWithoutResources = function(){
		var emptyHexes = [];

		for (var i = 0; i < self.grid.length; i++) {
			if(i % 2 === 0) {
				for (var j = 0; j < self.grid[i].length; j++) {
					if(self.grid[i][j].main_resource || self.grid[i][j].owner_id >= 0) {
						emptyHexes.push(CloneHex(self.grid[i][j]));
					}
				}
			}
		}

		return emptyHexes;
	}
	
};

function GenerateHexGrid(rows, columns, chanceForResource){
	var grid = [];
	for (var i = 0; i < rows; i++) {
		var aRow = [];
		for (var j = 0; j < columns; j++) {
			// If this hex should not have a resource
			if( Math.floor(Math.random() * (100 - 0 + 1) + 0) > chanceForResource ) {
				// Lav hexes med samme default settings
				var hex = new _hex.Hex({column: j, row: i, troop_count: 0}); // @TODO: Hexes skal kunne genereres med samme random data(troops etc)
				aRow.push(hex);

			// If this hex should have a resource
			}else{
				// Lav hexes med samme default settings
				var randResource = PickRandomResource();
				var hex = new _hex.Hex({column: j, row: i, troop_count: 0, main_resource: randResource}); // @TODO: Hexes skal kunne genereres med samme random data(troops etc)
				aRow.push(hex);
			}

			
		}
		grid.push(aRow);
	}
	return grid;
};
function GenerateBlankGrid(rows, columns) {
	var grid = [];
	for (var i = 0; i < rows; i++) {
		var aRow = [];
		for (var j = 0; j < columns; j++) {
			aRow.push(-1);
		}
		grid.push(aRow);
	}
	return grid;
};
function PickRandomResource() {
	var chanceForIron = 20;
	var chanceForStone = 40;
	// var chanceForWood = 100;

	// If this hex should not have iron
	if( Math.floor(Math.random() * (100 - 0 + 1) + 0) > chanceForIron ) {
		// If the hex should not have stone
		if( Math.floor(Math.random() * (100 - 0 + 1) + 0) > chanceForStone ) {
			// Default to wood
			return "Wood";

		// If the hex should have stone
		}else{
			return "Stone";
		}

	// If the hex should have iron
	}else{
		return "Iron";
	}
}
function CloneHex (hex) {
		// Clone the hex
		var clonedHex = new _hex.Hex({
			owner_id: hex.owner_id, 
			troop_count: hex.troop_count,
			building_type: hex.building_type,
			info_precision: hex.info_precision,
			column: hex.column, 
			row: hex.row
		});

		return clonedHex;
	};

exports.HexGrid = HexGrid;