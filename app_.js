var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongo = require('mongodb');
var mongoose = require('mongoose');

var routes = require('./routes/index');
var users = require('./routes/users');
var playingfield = require('./routes/playingfield');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var _HexGame = require('./server/HexGame.js');
var HexGame = new _HexGame.HexGame({});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/play', playingfield);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


// 
// Hver socket er en spiller
// 
// Kan lave socket.player_id for at skelne
io.on('connection', function(socket){
    console.log('client connected');

    // Set playerID
    socket.player = HexGame.debugPlayers[0];

    // socket.on('hexGame', function(hexGame){

    //     _hexGame = hexGame;
    //     io.emit('hexGame', _hexGame);
    // });

    socket.on('ClientAttackHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      console.log(msgObj);
      
      var theHex = HexGame.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];
      
      // If this hex is not owned by the player
      // if(theHex.owner_id != socket.player.player_id) {
      //   theHex.owner_id = socket.player.player_id;

        // Calculate combat stuff
        // Change hexgrid, update timelines
        console.log(theHex);
        HexGame.CommencePlayerAttack(socket.player, theHex, msgObj.troop_count);


        // Execute things that other players did this turn
        // 
        HexGame.RecreatePastEventsForPlayer(socket.player);

        // Send details about the hex back
        socket.emit('ClientTurnExecutedResponse', JSON.stringify({
          player : {
            troop_count: socket.player.troop_count,
            new_turn: socket.player.hexedTimeline.turns.length
          },
          hexGridData: socket.player.hexedGridSnapshot,
          mainAffectedHex: {column: theHex.column, row: theHex.row}
        }));
      // }
    });
    socket.on('ClientOracleHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var theHex = HexGame.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];

      HexGame.CommencePlayerOracleCheck(socket.player, theHex);

      // Execute things that other players did this turn
      // 
      HexGame.RecreatePastEventsForPlayer(socket.player);

      // Send details about the hex back
      socket.emit('ClientTurnExecutedResponse', JSON.stringify({
        player : {
          troop_count: socket.player.troop_count,
          new_turn: socket.player.hexedTimeline.turns.length
        },
        hexGridData: socket.player.hexedGridSnapshot,
        mainAffectedHex: {column: theHex.column, row: theHex.row}
      }));

    });
    socket.on('RecruitTroopsForHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var theHex = HexGame.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];

      HexGame.CommencePlayerRecruitForHex(socket.player, theHex);

      // Execute things that other players did this turn
      // 
      HexGame.RecreatePastEventsForPlayer(socket.player);

      // Send details about the hex back
      socket.emit('ClientTurnExecutedResponse', JSON.stringify({
        player : {
          troop_count: socket.player.troop_count,
          new_turn: socket.player.hexedTimeline.turns.length
        },
        hexGridData: socket.player.hexedGridSnapshot,
        mainAffectedHex: {column: theHex.column, row: theHex.row}
      }));

    });
    socket.on('RemoveTroopsFromHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var theHex = HexGame.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];

      HexGame.CommencePlayerRemoveTroopFromHex(socket.player, theHex, msgObj.amount);

      // Execute things that other players did this turn
      // 
      HexGame.RecreatePastEventsForPlayer(socket.player);

      // Send details about the hex back
      socket.emit('ClientTurnExecutedResponse', JSON.stringify({
        player : {
          troop_count: socket.player.troop_count,
          new_turn: socket.player.hexedTimeline.turns.length
        },
        hexGridData: socket.player.hexedGridSnapshot,
        mainAffectedHex: {column: theHex.column, row: theHex.row}
      }));

    });
    socket.on('AddTroopsFromHexRequest', function(message) {
      var msgObj = JSON.parse(message);
      
      // msgObj.hex.r & .c
      var theHex = HexGame.hexedGrid.grid[msgObj.hex.r][msgObj.hex.c];

      HexGame.CommencePlayerAddTroopToHex(socket.player, theHex, msgObj.amount);

      // Execute things that other players did this turn
      // 
      // HexGame.RecreatePastEventsForPlayer(socket.player);
      HexGame.CheckPlayerForGlitch(socket.player, HexGame.CommencePlayerAddTroopToHex);

      // Send details about the hex back
      socket.emit('ClientTurnExecutedResponse', JSON.stringify({
        player : {
          troop_count: socket.player.troop_count,
          new_turn: socket.player.hexedTimeline.turns.length
        },
        hexGridData: socket.player.hexedGridSnapshot,
        mainAffectedHex: {column: theHex.column, row: theHex.row}
      }));

    });



    socket.on('DebugChangePlayerMessage', function(message) {
      var msgObj = JSON.parse(message);
      if([0, 1, 2].indexOf(msgObj.player_id) > -1) {
        socket.player = HexGame.debugPlayers[msgObj.player_id];
        console.log("Changed to player #" + socket.player.player_id);

        // Send details about player change
        io.emit('DebugChangePlayerResponse', JSON.stringify({
          player : {
            player_id: socket.player.player_id,
            troop_count: socket.player.troop_count,
            hexedGridSnapshot: socket.player.hexedGridSnapshot,
            new_turn: socket.player.hexedTimeline.turns.length
            // ELLER ReturnClientFriendlyGrid
            // Måske jeg bare kun gemmer relevant info i snapshottet..
          },
          server : {
            new_turn: HexGame.hexedTimeline.turns.length
          }
        }));
      }
    });
    socket.on('DebugGetServerHexedGrid', function(message) {
      var msgObj = JSON.parse(message);        

      // Send details about player change
      io.emit('DebugGetServerHexedGridResponse', JSON.stringify({
        hexedGrid : HexGame.hexedGrid.grid
      }));
    });
    socket.on('DebugGetServerTimeline', function(message) {
      var msgObj = JSON.parse(message);        

      // Send details about player change
      io.emit('DebugGetServerTimelineResponse', JSON.stringify({
        TimelineTurns : HexGame.hexedTimeline.turns
      }));
    });
    socket.on('DebugGetServerSnaphot', function(message) {
      var msgObj = JSON.parse(message);        

      // Send details about player change
      io.emit('DebugGetServerSnaphotResponse', JSON.stringify({
        hexedGrid : HexGame.hexedTimeline.SnapshotHexGridAtTurn(msgObj.turn)
      }));
    });


    // Send details about the game on load..
    io.emit('ClientConnectResponse', JSON.stringify({
      hexMap : {
        columns: HexGame.columnCount,
        rows: HexGame.rowCount
      },
      player : {
        player_id: socket.player.player_id,
        troop_count: socket.player.troop_count,
        hexedGridSnapshot: socket.player.hexedGridSnapshot,
        new_turn: socket.player.hexedTimeline.turns.length
        // ELLER ReturnClientFriendlyGrid
        // Måske jeg bare kun gemmer relevant info i snapshottet..
      }
    }));
});


app.set('port', (process.env.PORT || 3000));
http.listen(app.get('port'), function(){
  console.log('Server started on port '+app.get('port'));
});


module.exports = app;
