/***************************
This is JavaScript (JS), the programming language that powers the web (and this is a comment, which you can delete).

To use this file, link it to your markup by placing a <script> in the <body> of your HTML file:

  <body>
    <script src="script.js"></script>

replacing "script.js" with the name of this JS file.

Learn more about JavaScript at

https://developer.mozilla.org/en-US/Learn/JavaScript
***************************/


/*
 *        **** TODO / Tanker **** 
 *

 får ikke fjernet hex-spin, når man skal klikke??

 *   *** Generelt ***
 *
 * Griddet skal nok have 1/2 extra hexes
 * i hver kant som er dækket af noget
 * fog of war lignende..
 *  Dvs. at index 0 skal være dummy hexes
 *    samme for sidste index..
 *    både i rækker og kolonner
 *
 *   *** Animations ***
 *
 * Der er stadig noget bug-værk over de der hex-spin
 *  Skal fjerne timeouts fra alle de hexes som bliver
 *  påvirket når man klikker.
 *
 *   *** Ny idé?? ***
 * Lav flip effekt rundt omkring den hex man klikker
 * med en function (int count) som tæller hvor mange
 * hexes der skal flippes.
 *
 * NEDENSTÅENDE MANGLER STADIG
 *
 * Skrive setTimeout brugen om, sådan at jeg altid
 * fjerner den timeout som er igang, når en ny effekt
 * skal overwrite.
 *  Måske der kommer nogle problemer med flip, hvor
 *  tager udgangspunkt i en vinkel som ikke er lige?
*/
var _hexedHelper = require("./HexedHelper.js");
var HexedHelper = new _hexedHelper();

window.onload = function () {

  var hex = document.getElementsByClassName('hex');

  $('#startup-modal').openModal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    in_duration: 300, // Transition in duration
    out_duration: 200, // Transition out duration
    starting_top: '4%', // Starting top style attribute
    ending_top: '10%', // Ending top style attribute
    ready: function() {}, // Callback for Modal open
    complete: function() { HexedHelper.MySocket.SendJoinTheActionRequest(); } // Callback for Modal close
  });

  // document.getElementById('player0').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.CloseHexedMenuCaller();
  //   HexedHelper.MySocket.SendDebugChangePlayerRequest(0);
  // }, false);
  // document.getElementById('player1').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.CloseHexedMenuCaller();
  //   HexedHelper.MySocket.SendDebugChangePlayerRequest(1);
  // }, false);
  // document.getElementById('player2').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.CloseHexedMenuCaller();
  //   HexedHelper.MySocket.SendDebugChangePlayerRequest(2);
  // }, false);

  // document.getElementById('getHexGrid').addEventListener('click', function () {
  //   HexedHelper.MySocket.SendDebugGetServerHexedGrid();
  // }, false);
  // document.getElementById('getTimeline').addEventListener('click', function () {
  //   HexedHelper.MySocket.SendDebugGetServerTimeline();
  // }, false);
  // document.getElementById('getSnapshot').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.ShowSweetPromptWindow("Get server snapshot", 
  //     "Which turn do you need to check?", 
  //     "Turn number", 
  //     function(inputVal){
  //       // If this is not a anumber
  //       if(! /^\d+$/.test(inputVal)) {
  //         swal.showInputError("Please enter a number.");     
  //         return false  
  //       }
  //       var inputNum = parseInt(inputVal);

  //       // Close the prompt window
  //       swal.close();
        
  //       HexedHelper.MySocket.SendDebugGetServerSnaphot(inputNum);
        
  //     }.bind(this)
  //   );
  // }, false);

  // [].forEach.call(document.querySelectorAll('.hex'), function (el) {

  //   el.addEventListener('mouseover', function () {
  //     if (!el.classList.contains('hex-green')) {
  //       el.classList.add('hex-green');
  //     }
  //   }, false);

  //   el.addEventListener('mouseleave', function () {
  //     if (el.classList.contains('hex-green')) {
  //       el.classList.remove('hex-green');
  //     }
  //   }, false);
    
  //   HexedHelper.AddListenerToHex(el, 'click', HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
    
  //   el.addEventListener('webkitAnimationEnd', function () {
  //     HexedHelper.RemoveClass(el, 'hex-spin');
  //   }, false);
  //   el.addEventListener('animationend', function () {
  //     HexedHelper.RemoveClass(el, 'hex-spin');
  //   }, false);
  // });
};
//# sourceMappingURL=menu.js.map


