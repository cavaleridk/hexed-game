
var HTMLGenerator = function(HexedHelper) {
	var self = this;

	self.AddIconToHex = function(el, iconName, spin = false) {
		var div = HexedHelper.HTMLGenerator.MenuCreateFullContainerDiv();

		var icon;
		if(spin) {
			icon = HexedHelper.HTMLGenerator.MenuCreateIcon(iconName);
			icon.classList.add('fa-spin');
		}else{
			icon = HexedHelper.HTMLGenerator.MenuCreateGameIcon(iconName);
		}
		div.appendChild(icon);
		el.appendChild(div);
	};
	/**
	 * StatsArray kan kun tage 2 ikoner lige nu..
	 * 	Skal rename ting og måske gøre mere versatil
	 * 	@TODO: Dét ^
	 *
	 * @class      AddStatsDivToHex (name)
	 * @param      {<type>}  el          { parameter_description }
	 * @param      {<type>}  statsArray  The statistics array
	 */
	self.AddStatsDivToHex = function(el, statsArray) {
		console.log(el.menuOldStructure);
		var div = HexedHelper.HTMLGenerator.MenuCreateContainerDiv();
		div.classList.add('hex-menu-item-container');

		for (var i = 0; i < statsArray.length; i++) {
			var _p = document.createElement('p');
			var _icon = document.createElement('img');
			var _text = document.createElement('span');
			_p.classList.add('hex-menu-item-paragraph');
			_icon.classList.add('responsive-img');
			_icon.classList.add('hex-menu-icon-small');
			_icon.src = "/hexed_images/"+statsArray[i].icon+".svg";
			_text.classList.add('hex-menu-item-text');

			var theHex = HexedHelper.hexedGridSnapshot[el.r][el.c];

			// Set troop text if not empty hex (aka if we know anything about it)
			if(! (Object.keys(theHex).length <= 3 && theHex.constructor === Object)) {
				// Show a parenthesis around troop count if there is any uncertainty with amounts
				_text.innerHTML = statsArray[i].populate(theHex);
				console.log("Precision / Opacity = " + theHex.info_precision);
				// Set opacity of the stats div based on precision, and default to full
				// opacity when no info is available
				_text.style.opacity = theHex.info_precision.toString();
			}else{
				_text.innerHTML = '???';
			}

			_p.appendChild(_icon);
			_p.appendChild(_text);
			div.appendChild(_p);
		}

		el.appendChild(div);
	};


	self.RemoveHTMLRecursively = function (el, removeSelf = false){
		while (el.hasChildNodes()) {
			el.removeChild(el.lastChild);
		}
		if(removeSelf){
			el.parentNode.removeChild(el);
		}
	};
	self.MenuCreateContainerDiv = function() {
		var div = document.createElement('div');
		div.className = 'hex-menu-item-container';

		return div;
	};
	self.MenuCreateFullContainerDiv = function() {
		var div = document.createElement('div');
		div.className = 'hex-menu-item-container-full';

		return div;
	};
	self.MenuCreateIcon = function(iconName) {
		var icon = document.createElement('i');
		icon.classList.add('fa');
		icon.classList.add(iconName);
		icon.classList.add('fa-3x');

		return icon;
	};
	self.MenuCreateGameIcon = function(iconName) {
		// var icon = document.createElement('i');
		// icon.classList.add('fa');
		// icon.classList.add(iconName);
		// icon.classList.add('fa-3x');
		var icon = document.createElement('img');
		icon.classList.add('responsive-img');
		icon.src = "/hexed_images/"+iconName+".svg";

		return icon;
	};




    
    
    
    
	// Also adds the border hexes that are not interactable by default
	self.GenerateHexMap = function (snapshot) {
		var rows = snapshot.length;
		var columns = snapshot[0].length;
		var gridDivTop = HexedHelper.hexedGridTop;
		var gridDivBottom = HexedHelper.hexedGridBottom;
		var gridDiv = HexedHelper.hexedGrid;
		var innerContainer = document.getElementById("hexedinnercontainer");

		self.RemoveHTMLRecursively(gridDivTop);
		self.RemoveHTMLRecursively(gridDivBottom);
		self.RemoveHTMLRecursively(gridDiv);

		// Increase size of container based on hex size
		// current size 60 + 124 width
		console.log("Container width: " + ((60+124) * columns));
		innerContainer.style.width = ((60+124) * (columns+1)) + "px";

		// Add the top and bottom border hexes
		// (2 rows in each end)
		for (var i = 0; i < 2; i++) {
			// Add to top
			var rowDivTop = self.CreateHexRow();
			for (var j = 0; j < columns +1; j++) {
				var hexDiv = self.CreateHexBorder();

				hexDiv.c = j;
				hexDiv.r = i;
				hexDiv.menuParent = null;
				AddInitialFakeHexListenerSetup(hexDiv);

				rowDivTop.appendChild(hexDiv);
			}
			gridDivTop.appendChild(rowDivTop);

			// add to bottom
			var rowDivBottom = (rows % 2 !== 0) ? self.CreateHexRowBorderBottom() : self.CreateHexRow();
			for (var k = 0; k < columns +1; k++) {
				var hexDiv = self.CreateHexBorder();

				hexDiv.c = k;
				hexDiv.r = i;
				hexDiv.menuParent = null;
				AddInitialFakeHexListenerSetup(hexDiv);

				rowDivBottom.appendChild(hexDiv);
			}
			gridDivBottom.appendChild(rowDivBottom);
		}

		for (var rowKey = 0; rowKey < rows; rowKey++) {
			var rowDiv = self.CreateHexRow();
			for (var colKey = 0; colKey < columns; colKey++) {
				var hexDiv;

				if(snapshot[rowKey][colKey] === -1) {
					hexDiv = self.CreateHexBorder();
					AddInitialFakeHexListenerSetup(hexDiv);
				}else{
					hexDiv = self.CreateHex();
					AddInitialHexListenerSetup(hexDiv);
				}

				// Set column key to -1 for hexes that goes on even lines
				// (first hex is below index 0)
				hexDiv.c = colKey;
				hexDiv.r = rowKey;
				hexDiv.menuParent = null;

				rowDiv.appendChild(hexDiv);
			}
			gridDiv.appendChild(rowDiv);
		}
		for (var rowKey = 0; rowKey < rows; rowKey++) {
			var borderHex = self.CreateHexBorder();

			// if this is an even numbered row
			if(((rowKey % 2 == 0) ? true : false)) {
				AddInitialFakeHexListenerSetup(borderHex);
				gridDiv.children[rowKey].insertBefore(borderHex, gridDiv.children[rowKey].firstChild);
			}else{
				AddInitialFakeHexListenerSetup(borderHex);
				gridDiv.children[rowKey].appendChild(borderHex);
			}
		}

		// reset the counter
		HexedHelper.animatingHexes = 0;
	};
	self.CreateHexRow = function() {
		var div = document.createElement('div');
		div.className = 'hex-row';
	  
		return div;
	};
	self.CreateHexRowBorderBottom = function() {
		var div = document.createElement('div');
		div.className = 'hex-row-border-bottom';
	  
		return div;
	};
	self.CreateHex = function() {
		var div = document.createElement('div');
		div.className = 'hex';
	  
		return div;
	};
	self.CreateHexBorder = function() {
		var div = document.createElement('div');
		div.className = 'hex-border';
	  
		return div;
	};
	var AddInitialHexListenerSetup = function(theHex){
		HexedHelper.ChangeClickListenerForHex(theHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
		HexedHelper.AddClickListenerToHex(theHex);
		HexedHelper.ChangeHoverListenerForHex(theHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverBehaviour);
		HexedHelper.AddHoverListenerToHex(theHex);
		HexedHelper.ChangeHoverOutListenerForHex(theHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverOutBehaviour);
		HexedHelper.AddHoverOutListenerToHex(theHex);

    	HexedHelper.AddListenerToHex(theHex, 'webkitAnimationEnd', function () {
			HexedHelper.RemoveClass(theHex, 'hex-spin');
	    });
	    HexedHelper.AddListenerToHex(theHex, 'animationend', function () {
			HexedHelper.RemoveClass(theHex, 'hex-spin');
	    });
	};
	var AddInitialFakeHexListenerSetup = function(fakeHex) {
		HexedHelper.ChangeClickListenerForHex(fakeHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexClickBehaviour);
		HexedHelper.AddClickListenerToHex(fakeHex);
		HexedHelper.ChangeHoverListenerForHex(fakeHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexHoverBehaviour);
		HexedHelper.AddHoverListenerToHex(fakeHex);
		HexedHelper.ChangeHoverOutListenerForHex(fakeHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexHoverOutBehaviour);
		HexedHelper.AddHoverOutListenerToHex(fakeHex);

		HexedHelper.AddListenerToHex(fakeHex, 'webkitAnimationEnd', function () {
			HexedHelper.RemoveClass(fakeHex, 'hex-spin');
	    });
	    HexedHelper.AddListenerToHex(fakeHex, 'animationend', function () {
			HexedHelper.RemoveClass(fakeHex, 'hex-spin');
	    });
	};
};

module.exports = HTMLGenerator;