var _hexedMenuHelper = require("./HexedMenuHelper.js");
var _HTMLGenerator = require("./HTMLGenerator.js");
var _SocketHelper = require("./SocketHelper.js");

var HexedHelper = function(){
  var self = this;

  self.knownColumns = 0;
  self.knownRows = 0;
  self.rowOffset = 0;

  self.HexedMenuHelper = new _hexedMenuHelper(self);
  self.HTMLGenerator = new _HTMLGenerator(self);
  self.MySocket = new _SocketHelper(self);
  self.hexedInnerContainer = document.getElementById('hexedinnercontainer');
  self.hexedGrid = document.getElementById('hexedgrid');
  self.hexedGridTop = document.getElementById('hexedgridtop');
  self.hexedGridBottom = document.getElementById('hexedgridbottom');
  self.hexedGridSnapshot = [];
  self.animatingHexes = 0;
  self.waitingForServerTurnExecution = false;

  self.lastClickData = {
    top: -1,
    left: -1,
    dragged: false
  };

  self.ReturnSurroundingHexes = function() {
    return [
      self.ReturnSelf,
      self.ReturnTopLeft,
      self.ReturnTop,
      self.ReturnTopRight,
      self.ReturnBottomRight,
      self.ReturnBottom,
      self.ReturnBottomLeft
    ];
  };
  self.ReturnSelf = function(el){
    return el;
  };
  self.ReturnTopLeft = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // first row
    if(row === 0) {

      return self.hexedGridTop.children[1].children[col];

    // everything else
    }else{
      return self.hexedGrid.children[row-1].children[col];
    }
    
  };
  self.ReturnTop = function(el){
    var row = el.r;
    var col = el.c;
    col = (self.IAmEven(row) == true) ? col+1 : col;

    // Account for borders
    // first row
    if(row < 2) {
      // Return the border hex on top
      return self.hexedGridTop.children[row].children[col];

    // First column on odd numbered rows
    }else{
      return self.hexedGrid.children[row-2].children[col];
    }

  };
  self.ReturnTopRight = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // first row
    if(row === 0) {
      return self.hexedGridTop.children[1].children[col+1];

    // everything else
    }else{
      return self.hexedGrid.children[row-1].children[col+1];
    }

  };
  self.ReturnBottomRight = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // last row
    if(row === self.knownRows-1) {
      return self.hexedGridBottom.children[0].children[col+1];

    // everything else
    }else{
      return self.hexedGrid.children[row+1].children[col+1];
    }

  };
  self.ReturnBottom = function(el){
    var row = el.r;
    var col = el.c;
    col = (self.IAmEven(row) == true) ? col+1 : col;

    // Account for borders
    // two last rows?
    if(row >= self.knownRows-2) {
      // Return the border hex on top
      var r_r = (row == self.knownRows -1) ? 1 : 0;
      return self.hexedGridBottom.children[r_r].children[col];

    // First column on odd numbered rows
    }else{
      return self.hexedGrid.children[row+2].children[col];
    }
  };
  self.ReturnBottomLeft = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // first row
    if(row === self.knownRows-1) {

      return self.hexedGridBottom.children[0].children[col];

    // everything else
    }else{
      return self.hexedGrid.children[row+1].children[col];
    }
  };
  
  self.ChangeClickListenerForHex = function(el, listenerFunc) {
    el.myClickEffect = listenerFunc.bind(el);
  };
  self.AddClickListenerToHex = function (el){
    // Save a record of where the mouse button was clicked down
    el.addEventListener('mousedown', function(e){
      self.lastClickData.top = e.pageX;
      self.lastClickData.left = e.pageY;
    }, false);
    // Check if the mouse has been moving more than 15px (the user likely is dragging map)
    el.addEventListener('mouseup', function(e){
      var top   = e.pageX,
          left  = e.pageY,
          ptop  = self.lastClickData.top,
          pleft = self.lastClickData.left;

        if(self.lastClickData.top !== -1 && self.lastClickData.left !== -1) {
          self.lastClickData.dragged = Math.abs(top - ptop) > 15 || Math.abs(left - pleft) > 15;
        }
    }, false);
    // If the user was dragging the map, don't fire off the click listener
    el.addEventListener('click', function(e){
      if(!self.lastClickData.dragged) {
        el.myClickEffect();
      }else{
        e.preventDefault()
      }
      self.lastClickData = {top: -1, left: -1, dragged: false};
    }, false);
  };
  self.ChangeHoverListenerForHex = function(el, listenerFunc) {
    el.myHoverEffect = listenerFunc.bind(el);
  }
  self.AddHoverListenerToHex = function (el){
    el.addEventListener('mouseover', function(){
      el.myHoverEffect();
    }, false);
  };
  self.ChangeHoverOutListenerForHex = function(el, listenerFunc) {
    el.myHoverOutEffect = listenerFunc.bind(el);
  }
  self.AddHoverOutListenerToHex = function (el){
    el.addEventListener('mouseleave', function(){
      el.myHoverOutEffect();
    }, false);
  };

  // Add/Remove generic listener function to hex
  // (almost deprecated)
  // @TODO: Skal nok bare fjerne den helt :)
  self.AddListenerToHex = function (el, event, listenerFunc){
    el.addEventListener(event, listenerFunc.bind(el), false);
  };
  self.RemoveListenerFromHex = function (el, event){
    if(el.myClickListener != undefined) {
      el.removeEventListener(event, el.myClickListener);
    }
  };

  self.DelayedAddClass = function(el, timeout, className, callback) {
    el['transitionint'] = setTimeout(function () {
      self.AddClass(el, className);

      if(callback){
        callback();
      }
    }, timeout);
  };
  self.DelayedRemoveClass = function(el, timeout, className, callback) {
    el['transitionint'] = setTimeout(function() {
      self.RemoveClass(el, className);

      if(callback){
        callback();
      }
    }, timeout);
  };

  

  self.DisableAllHexesExceptClass = function(className) {
    
    var hexes = self.hexedGrid.getElementsByClassName('hex');
    var borderHexes = self.hexedInnerContainer.getElementsByClassName('hex-border');
    //var  = hexGrid.getElementsByClassName('hex');
    for(var i = 0; i < hexes.length; i++){
      if(!hexes[i].classList.contains(className)){
        self.AddClass(hexes[i], 'hex-disabled');
      }
    }
    for(var i = 0; i < borderHexes.length; i++){
      if(!borderHexes[i].classList.contains(className)){
        self.AddClass(borderHexes[i], 'hex-border-disabled');
      }
    }
  };
  self.EnableAllHexes = function() {
    var hexes = self.hexedGrid.getElementsByClassName('hex-disabled');
    var borderHexes = self.hexedInnerContainer.getElementsByClassName('hex-border');
    for(var i = 0; i < hexes.length; i++){
      (function(index) {
        self.DelayedRemoveClass(hexes[index], 100, 'hex-disabled');
      })(i);
    }
    for(var i = 0; i < borderHexes.length; i++){
      (function(index) {
        self.DelayedRemoveClass(borderHexes[index], 100, 'hex-border-disabled');
      })(i);
    }
  };
  
  
  
  self.RemoveClass = function(el, className) {
    if(el.classList.contains(className)){
      el.classList.remove(className);
      if (className === 'hex-spin') {
        self.animatingHexes--;
      }
    }
  };
  self.AddClass = function(el, className) {
    if(!el.classList.contains(className)){
      el.classList.add(className);
      if (className === 'hex-spin') {
        self.animatingHexes++;
      }
    }
  };


  /**
   * Find the column index for this hex
   * Return -1 if not found?
  */
  self.FindMyColIndex = function(el) {
    return Array.prototype.indexOf.call(el.parentElement.children, el);
  };
  self.FindMyRowIndex = function (el) {
    return Array.prototype.indexOf.call(el.parentElement.parentElement.children, el.parentElement);
  };
  self.IAmEven = function(number) {
    return (number % 2 == 0) ? true : false; 
  };


  self.GenerateHexGrid = function(rows, columns){
    var grid = [];
    for (var i = 0; i < rows; i++) {
      var aRow = [];
      for (var j = 0; j < columns; j++) {
        // Lav hexes med samme default settings
        var hex = {

        };
        aRow.push(hex);
      }
      grid.push(aRow);
    }
    return grid;
  };
  self.UpdateHexGridFromNewSnapshot = function(snapshot){
    if(snapshot.length != self.hexedGridSnapshot.length || snapshot[0].length != self.hexedGridSnapshot[0].length) {
      self.HTMLGenerator.GenerateHexMap(snapshot);
    }
    for (var row = 0; row < snapshot.length; row++) {
      for (var column = 0; column < snapshot[row].length; column++) {
        // Calculate actual html rows and columns with account for borders
        var htmlRowColum = self.ConvertToActualHTMLGridValues(row, column);
        var singleHex = snapshot[row][column];
        var htmlHex = self.hexedGrid.children[htmlRowColum.row].children[htmlRowColum.column];

        // If we have info on the hex
        if (singleHex !== -1) {
          // If the hex is a border hex
          if(htmlHex.classList.contains("hex-border")) {
            // Convert it to an actual hex
            self.RemoveClass(htmlHex, "hex-border");
            self.AddClass(htmlHex, "hex");
            self.ChangeClickListenerForHex(htmlHex, self.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
            self.ChangeHoverListenerForHex(htmlHex, self.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverBehaviour);
            self.ChangeHoverOutListenerForHex(htmlHex, self.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverOutBehaviour);

          }
          // Check and set owner color
          // If this hex is my own
          if(singleHex.owner_id == self.MySocket.player.player_id) {
            // Add turquoise color and remove red if not already done
            self.RemoveClass(htmlHex, 'hex-red');
            self.AddClass(htmlHex, 'hex-turq');
            self.RemoveClass(htmlHex, 'hex-pink');

          // If this hex is an opponents
          }else if(singleHex.owner_id >= 0 && singleHex.owner_id != self.MySocket.player.player_id) {
            // Add red color and remove turquoise if not already done
            self.AddClass(htmlHex, 'hex-red');
            self.RemoveClass(htmlHex, 'hex-turq');
            self.RemoveClass(htmlHex, 'hex-pink');

          // If this hex is a neutral owned
          }else{
            // Remove colors if exists
            self.RemoveClass(htmlHex, 'hex-red');
            self.RemoveClass(htmlHex, 'hex-turq');
            self.AddClass(htmlHex, 'hex-pink');
          }

          var iconStr = self.ReturnProperHexIcon(singleHex);
          if(iconStr !== "") {
            self.HTMLGenerator.AddIconToHex(htmlHex, iconStr);
          }

        // If we have no info on the hex
        }else{
          // Remove colors if exists
          self.RemoveClass(htmlHex, 'hex-red');
          self.RemoveClass(htmlHex, 'hex-turq');
          self.RemoveClass(htmlHex, 'hex-pink');
        }

      }
    }

    self.hexedGridSnapshot = snapshot;
  };
  self.ReturnProperHexIcon = function(hex) {
    var str = "";
    if(hex.building_type) {
      switch(hex.building_type.name) {
        case "WoodCutter" :
          str = "log";
          break;

        case "StoneCutter" :
          str = "stone-block";
          break;

        case "AppleYard" :
          str = "shiny-apple";
          break;

        case "IronSmelter" :
          str = "metal-bar";
          break;

        case "LookoutTower" :
          str = "watchtower";
          break;

        default :
          break;
      }
    // If there is no building on the hex
    }else{
      // Add a resource icon to the hex if it has resources
      switch(hex.main_resource) {
        case "Wood" :
          str = "forest";
          break;

        case "Stone" :
          str = "stone-pile";
          break;

        case "Iron" :
          str = "mine-wagon";
          break;

        default :
          break;
      }
    }

    return str;
  }

  // hexes : this.HexedHelper.hexedGrid.getElementsByClassName('hex')


  self.ConvertToActualHTMLGridValues = function(row, column) {
      var offsetCalc = row - self.rowOffset;
      // console.log("row vs offset: "+row+","+offsetCalc);
      // console.log(self.rowOffset);
      if (row % 2 === 0) {
          return {row: row, column: column+1};
      }else{
          return {row: row, column: column};
      }
  };
};





module.exports = HexedHelper;