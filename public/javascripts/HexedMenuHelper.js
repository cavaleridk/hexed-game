
var HexedMenuHelper = function(HexedHelper) {
  var self = this;

  /**
   * @TODO: Skal lige lave comments herinde om!
   */
  self.CreateMenuArray = [
    Create_clicked_menu_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, function(){
        console.log('Clicked the main hex...');
      })
      HexedHelper.HTMLGenerator.AddStatsDivToHex(el, [
        {icon: "guards", populate: function(theHex){
          return (theHex.info_precision < 1) ? "("+theHex.troop_count+")" : theHex.troop_count;
        }}
      ]);
    },
    /* 
     * Functions for ones own hexes menu
    */
    Create_build_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Click_build_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "hammer-nails");
    },

    /* 
     * Functions for neutral hexes menu
    */
    Create_attack_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.ForeignHexes.Click_attack_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "crossed-swords");
    },
    Create_oracle_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.ForeignHexes.Click_oracle_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "spy");
    },

    // 4
    Create_troops_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "guards");
    },
    // 5
    Create_sub_menu_return_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.General.Click_sub_menu_return_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "anticlockwise-rotation");
    },
    // 6
    Create_troops_menu_deposit_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_deposit_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "exit-door");
    },
    // 7
    Create_troops_menu_withdraw_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_withdraw_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "entry-door");
    },
    // 8
    Create_troops_menu_recruit_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_recruit_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "biceps");
    },
    // 9
    Create_troops_clicked_sub_menu_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, function(){
        console.log('Clicked the main hex...');
      })
      HexedHelper.HTMLGenerator.AddStatsDivToHex(el, [
        {icon: "guards", populate: function(theHex){
          return (theHex.info_precision < 1) ? "("+theHex.troop_count+")" : theHex.troop_count;
        }},
        {icon: "biceps", populate: function(theHex) {
          return 1;
        }}
      ]);
    },
    // 10
    Create_build_clicked_sub_menu_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, function(){
        console.log('Clicked the main hex...');
      })
      HexedHelper.HTMLGenerator.AddStatsDivToHex(el, [
        {icon: "guards", populate: function(theHex){
          return (theHex.info_precision < 1) ? "("+theHex.troop_count+")" : theHex.troop_count;
        }}
      ]);
    },
    // 11
    Create_build_menu_appleyard_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_appleyard_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "shiny-apple");
    },
    // 12
    Create_build_menu_woodcutter_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_woodcutter_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "axe-in-stump");
    },
    // 13
    Create_build_menu_stonecutter_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_stonecutter_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "stone-block");
    },
    // 14
    Create_build_menu_ironsmelter_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_ironsmelter_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "metal-bar");
    },
    // 15
    Create_build_menu_watchtower_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_watchtower_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "watchtower");
    }
  ];
  
  /**
   * Event handlers for listeners in navigation 
   * 
   * NOTE: 'this' refers to the hex that was clicked.
  */
  self.MenuEventHandlers = {
    DefaultHexHoverBehaviour : function() {
      if(!this.classList.contains('hex-menu')) {
        HexedHelper.AddClass(this, 'hex-green');
      }
    },
    DefaultHexHoverOutBehaviour : function() {
      if (!this.classList.contains('hex-menu')) {
        HexedHelper.RemoveClass(this, 'hex-green');
      }
    },
    DefaultHexClickBehaviour : function() {
      if(IsMenuEnabled()){
        if(this.classList.contains('hex-disabled')){
          // Execute the hover out function first
          this.myHoverOutEffect();

          CloseHexedMenu();

        }else{
          // Execute the hover out function first
          this.myHoverOutEffect();

          // Open hex with the appropriate menu
          OpenHexedMenu(this, PrepareMenuStructure(this), 50);
          // OpenHexedMenu(this, [0, 1, 2, 3, 4, 5, 2], 50);
        }

        // Got to figure out what to do instead..
      }else{
        console.log('Dev: Please wait for animation to finish or server to respond..')
      }
    },
    BorderHexHoverBehaviour : function() {
      if(this.classList.contains('hex-border-disabled')) {
        HexedHelper.AddClass(this, 'hex-border-hover');
      }
    },
    BorderHexHoverOutBehaviour : function() {
      if (this.classList.contains('hex-border-disabled')) {
        HexedHelper.RemoveClass(this, 'hex-border-hover');
      }
    },
    BorderHexClickBehaviour : function () {
      if(this.classList.contains('hex-border-disabled')){
        // Execute the hover out function first
        this.myHoverOutEffect();

        CloseHexedMenu();
      }
    },
    /**
     * General stuff
     */
    General : {
      Click_sub_menu_return_item : function(){
        if(IsMenuEnabled()){
          // IF we are actually in a sub mnue
          if(this.menuParent.menuOldStructure) {
            // Close the menu
            // 
            PrepareHexedMenuForSubMenu();
            
            // Open the new menu
            // 
            OpenHexedMenu(this.menuParent, this.menuParent.menuOldStructure, 50);

            // Remove reference to old menu..
            this.menuParent.menuOldStructure = null;

          // Should proably not log anything here
          }else{
            console.log('Dev: Not in a sub menu..')
          }

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    },
    /* 
     * Functions for ones own hexes menu
    */
    OwnHexes : {
      Click_build_menu_item : function(){
        if(IsMenuEnabled()){
          // Close the menu
          // 
          PrepareHexedMenuForSubMenu();

          // Save a reference to the old menu?
          //  Skal nok gøres ved open menu tænker jeg..
          // 
          this.menuParent.menuOldStructure = MenuArrays.OwnHexDefault;

          // Select the right building menu
          var properMenu = [];
          console.log(HexedHelper.hexedGridSnapshot[this.menuParent.r][this.menuParent.c].main_resource);
          switch(HexedHelper.hexedGridSnapshot[this.menuParent.r][this.menuParent.c].main_resource) {
            case "Wood" :
              properMenu = MenuArrays.OwnHexBuildingsWithWood;
              break;
            
            case "Stone" :
              properMenu = MenuArrays.OwnHexBuildingsWithStone;
              break;
              
            case "Iron" :
              properMenu = MenuArrays.OwnHexBuildingsWithIron;
              break;
              
            default :
              properMenu = MenuArrays.OwnHexBuildingsDefault;
              break;
          }
          // Open the new menu
          // 
          OpenHexedMenu(this.menuParent, properMenu, 50);

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_item : function(){
        if(IsMenuEnabled()){
          // Close the menu
          // 
          PrepareHexedMenuForSubMenu();

          // Save a reference to the old menu?
          //  Skal nok gøres ved open menu tænker jeg..
          // 
          this.menuParent.menuOldStructure = MenuArrays.OwnHexDefault;
          // Open the new menu
          // 
          OpenHexedMenu(this.menuParent, MenuArrays.OwnHexTroopsDefault, 50);
          // alert('I am the troops menu!');

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_deposit_item : function(){
        if(IsMenuEnabled()){
          self.ShowSweetPromptWindow("Prepare Armed Units", 
            "How many troops do you want to add to defend this hex from your armed units?", 
            "How many defenders do we really need!?", 
            function(inputVal){
              // If this is not a anumber
              if(! /^\d+$/.test(inputVal)) {
                swal.showInputError("Please enter a number.");     
                return false  
              }
              var menuParent = this.menuParent;
              var inputNum = parseInt(inputVal);
              if(inputNum > HexedHelper.MySocket.player.troop_count) {
                swal.showInputError("Cannot send more than we have!");     
                return false  
              }

              // Close the prompt window
              swal.close();

              // Send the attack request to server
              HexedHelper.MySocket.SendAddTroopsFromHexRequest(menuParent, inputNum);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);

              // swal("Nice!", "You wrote: " + inputVal, "success");
              
            }.bind(this)
          );

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_withdraw_item: function(){
        if(IsMenuEnabled()){
          self.ShowSweetPromptWindow("Prepare Armed Units", 
            "How many troops do you want to remove from this hex and add to your armed units?", 
            "Leave at least 1 troop...", 
            function(inputVal){
              // If this is not a anumber
              if(! /^\d+$/.test(inputVal)) {
                swal.showInputError("Please enter a number.");     
                return false  
              }
              var menuParent = this.menuParent;
              var inputNum = parseInt(inputVal);
              if(inputNum >= HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count) {
                swal.showInputError("Please leave at least 1 remaining troop.");     
                return false  
              }

              // Close the prompt window
              swal.close();

              // Send the attack request to server
              HexedHelper.MySocket.SendRemoveTroopsFromHexRequest(menuParent, inputNum);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);

              // swal("Nice!", "You wrote: " + inputVal, "success");
              
            }.bind(this)
          );

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_recruit_item: function(){
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Recruit Troops",   
              text: '<p>This is your boot camp, where you can spend the day to recruit new troops. The amount of troops you gain can later be upgraded.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" /> +1</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Do it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendRecruitTroopsForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_appleyard_item: function(){
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Apple Yard",   
              text: '<p>Would you like to build a new Apply Yard here? Each Apple Yard will produce a bit of food over time.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/shiny-apple.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" /> 2</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("The Apple Yard requires at least 2 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildAppleYardForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_woodcutter_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Woodcutter",   
              text: '<p>Would you like to build a new Woodcutter here? Each woodcutter will process wood over time.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" /> 2</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("The Woodcutter requires at least 2 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildWoodCutterForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_stonecutter_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Stonecutter",   
              text: '<p>Would you like to build a new Stonecutter here? Each Stonecutter will gather stones as material over time.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/stone-block.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" />2</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> 10</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("The Stonecutter requires at least 2 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }else if(HexedHelper.MySocket.player.wood < 10) {
                swal.showInputError("This building requires 10 wood.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildStoneCutterForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_ironsmelter_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Iron Smelter",   
              text: '<p>You can build an Iron Smelter here. Each Iron Smelter will turn iron ore into usable iron bars.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/metal-bar.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" />4</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> 30</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/stone-block.svg" /> 20</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 4) {
                swal.showInputError("The Iron Smelter requires at least 4 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count <= 4) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }else if(HexedHelper.MySocket.player.wood < 30) {
                swal.showInputError("This building requires 30 wood.");     
                return false;
              }else if(HexedHelper.MySocket.player.stone < 20) {
                swal.showInputError("This building requires 20 stone.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildIronSmelterForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_watchtower_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build a Watch Tower",   
              text: '<p>This is your regular Lookout Tower. It provides you with an increase range of vision from this hex.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" />3</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> 20</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/stone-block.svg" /> 10</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 3) {
                swal.showInputError("The Watch Tower requires at least 3 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count <= 3) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }else if(HexedHelper.MySocket.player.wood < 20) {
                swal.showInputError("This building requires 20 wood.");     
                return false;
              }else if(HexedHelper.MySocket.player.stone < 10) {
                swal.showInputError("This building requires 10 stone.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildLookoutTowerForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    },

    /* 
     * Functions for foreign hexes menu
    */
    ForeignHexes : {
      Click_attack_menu_item : function(){
        if(IsMenuEnabled()){

          // linje 191..
          self.ShowSweetPromptWindow("Attack", 
            "How many troops will you send into battle?", 
            "Give us your command!", 
            function(inputVal){
              // If this is not a anumber
              if(! /^\d+$/.test(inputVal)) {
                swal.showInputError("Please enter a number.");     
                return false  
              }
              var inputNum = parseInt(inputVal);
              if(inputNum <= 0) {
                swal.showInputError("Cannot send less than 1 troop.");     
                return false  
              }
              if(inputNum > HexedHelper.MySocket.player.troop_count) {
                swal.showInputError("Cannot send more troops than we have!");     
                return false  
              }

              // Close the prompt window
              swal.close();
              var menuParent = this.menuParent;

              // Send the attack request to server
              HexedHelper.MySocket.SendAttackHexRequest(menuParent, inputNum);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);

              // swal("Nice!", "You wrote: " + inputVal, "success");
              
            }.bind(this)
          );

          // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Click_oracle_menu_item : function(){
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Oracles",   
              text: "Oracles will spend the day to gather intel about this hex",   
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Do it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendClientOracleHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });
          
        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    },
    /* 
     * Functions for hexes menu
    */
    NotUsedHexes : {
      Click_attack_menu_item : function(){
        if(IsMenuEnabled()){

          alert('I am the attack menu for opponents!');

          // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Click_oracle_menu_item : function(){
        if(IsMenuEnabled()){

          alert('I am the oracle menu for opponents!');

          // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    }
  };


  var text_controlled_hexes = document.getElementById("stats_hexes");
  var text_available_units = document.getElementById("stats_units");
  var text_food = document.getElementById("stats_food");
  var text_wood = document.getElementById("stats_wood");
  var text_stone = document.getElementById("stats_stone");
  var text_iron = document.getElementById("stats_iron");
  var text_food_consumption = document.getElementById("stats_food_consumption");
  var text_day = document.getElementById("stats_days");
  self.UpdateStatsUIData = function (statsObj) {
    // Do controlled hexes stuff here..
    // hvis jeg altså vil have det med? Måske andet data er bedre
    // 
    // statsObj {
    //  troop_count: xx,
    //  this_turn: turns.length,
    // }
    if(text_available_units.innerHTML != statsObj.troop_count) {
      text_available_units.innerHTML = statsObj.troop_count;
    }
    if(text_food.innerHTML != statsObj.player_food) {
      text_food.innerHTML = statsObj.player_food;
    }
    if(text_wood.innerHTML != statsObj.player_wood) {
      text_wood.innerHTML = statsObj.player_wood;
    }
    if(text_stone.innerHTML != statsObj.player_stone) {
      text_stone.innerHTML = statsObj.player_stone;
    }
    if(text_iron.innerHTML != statsObj.player_iron) {
      text_iron.innerHTML = statsObj.player_iron;
    }
    // if(text_food_consumption.innerHTML != statsObj.this_turn) {
    //   text_day.innerHTML = statsObj.this_turn;
    // }
    if(text_day.innerHTML != statsObj.this_turn) {
      text_day.innerHTML = statsObj.this_turn;
    }
  }

  var PrepareMenuStructure = function(el) {
    // Check my snapshot for info
    var snapHex = HexedHelper.hexedGridSnapshot[el.r][el.c];

    // If we have any information about this hex
    if(snapHex) {
      // If this is the players own hex
      if (snapHex.owner_id == HexedHelper.MySocket.player.player_id) {
        // Set the menu with build(1)
        return MenuArrays.OwnHexDefault;

      // If this hex is known to be owned by neutral 
      }else if(snapHex.owner_id == -100){
        // Set the menu with attack(2) and oracle(3)
        return MenuArrays.ForeignHexDefault;

      // Anything else (IE. some other player owns it)
      }else{
        // Set the menu with attack(2) and oracle(3)
        return MenuArrays.ForeignHexDefault;
      }

    // If we have no information about this hex
    }else{
      // Set the menu with attack(2) and oracle(3)
      return MenuArrays.ForeignHexDefault;
    }

    // Fallback to just targetting the clicked hex
    return [0];
  };

  var MenuArrays = {
    OwnHexDefault: [0, 1, 4],
    OwnHexTroopsDefault: [9, 6, 7, 8, 5],
    OwnHexBuildingsDefault: [10, 11, 15, 5],
    OwnHexBuildingsWithWood: [10, 11, 12, 15, 5],
    OwnHexBuildingsWithStone: [10, 11, 13, 15, 5],
    OwnHexBuildingsWithIron: [10, 11, 14, 15, 5],
    ForeignHexDefault: [0, 2, 3]
  };

  var OpenHexedMenu = function (el, menuArray, delay){
    var arr = HexedHelper.ReturnSurroundingHexes();
    var totalDelay = 0;
    
    for (var i = 0; i < menuArray.length; i++) {

      // Get a hold of the hex
      var theHex = arr[i].apply(HexedHelper, [el]);

      // Add color and spin effect
      ChangeHexToMenu(theHex, totalDelay, menuArray[i], el);
      
      totalDelay += delay;
      
    }
    
    // Disable alle andre hexes her
    HexedHelper.DisableAllHexesExceptClass('hex-menu');
  };

  var ChangeHexToMenu = function(hex, delay, menuType, parentHex) {
    HexedHelper.HTMLGenerator.RemoveHTMLRecursively(hex);
    HexedHelper.AddClass(hex, 'hex-menu');

    HexedHelper.DelayedAddClass(hex, delay, 'hex-spin', function() {
      // If this is the main hex that was clicked
      if(hex.r == parentHex.r && hex.c == parentHex.c && !hex.classList.contains('hex-border')) {
        // Set this hex up with menu stuff
        HexedHelper.HexedMenuHelper.CreateMenuArray[menuType](hex);
        hex.menuParent = parentHex;

      // If this is one of the actual menu tabs
      }else{
        // Change the hex's colour to blue
        HexedHelper.DelayedAddClass(hex, 250, 'hex-blue', function(){
          // Set this hex up with menu stuff
          HexedHelper.HexedMenuHelper.CreateMenuArray[menuType](hex);
          hex.menuParent = parentHex;
        });
      }
    });
  };

  self.CloseHexedMenuCaller = function() {
    CloseHexedMenu();
  };

  var CloseHexedMenu = function (){
    var menuHexes = Array.prototype.slice.call(HexedHelper.hexedGrid.getElementsByClassName('hex-menu'));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridTop.getElementsByClassName('hex-menu')));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridBottom.getElementsByClassName('hex-menu')));

    for(var i = 0; i < menuHexes.length; i++){
      // console.dir(menuHexes.length);
      HexedHelper.DelayedRemoveClass(menuHexes[i], 250, 'hex-menu');

      HexedHelper.AddClass(menuHexes[i], 'hex-spin');
      HexedHelper.DelayedRemoveClass(menuHexes[i], 250, 'hex-blue');

      // (function(index){
      //   menuHexes[index].menuParent = null;
      // })(i);

      HexedHelper.HTMLGenerator.RemoveHTMLRecursively(menuHexes[i]);

      menuHexes[i].menuParent = null;
      if(! menuHexes[i].classList.contains('hex-border')) {
        AddResourceIconToHex(menuHexes[i]);
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
      }else{
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexClickBehaviour);
      }
    }
    HexedHelper.EnableAllHexes();
  };

  var AddResourceIconToHex = function(theHex) {
    var snapHex = HexedHelper.hexedGridSnapshot[theHex.r][theHex.c];
    var iconStr = HexedHelper.ReturnProperHexIcon(snapHex);
    if(iconStr !== "") {
      HexedHelper.HTMLGenerator.AddIconToHex(theHex, iconStr);
    }
  };

  var PrepareHexedMenuForSubMenu = function() {
    console.log('preppp');
    var menuHexes = Array.prototype.slice.call(HexedHelper.hexedGrid.getElementsByClassName('hex-menu'));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridTop.getElementsByClassName('hex-menu')));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridBottom.getElementsByClassName('hex-menu')));

    for(var i = 0; i < menuHexes.length; i++){
      // console.dir(menuHexes.length);
      HexedHelper.RemoveClass(menuHexes[i], 'hex-menu');

      HexedHelper.AddClass(menuHexes[i], 'hex-spin');
      HexedHelper.DelayedRemoveClass(menuHexes[i], 250, 'hex-blue');

      (function(index){
        HexedHelper.HTMLGenerator.RemoveHTMLRecursively(menuHexes[index]);
        AddResourceIconToHex(menuHexes[index]);
      })(i);

      if(! menuHexes[i].classList.contains('hex-border')) {
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
      }else{
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], function(){});
      }
    }
  };

  var IsMenuEnabled = function() {
    if(HexedHelper.animatingHexes === 0 && HexedHelper.waitingForServerTurnExecution === false) {
      return true;
    }else{
      return false
    }
  };

  self.ShowSweetPromptWindow = function(title, text, placeholder, callback) {
    swal(
      {
        title: title,   
        text: text,   
        type: "input",  
        customClass: "hex-menu-swal", 
        showCancelButton: true,   
        closeOnConfirm: false,   
        animation: "slide-from-top",   
        inputPlaceholder: placeholder 
      }, 
      function(inputValue){
        if (inputValue === false) return false; 
        if (inputValue === "") {     
          swal.showInputError("Input can not be empty!");     
          return false   
        }    
        if(callback) {
          callback(inputValue);
        }
      }
    );
  };
};


module.exports = HexedMenuHelper;