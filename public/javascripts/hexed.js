(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

var HTMLGenerator = function(HexedHelper) {
	var self = this;

	self.AddIconToHex = function(el, iconName, spin = false) {
		var div = HexedHelper.HTMLGenerator.MenuCreateFullContainerDiv();

		var icon;
		if(spin) {
			icon = HexedHelper.HTMLGenerator.MenuCreateIcon(iconName);
			icon.classList.add('fa-spin');
		}else{
			icon = HexedHelper.HTMLGenerator.MenuCreateGameIcon(iconName);
		}
		div.appendChild(icon);
		el.appendChild(div);
	};
	/**
	 * StatsArray kan kun tage 2 ikoner lige nu..
	 * 	Skal rename ting og måske gøre mere versatil
	 * 	@TODO: Dét ^
	 *
	 * @class      AddStatsDivToHex (name)
	 * @param      {<type>}  el          { parameter_description }
	 * @param      {<type>}  statsArray  The statistics array
	 */
	self.AddStatsDivToHex = function(el, statsArray) {
		console.log(el.menuOldStructure);
		var div = HexedHelper.HTMLGenerator.MenuCreateContainerDiv();
		div.classList.add('hex-menu-item-container');

		for (var i = 0; i < statsArray.length; i++) {
			var _p = document.createElement('p');
			var _icon = document.createElement('img');
			var _text = document.createElement('span');
			_p.classList.add('hex-menu-item-paragraph');
			_icon.classList.add('responsive-img');
			_icon.classList.add('hex-menu-icon-small');
			_icon.src = "/hexed_images/"+statsArray[i].icon+".svg";
			_text.classList.add('hex-menu-item-text');

			var theHex = HexedHelper.hexedGridSnapshot[el.r][el.c];

			// Set troop text if not empty hex (aka if we know anything about it)
			if(! (Object.keys(theHex).length <= 3 && theHex.constructor === Object)) {
				// Show a parenthesis around troop count if there is any uncertainty with amounts
				_text.innerHTML = statsArray[i].populate(theHex);
				console.log("Precision / Opacity = " + theHex.info_precision);
				// Set opacity of the stats div based on precision, and default to full
				// opacity when no info is available
				_text.style.opacity = theHex.info_precision.toString();
			}else{
				_text.innerHTML = '???';
			}

			_p.appendChild(_icon);
			_p.appendChild(_text);
			div.appendChild(_p);
		}

		el.appendChild(div);
	};


	self.RemoveHTMLRecursively = function (el, removeSelf = false){
		while (el.hasChildNodes()) {
			el.removeChild(el.lastChild);
		}
		if(removeSelf){
			el.parentNode.removeChild(el);
		}
	};
	self.MenuCreateContainerDiv = function() {
		var div = document.createElement('div');
		div.className = 'hex-menu-item-container';

		return div;
	};
	self.MenuCreateFullContainerDiv = function() {
		var div = document.createElement('div');
		div.className = 'hex-menu-item-container-full';

		return div;
	};
	self.MenuCreateIcon = function(iconName) {
		var icon = document.createElement('i');
		icon.classList.add('fa');
		icon.classList.add(iconName);
		icon.classList.add('fa-3x');

		return icon;
	};
	self.MenuCreateGameIcon = function(iconName) {
		// var icon = document.createElement('i');
		// icon.classList.add('fa');
		// icon.classList.add(iconName);
		// icon.classList.add('fa-3x');
		var icon = document.createElement('img');
		icon.classList.add('responsive-img');
		icon.src = "/hexed_images/"+iconName+".svg";

		return icon;
	};




    
    
    
    
	// Also adds the border hexes that are not interactable by default
	self.GenerateHexMap = function (snapshot) {
		var rows = snapshot.length;
		var columns = snapshot[0].length;
		var gridDivTop = HexedHelper.hexedGridTop;
		var gridDivBottom = HexedHelper.hexedGridBottom;
		var gridDiv = HexedHelper.hexedGrid;
		var innerContainer = document.getElementById("hexedinnercontainer");

		self.RemoveHTMLRecursively(gridDivTop);
		self.RemoveHTMLRecursively(gridDivBottom);
		self.RemoveHTMLRecursively(gridDiv);

		// Increase size of container based on hex size
		// current size 60 + 124 width
		console.log("Container width: " + ((60+124) * columns));
		innerContainer.style.width = ((60+124) * (columns+1)) + "px";

		// Add the top and bottom border hexes
		// (2 rows in each end)
		for (var i = 0; i < 2; i++) {
			// Add to top
			var rowDivTop = self.CreateHexRow();
			for (var j = 0; j < columns +1; j++) {
				var hexDiv = self.CreateHexBorder();

				hexDiv.c = j;
				hexDiv.r = i;
				hexDiv.menuParent = null;
				AddInitialFakeHexListenerSetup(hexDiv);

				rowDivTop.appendChild(hexDiv);
			}
			gridDivTop.appendChild(rowDivTop);

			// add to bottom
			var rowDivBottom = (rows % 2 !== 0) ? self.CreateHexRowBorderBottom() : self.CreateHexRow();
			for (var k = 0; k < columns +1; k++) {
				var hexDiv = self.CreateHexBorder();

				hexDiv.c = k;
				hexDiv.r = i;
				hexDiv.menuParent = null;
				AddInitialFakeHexListenerSetup(hexDiv);

				rowDivBottom.appendChild(hexDiv);
			}
			gridDivBottom.appendChild(rowDivBottom);
		}

		for (var rowKey = 0; rowKey < rows; rowKey++) {
			var rowDiv = self.CreateHexRow();
			for (var colKey = 0; colKey < columns; colKey++) {
				var hexDiv;

				if(snapshot[rowKey][colKey] === -1) {
					hexDiv = self.CreateHexBorder();
					AddInitialFakeHexListenerSetup(hexDiv);
				}else{
					hexDiv = self.CreateHex();
					AddInitialHexListenerSetup(hexDiv);
				}

				// Set column key to -1 for hexes that goes on even lines
				// (first hex is below index 0)
				hexDiv.c = colKey;
				hexDiv.r = rowKey;
				hexDiv.menuParent = null;

				rowDiv.appendChild(hexDiv);
			}
			gridDiv.appendChild(rowDiv);
		}
		for (var rowKey = 0; rowKey < rows; rowKey++) {
			var borderHex = self.CreateHexBorder();

			// if this is an even numbered row
			if(((rowKey % 2 == 0) ? true : false)) {
				AddInitialFakeHexListenerSetup(borderHex);
				gridDiv.children[rowKey].insertBefore(borderHex, gridDiv.children[rowKey].firstChild);
			}else{
				AddInitialFakeHexListenerSetup(borderHex);
				gridDiv.children[rowKey].appendChild(borderHex);
			}
		}

		// reset the counter
		HexedHelper.animatingHexes = 0;
	};
	self.CreateHexRow = function() {
		var div = document.createElement('div');
		div.className = 'hex-row';
	  
		return div;
	};
	self.CreateHexRowBorderBottom = function() {
		var div = document.createElement('div');
		div.className = 'hex-row-border-bottom';
	  
		return div;
	};
	self.CreateHex = function() {
		var div = document.createElement('div');
		div.className = 'hex';
	  
		return div;
	};
	self.CreateHexBorder = function() {
		var div = document.createElement('div');
		div.className = 'hex-border';
	  
		return div;
	};
	var AddInitialHexListenerSetup = function(theHex){
		HexedHelper.ChangeClickListenerForHex(theHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
		HexedHelper.AddClickListenerToHex(theHex);
		HexedHelper.ChangeHoverListenerForHex(theHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverBehaviour);
		HexedHelper.AddHoverListenerToHex(theHex);
		HexedHelper.ChangeHoverOutListenerForHex(theHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverOutBehaviour);
		HexedHelper.AddHoverOutListenerToHex(theHex);

    	HexedHelper.AddListenerToHex(theHex, 'webkitAnimationEnd', function () {
			HexedHelper.RemoveClass(theHex, 'hex-spin');
	    });
	    HexedHelper.AddListenerToHex(theHex, 'animationend', function () {
			HexedHelper.RemoveClass(theHex, 'hex-spin');
	    });
	};
	var AddInitialFakeHexListenerSetup = function(fakeHex) {
		HexedHelper.ChangeClickListenerForHex(fakeHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexClickBehaviour);
		HexedHelper.AddClickListenerToHex(fakeHex);
		HexedHelper.ChangeHoverListenerForHex(fakeHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexHoverBehaviour);
		HexedHelper.AddHoverListenerToHex(fakeHex);
		HexedHelper.ChangeHoverOutListenerForHex(fakeHex, HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexHoverOutBehaviour);
		HexedHelper.AddHoverOutListenerToHex(fakeHex);

		HexedHelper.AddListenerToHex(fakeHex, 'webkitAnimationEnd', function () {
			HexedHelper.RemoveClass(fakeHex, 'hex-spin');
	    });
	    HexedHelper.AddListenerToHex(fakeHex, 'animationend', function () {
			HexedHelper.RemoveClass(fakeHex, 'hex-spin');
	    });
	};
};

module.exports = HTMLGenerator;
},{}],2:[function(require,module,exports){
var _hexedMenuHelper = require("./HexedMenuHelper.js");
var _HTMLGenerator = require("./HTMLGenerator.js");
var _SocketHelper = require("./SocketHelper.js");

var HexedHelper = function(){
  var self = this;

  self.knownColumns = 0;
  self.knownRows = 0;
  self.rowOffset = 0;

  self.HexedMenuHelper = new _hexedMenuHelper(self);
  self.HTMLGenerator = new _HTMLGenerator(self);
  self.MySocket = new _SocketHelper(self);
  self.hexedInnerContainer = document.getElementById('hexedinnercontainer');
  self.hexedGrid = document.getElementById('hexedgrid');
  self.hexedGridTop = document.getElementById('hexedgridtop');
  self.hexedGridBottom = document.getElementById('hexedgridbottom');
  self.hexedGridSnapshot = [];
  self.animatingHexes = 0;
  self.waitingForServerTurnExecution = false;

  self.lastClickData = {
    top: -1,
    left: -1,
    dragged: false
  };

  self.ReturnSurroundingHexes = function() {
    return [
      self.ReturnSelf,
      self.ReturnTopLeft,
      self.ReturnTop,
      self.ReturnTopRight,
      self.ReturnBottomRight,
      self.ReturnBottom,
      self.ReturnBottomLeft
    ];
  };
  self.ReturnSelf = function(el){
    return el;
  };
  self.ReturnTopLeft = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // first row
    if(row === 0) {

      return self.hexedGridTop.children[1].children[col];

    // everything else
    }else{
      return self.hexedGrid.children[row-1].children[col];
    }
    
  };
  self.ReturnTop = function(el){
    var row = el.r;
    var col = el.c;
    col = (self.IAmEven(row) == true) ? col+1 : col;

    // Account for borders
    // first row
    if(row < 2) {
      // Return the border hex on top
      return self.hexedGridTop.children[row].children[col];

    // First column on odd numbered rows
    }else{
      return self.hexedGrid.children[row-2].children[col];
    }

  };
  self.ReturnTopRight = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // first row
    if(row === 0) {
      return self.hexedGridTop.children[1].children[col+1];

    // everything else
    }else{
      return self.hexedGrid.children[row-1].children[col+1];
    }

  };
  self.ReturnBottomRight = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // last row
    if(row === self.knownRows-1) {
      return self.hexedGridBottom.children[0].children[col+1];

    // everything else
    }else{
      return self.hexedGrid.children[row+1].children[col+1];
    }

  };
  self.ReturnBottom = function(el){
    var row = el.r;
    var col = el.c;
    col = (self.IAmEven(row) == true) ? col+1 : col;

    // Account for borders
    // two last rows?
    if(row >= self.knownRows-2) {
      // Return the border hex on top
      var r_r = (row == self.knownRows -1) ? 1 : 0;
      return self.hexedGridBottom.children[r_r].children[col];

    // First column on odd numbered rows
    }else{
      return self.hexedGrid.children[row+2].children[col];
    }
  };
  self.ReturnBottomLeft = function(el){
    var row = el.r;
    var col = el.c;

    // Account for borders
    // first row
    if(row === self.knownRows-1) {

      return self.hexedGridBottom.children[0].children[col];

    // everything else
    }else{
      return self.hexedGrid.children[row+1].children[col];
    }
  };
  
  self.ChangeClickListenerForHex = function(el, listenerFunc) {
    el.myClickEffect = listenerFunc.bind(el);
  };
  self.AddClickListenerToHex = function (el){
    // Save a record of where the mouse button was clicked down
    el.addEventListener('mousedown', function(e){
      self.lastClickData.top = e.pageX;
      self.lastClickData.left = e.pageY;
    }, false);
    // Check if the mouse has been moving more than 15px (the user likely is dragging map)
    el.addEventListener('mouseup', function(e){
      var top   = e.pageX,
          left  = e.pageY,
          ptop  = self.lastClickData.top,
          pleft = self.lastClickData.left;

        if(self.lastClickData.top !== -1 && self.lastClickData.left !== -1) {
          self.lastClickData.dragged = Math.abs(top - ptop) > 15 || Math.abs(left - pleft) > 15;
        }
    }, false);
    // If the user was dragging the map, don't fire off the click listener
    el.addEventListener('click', function(e){
      if(!self.lastClickData.dragged) {
        el.myClickEffect();
      }else{
        e.preventDefault()
      }
      self.lastClickData = {top: -1, left: -1, dragged: false};
    }, false);
  };
  self.ChangeHoverListenerForHex = function(el, listenerFunc) {
    el.myHoverEffect = listenerFunc.bind(el);
  }
  self.AddHoverListenerToHex = function (el){
    el.addEventListener('mouseover', function(){
      el.myHoverEffect();
    }, false);
  };
  self.ChangeHoverOutListenerForHex = function(el, listenerFunc) {
    el.myHoverOutEffect = listenerFunc.bind(el);
  }
  self.AddHoverOutListenerToHex = function (el){
    el.addEventListener('mouseleave', function(){
      el.myHoverOutEffect();
    }, false);
  };

  // Add/Remove generic listener function to hex
  // (almost deprecated)
  // @TODO: Skal nok bare fjerne den helt :)
  self.AddListenerToHex = function (el, event, listenerFunc){
    el.addEventListener(event, listenerFunc.bind(el), false);
  };
  self.RemoveListenerFromHex = function (el, event){
    if(el.myClickListener != undefined) {
      el.removeEventListener(event, el.myClickListener);
    }
  };

  self.DelayedAddClass = function(el, timeout, className, callback) {
    el['transitionint'] = setTimeout(function () {
      self.AddClass(el, className);

      if(callback){
        callback();
      }
    }, timeout);
  };
  self.DelayedRemoveClass = function(el, timeout, className, callback) {
    el['transitionint'] = setTimeout(function() {
      self.RemoveClass(el, className);

      if(callback){
        callback();
      }
    }, timeout);
  };

  

  self.DisableAllHexesExceptClass = function(className) {
    
    var hexes = self.hexedGrid.getElementsByClassName('hex');
    var borderHexes = self.hexedInnerContainer.getElementsByClassName('hex-border');
    //var  = hexGrid.getElementsByClassName('hex');
    for(var i = 0; i < hexes.length; i++){
      if(!hexes[i].classList.contains(className)){
        self.AddClass(hexes[i], 'hex-disabled');
      }
    }
    for(var i = 0; i < borderHexes.length; i++){
      if(!borderHexes[i].classList.contains(className)){
        self.AddClass(borderHexes[i], 'hex-border-disabled');
      }
    }
  };
  self.EnableAllHexes = function() {
    var hexes = self.hexedGrid.getElementsByClassName('hex-disabled');
    var borderHexes = self.hexedInnerContainer.getElementsByClassName('hex-border');
    for(var i = 0; i < hexes.length; i++){
      (function(index) {
        self.DelayedRemoveClass(hexes[index], 100, 'hex-disabled');
      })(i);
    }
    for(var i = 0; i < borderHexes.length; i++){
      (function(index) {
        self.DelayedRemoveClass(borderHexes[index], 100, 'hex-border-disabled');
      })(i);
    }
  };
  
  
  
  self.RemoveClass = function(el, className) {
    if(el.classList.contains(className)){
      el.classList.remove(className);
      if (className === 'hex-spin') {
        self.animatingHexes--;
      }
    }
  };
  self.AddClass = function(el, className) {
    if(!el.classList.contains(className)){
      el.classList.add(className);
      if (className === 'hex-spin') {
        self.animatingHexes++;
      }
    }
  };


  /**
   * Find the column index for this hex
   * Return -1 if not found?
  */
  self.FindMyColIndex = function(el) {
    return Array.prototype.indexOf.call(el.parentElement.children, el);
  };
  self.FindMyRowIndex = function (el) {
    return Array.prototype.indexOf.call(el.parentElement.parentElement.children, el.parentElement);
  };
  self.IAmEven = function(number) {
    return (number % 2 == 0) ? true : false; 
  };


  self.GenerateHexGrid = function(rows, columns){
    var grid = [];
    for (var i = 0; i < rows; i++) {
      var aRow = [];
      for (var j = 0; j < columns; j++) {
        // Lav hexes med samme default settings
        var hex = {

        };
        aRow.push(hex);
      }
      grid.push(aRow);
    }
    return grid;
  };
  self.UpdateHexGridFromNewSnapshot = function(snapshot){
    if(snapshot.length != self.hexedGridSnapshot.length || snapshot[0].length != self.hexedGridSnapshot[0].length) {
      self.HTMLGenerator.GenerateHexMap(snapshot);
    }
    for (var row = 0; row < snapshot.length; row++) {
      for (var column = 0; column < snapshot[row].length; column++) {
        // Calculate actual html rows and columns with account for borders
        var htmlRowColum = self.ConvertToActualHTMLGridValues(row, column);
        var singleHex = snapshot[row][column];
        var htmlHex = self.hexedGrid.children[htmlRowColum.row].children[htmlRowColum.column];

        // If we have info on the hex
        if (singleHex !== -1) {
          // If the hex is a border hex
          if(htmlHex.classList.contains("hex-border")) {
            // Convert it to an actual hex
            self.RemoveClass(htmlHex, "hex-border");
            self.AddClass(htmlHex, "hex");
            self.ChangeClickListenerForHex(htmlHex, self.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
            self.ChangeHoverListenerForHex(htmlHex, self.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverBehaviour);
            self.ChangeHoverOutListenerForHex(htmlHex, self.HexedMenuHelper.MenuEventHandlers.DefaultHexHoverOutBehaviour);

          }
          // Check and set owner color
          // If this hex is my own
          if(singleHex.owner_id == self.MySocket.player.player_id) {
            // Add turquoise color and remove red if not already done
            self.RemoveClass(htmlHex, 'hex-red');
            self.AddClass(htmlHex, 'hex-turq');
            self.RemoveClass(htmlHex, 'hex-pink');

          // If this hex is an opponents
          }else if(singleHex.owner_id >= 0 && singleHex.owner_id != self.MySocket.player.player_id) {
            // Add red color and remove turquoise if not already done
            self.AddClass(htmlHex, 'hex-red');
            self.RemoveClass(htmlHex, 'hex-turq');
            self.RemoveClass(htmlHex, 'hex-pink');

          // If this hex is a neutral owned
          }else{
            // Remove colors if exists
            self.RemoveClass(htmlHex, 'hex-red');
            self.RemoveClass(htmlHex, 'hex-turq');
            self.AddClass(htmlHex, 'hex-pink');
          }

          var iconStr = self.ReturnProperHexIcon(singleHex);
          if(iconStr !== "") {
            self.HTMLGenerator.AddIconToHex(htmlHex, iconStr);
          }

        // If we have no info on the hex
        }else{
          // Remove colors if exists
          self.RemoveClass(htmlHex, 'hex-red');
          self.RemoveClass(htmlHex, 'hex-turq');
          self.RemoveClass(htmlHex, 'hex-pink');
        }

      }
    }

    self.hexedGridSnapshot = snapshot;
  };
  self.ReturnProperHexIcon = function(hex) {
    var str = "";
    if(hex.building_type) {
      switch(hex.building_type.name) {
        case "WoodCutter" :
          str = "log";
          break;

        case "StoneCutter" :
          str = "stone-block";
          break;

        case "AppleYard" :
          str = "shiny-apple";
          break;

        case "IronSmelter" :
          str = "metal-bar";
          break;

        case "LookoutTower" :
          str = "watchtower";
          break;

        default :
          break;
      }
    // If there is no building on the hex
    }else{
      // Add a resource icon to the hex if it has resources
      switch(hex.main_resource) {
        case "Wood" :
          str = "forest";
          break;

        case "Stone" :
          str = "stone-pile";
          break;

        case "Iron" :
          str = "mine-wagon";
          break;

        default :
          break;
      }
    }

    return str;
  }

  // hexes : this.HexedHelper.hexedGrid.getElementsByClassName('hex')


  self.ConvertToActualHTMLGridValues = function(row, column) {
      var offsetCalc = row - self.rowOffset;
      // console.log("row vs offset: "+row+","+offsetCalc);
      // console.log(self.rowOffset);
      if (row % 2 === 0) {
          return {row: row, column: column+1};
      }else{
          return {row: row, column: column};
      }
  };
};





module.exports = HexedHelper;
},{"./HTMLGenerator.js":1,"./HexedMenuHelper.js":3,"./SocketHelper.js":4}],3:[function(require,module,exports){

var HexedMenuHelper = function(HexedHelper) {
  var self = this;

  /**
   * @TODO: Skal lige lave comments herinde om!
   */
  self.CreateMenuArray = [
    Create_clicked_menu_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, function(){
        console.log('Clicked the main hex...');
      })
      HexedHelper.HTMLGenerator.AddStatsDivToHex(el, [
        {icon: "guards", populate: function(theHex){
          return (theHex.info_precision < 1) ? "("+theHex.troop_count+")" : theHex.troop_count;
        }}
      ]);
    },
    /* 
     * Functions for ones own hexes menu
    */
    Create_build_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Click_build_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "hammer-nails");
    },

    /* 
     * Functions for neutral hexes menu
    */
    Create_attack_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.ForeignHexes.Click_attack_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "crossed-swords");
    },
    Create_oracle_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.ForeignHexes.Click_oracle_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "spy");
    },

    // 4
    Create_troops_menu_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "guards");
    },
    // 5
    Create_sub_menu_return_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.General.Click_sub_menu_return_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "anticlockwise-rotation");
    },
    // 6
    Create_troops_menu_deposit_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_deposit_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "exit-door");
    },
    // 7
    Create_troops_menu_withdraw_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_withdraw_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "entry-door");
    },
    // 8
    Create_troops_menu_recruit_item = function(el){
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_troops_menu_recruit_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "biceps");
    },
    // 9
    Create_troops_clicked_sub_menu_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, function(){
        console.log('Clicked the main hex...');
      })
      HexedHelper.HTMLGenerator.AddStatsDivToHex(el, [
        {icon: "guards", populate: function(theHex){
          return (theHex.info_precision < 1) ? "("+theHex.troop_count+")" : theHex.troop_count;
        }},
        {icon: "biceps", populate: function(theHex) {
          return 1;
        }}
      ]);
    },
    // 10
    Create_build_clicked_sub_menu_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, function(){
        console.log('Clicked the main hex...');
      })
      HexedHelper.HTMLGenerator.AddStatsDivToHex(el, [
        {icon: "guards", populate: function(theHex){
          return (theHex.info_precision < 1) ? "("+theHex.troop_count+")" : theHex.troop_count;
        }}
      ]);
    },
    // 11
    Create_build_menu_appleyard_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_appleyard_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "shiny-apple");
    },
    // 12
    Create_build_menu_woodcutter_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_woodcutter_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "axe-in-stump");
    },
    // 13
    Create_build_menu_stonecutter_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_stonecutter_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "stone-block");
    },
    // 14
    Create_build_menu_ironsmelter_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_ironsmelter_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "metal-bar");
    },
    // 15
    Create_build_menu_watchtower_item = function (el) {
      HexedHelper.ChangeClickListenerForHex(el, HexedHelper.HexedMenuHelper.MenuEventHandlers.OwnHexes.Create_build_menu_watchtower_item);
      HexedHelper.HTMLGenerator.AddIconToHex(el, "watchtower");
    }
  ];
  
  /**
   * Event handlers for listeners in navigation 
   * 
   * NOTE: 'this' refers to the hex that was clicked.
  */
  self.MenuEventHandlers = {
    DefaultHexHoverBehaviour : function() {
      if(!this.classList.contains('hex-menu')) {
        HexedHelper.AddClass(this, 'hex-green');
      }
    },
    DefaultHexHoverOutBehaviour : function() {
      if (!this.classList.contains('hex-menu')) {
        HexedHelper.RemoveClass(this, 'hex-green');
      }
    },
    DefaultHexClickBehaviour : function() {
      if(IsMenuEnabled()){
        if(this.classList.contains('hex-disabled')){
          // Execute the hover out function first
          this.myHoverOutEffect();

          CloseHexedMenu();

        }else{
          // Execute the hover out function first
          this.myHoverOutEffect();

          // Open hex with the appropriate menu
          OpenHexedMenu(this, PrepareMenuStructure(this), 50);
          // OpenHexedMenu(this, [0, 1, 2, 3, 4, 5, 2], 50);
        }

        // Got to figure out what to do instead..
      }else{
        console.log('Dev: Please wait for animation to finish or server to respond..')
      }
    },
    BorderHexHoverBehaviour : function() {
      if(this.classList.contains('hex-border-disabled')) {
        HexedHelper.AddClass(this, 'hex-border-hover');
      }
    },
    BorderHexHoverOutBehaviour : function() {
      if (this.classList.contains('hex-border-disabled')) {
        HexedHelper.RemoveClass(this, 'hex-border-hover');
      }
    },
    BorderHexClickBehaviour : function () {
      if(this.classList.contains('hex-border-disabled')){
        // Execute the hover out function first
        this.myHoverOutEffect();

        CloseHexedMenu();
      }
    },
    /**
     * General stuff
     */
    General : {
      Click_sub_menu_return_item : function(){
        if(IsMenuEnabled()){
          // IF we are actually in a sub mnue
          if(this.menuParent.menuOldStructure) {
            // Close the menu
            // 
            PrepareHexedMenuForSubMenu();
            
            // Open the new menu
            // 
            OpenHexedMenu(this.menuParent, this.menuParent.menuOldStructure, 50);

            // Remove reference to old menu..
            this.menuParent.menuOldStructure = null;

          // Should proably not log anything here
          }else{
            console.log('Dev: Not in a sub menu..')
          }

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    },
    /* 
     * Functions for ones own hexes menu
    */
    OwnHexes : {
      Click_build_menu_item : function(){
        if(IsMenuEnabled()){
          // Close the menu
          // 
          PrepareHexedMenuForSubMenu();

          // Save a reference to the old menu?
          //  Skal nok gøres ved open menu tænker jeg..
          // 
          this.menuParent.menuOldStructure = MenuArrays.OwnHexDefault;

          // Select the right building menu
          var properMenu = [];
          console.log(HexedHelper.hexedGridSnapshot[this.menuParent.r][this.menuParent.c].main_resource);
          switch(HexedHelper.hexedGridSnapshot[this.menuParent.r][this.menuParent.c].main_resource) {
            case "Wood" :
              properMenu = MenuArrays.OwnHexBuildingsWithWood;
              break;
            
            case "Stone" :
              properMenu = MenuArrays.OwnHexBuildingsWithStone;
              break;
              
            case "Iron" :
              properMenu = MenuArrays.OwnHexBuildingsWithIron;
              break;
              
            default :
              properMenu = MenuArrays.OwnHexBuildingsDefault;
              break;
          }
          // Open the new menu
          // 
          OpenHexedMenu(this.menuParent, properMenu, 50);

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_item : function(){
        if(IsMenuEnabled()){
          // Close the menu
          // 
          PrepareHexedMenuForSubMenu();

          // Save a reference to the old menu?
          //  Skal nok gøres ved open menu tænker jeg..
          // 
          this.menuParent.menuOldStructure = MenuArrays.OwnHexDefault;
          // Open the new menu
          // 
          OpenHexedMenu(this.menuParent, MenuArrays.OwnHexTroopsDefault, 50);
          // alert('I am the troops menu!');

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_deposit_item : function(){
        if(IsMenuEnabled()){
          self.ShowSweetPromptWindow("Prepare Armed Units", 
            "How many troops do you want to add to defend this hex from your armed units?", 
            "How many defenders do we really need!?", 
            function(inputVal){
              // If this is not a anumber
              if(! /^\d+$/.test(inputVal)) {
                swal.showInputError("Please enter a number.");     
                return false  
              }
              var menuParent = this.menuParent;
              var inputNum = parseInt(inputVal);
              if(inputNum > HexedHelper.MySocket.player.troop_count) {
                swal.showInputError("Cannot send more than we have!");     
                return false  
              }

              // Close the prompt window
              swal.close();

              // Send the attack request to server
              HexedHelper.MySocket.SendAddTroopsFromHexRequest(menuParent, inputNum);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);

              // swal("Nice!", "You wrote: " + inputVal, "success");
              
            }.bind(this)
          );

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_withdraw_item: function(){
        if(IsMenuEnabled()){
          self.ShowSweetPromptWindow("Prepare Armed Units", 
            "How many troops do you want to remove from this hex and add to your armed units?", 
            "Leave at least 1 troop...", 
            function(inputVal){
              // If this is not a anumber
              if(! /^\d+$/.test(inputVal)) {
                swal.showInputError("Please enter a number.");     
                return false  
              }
              var menuParent = this.menuParent;
              var inputNum = parseInt(inputVal);
              if(inputNum >= HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count) {
                swal.showInputError("Please leave at least 1 remaining troop.");     
                return false  
              }

              // Close the prompt window
              swal.close();

              // Send the attack request to server
              HexedHelper.MySocket.SendRemoveTroopsFromHexRequest(menuParent, inputNum);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);

              // swal("Nice!", "You wrote: " + inputVal, "success");
              
            }.bind(this)
          );

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_troops_menu_recruit_item: function(){
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Recruit Troops",   
              text: '<p>This is your boot camp, where you can spend the day to recruit new troops. The amount of troops you gain can later be upgraded.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" /> +1</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Do it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendRecruitTroopsForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_appleyard_item: function(){
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Apple Yard",   
              text: '<p>Would you like to build a new Apply Yard here? Each Apple Yard will produce a bit of food over time.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/shiny-apple.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" /> 2</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("The Apple Yard requires at least 2 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildAppleYardForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_woodcutter_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Woodcutter",   
              text: '<p>Would you like to build a new Woodcutter here? Each woodcutter will process wood over time.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" /> 2</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("The Woodcutter requires at least 2 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildWoodCutterForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_stonecutter_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Stonecutter",   
              text: '<p>Would you like to build a new Stonecutter here? Each Stonecutter will gather stones as material over time.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/stone-block.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" />2</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> 10</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("The Stonecutter requires at least 2 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 2) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }else if(HexedHelper.MySocket.player.wood < 10) {
                swal.showInputError("This building requires 10 wood.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildStoneCutterForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_ironsmelter_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build the Iron Smelter",   
              text: '<p>You can build an Iron Smelter here. Each Iron Smelter will turn iron ore into usable iron bars.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/metal-bar.svg" /> +1/min</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" />4</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> 30</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/stone-block.svg" /> 20</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 4) {
                swal.showInputError("The Iron Smelter requires at least 4 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count <= 4) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }else if(HexedHelper.MySocket.player.wood < 30) {
                swal.showInputError("This building requires 30 wood.");     
                return false;
              }else if(HexedHelper.MySocket.player.stone < 20) {
                swal.showInputError("This building requires 20 stone.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildIronSmelterForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Create_build_menu_watchtower_item: function() {
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Build a Watch Tower",   
              text: '<p>This is your regular Lookout Tower. It provides you with an increase range of vision from this hex.</p><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/guards.svg" />3</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/log.svg" /> 20</h2><h2><img class="responsive-img hex-menu-icon-small" src="/hexed_images/stone-block.svg" /> 10</h2>',
              html: true,   
              customClass: "hex-menu-swal",
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Build it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // If there is not enough available troops on this hex
              if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count < 3) {
                swal.showInputError("The Watch Tower requires at least 3 workers.");     
                return false;
              }else if(HexedHelper.hexedGridSnapshot[menuParent.r][menuParent.c].troop_count <= 3) {
                swal.showInputError("You need 1 worker left to defend this hex.");     
                return false;
              }else if(HexedHelper.MySocket.player.wood < 20) {
                swal.showInputError("This building requires 20 wood.");     
                return false;
              }else if(HexedHelper.MySocket.player.stone < 10) {
                swal.showInputError("This building requires 10 stone.");     
                return false;
              }

              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendBuildLookoutTowerForHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });

        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    },

    /* 
     * Functions for foreign hexes menu
    */
    ForeignHexes : {
      Click_attack_menu_item : function(){
        if(IsMenuEnabled()){

          // linje 191..
          self.ShowSweetPromptWindow("Attack", 
            "How many troops will you send into battle?", 
            "Give us your command!", 
            function(inputVal){
              // If this is not a anumber
              if(! /^\d+$/.test(inputVal)) {
                swal.showInputError("Please enter a number.");     
                return false  
              }
              var inputNum = parseInt(inputVal);
              if(inputNum <= 0) {
                swal.showInputError("Cannot send less than 1 troop.");     
                return false  
              }
              if(inputNum > HexedHelper.MySocket.player.troop_count) {
                swal.showInputError("Cannot send more troops than we have!");     
                return false  
              }

              // Close the prompt window
              swal.close();
              var menuParent = this.menuParent;

              // Send the attack request to server
              HexedHelper.MySocket.SendAttackHexRequest(menuParent, inputNum);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);

              // swal("Nice!", "You wrote: " + inputVal, "success");
              
            }.bind(this)
          );

          // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Click_oracle_menu_item : function(){
        if(IsMenuEnabled()){
          var menuParent = this.menuParent;

          swal({   
              title: "Oracles",   
              text: "Oracles will spend the day to gather intel about this hex",   
              showCancelButton: true,   
              confirmButtonColor: "#DD6B55",   
              confirmButtonText: "Do it!",   
              closeOnConfirm: false 
            }, 
            function(){
              // Close the prompt window
              swal.close();

              // Send the oracle request to server
              HexedHelper.MySocket.SendClientOracleHexRequest(menuParent);

              // Close the menu
              CloseHexedMenu();

              // Add loading icon to the hex
              HexedHelper.HTMLGenerator.AddIconToHex(menuParent, "fa-spinner", true);
            });
          
        // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    },
    /* 
     * Functions for hexes menu
    */
    NotUsedHexes : {
      Click_attack_menu_item : function(){
        if(IsMenuEnabled()){

          alert('I am the attack menu for opponents!');

          // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      },
      Click_oracle_menu_item : function(){
        if(IsMenuEnabled()){

          alert('I am the oracle menu for opponents!');

          // Got to figure out what to do instead..
        }else{
          console.log('Dev: Please wait for animation to finish or server to respond..')
        }
      }
    }
  };


  var text_controlled_hexes = document.getElementById("stats_hexes");
  var text_available_units = document.getElementById("stats_units");
  var text_food = document.getElementById("stats_food");
  var text_wood = document.getElementById("stats_wood");
  var text_stone = document.getElementById("stats_stone");
  var text_iron = document.getElementById("stats_iron");
  var text_food_consumption = document.getElementById("stats_food_consumption");
  var text_day = document.getElementById("stats_days");
  self.UpdateStatsUIData = function (statsObj) {
    // Do controlled hexes stuff here..
    // hvis jeg altså vil have det med? Måske andet data er bedre
    // 
    // statsObj {
    //  troop_count: xx,
    //  this_turn: turns.length,
    // }
    if(text_available_units.innerHTML != statsObj.troop_count) {
      text_available_units.innerHTML = statsObj.troop_count;
    }
    if(text_food.innerHTML != statsObj.player_food) {
      text_food.innerHTML = statsObj.player_food;
    }
    if(text_wood.innerHTML != statsObj.player_wood) {
      text_wood.innerHTML = statsObj.player_wood;
    }
    if(text_stone.innerHTML != statsObj.player_stone) {
      text_stone.innerHTML = statsObj.player_stone;
    }
    if(text_iron.innerHTML != statsObj.player_iron) {
      text_iron.innerHTML = statsObj.player_iron;
    }
    // if(text_food_consumption.innerHTML != statsObj.this_turn) {
    //   text_day.innerHTML = statsObj.this_turn;
    // }
    if(text_day.innerHTML != statsObj.this_turn) {
      text_day.innerHTML = statsObj.this_turn;
    }
  }

  var PrepareMenuStructure = function(el) {
    // Check my snapshot for info
    var snapHex = HexedHelper.hexedGridSnapshot[el.r][el.c];

    // If we have any information about this hex
    if(snapHex) {
      // If this is the players own hex
      if (snapHex.owner_id == HexedHelper.MySocket.player.player_id) {
        // Set the menu with build(1)
        return MenuArrays.OwnHexDefault;

      // If this hex is known to be owned by neutral 
      }else if(snapHex.owner_id == -100){
        // Set the menu with attack(2) and oracle(3)
        return MenuArrays.ForeignHexDefault;

      // Anything else (IE. some other player owns it)
      }else{
        // Set the menu with attack(2) and oracle(3)
        return MenuArrays.ForeignHexDefault;
      }

    // If we have no information about this hex
    }else{
      // Set the menu with attack(2) and oracle(3)
      return MenuArrays.ForeignHexDefault;
    }

    // Fallback to just targetting the clicked hex
    return [0];
  };

  var MenuArrays = {
    OwnHexDefault: [0, 1, 4],
    OwnHexTroopsDefault: [9, 6, 7, 8, 5],
    OwnHexBuildingsDefault: [10, 11, 15, 5],
    OwnHexBuildingsWithWood: [10, 11, 12, 15, 5],
    OwnHexBuildingsWithStone: [10, 11, 13, 15, 5],
    OwnHexBuildingsWithIron: [10, 11, 14, 15, 5],
    ForeignHexDefault: [0, 2, 3]
  };

  var OpenHexedMenu = function (el, menuArray, delay){
    var arr = HexedHelper.ReturnSurroundingHexes();
    var totalDelay = 0;
    
    for (var i = 0; i < menuArray.length; i++) {

      // Get a hold of the hex
      var theHex = arr[i].apply(HexedHelper, [el]);

      // Add color and spin effect
      ChangeHexToMenu(theHex, totalDelay, menuArray[i], el);
      
      totalDelay += delay;
      
    }
    
    // Disable alle andre hexes her
    HexedHelper.DisableAllHexesExceptClass('hex-menu');
  };

  var ChangeHexToMenu = function(hex, delay, menuType, parentHex) {
    HexedHelper.HTMLGenerator.RemoveHTMLRecursively(hex);
    HexedHelper.AddClass(hex, 'hex-menu');

    HexedHelper.DelayedAddClass(hex, delay, 'hex-spin', function() {
      // If this is the main hex that was clicked
      if(hex.r == parentHex.r && hex.c == parentHex.c && !hex.classList.contains('hex-border')) {
        // Set this hex up with menu stuff
        HexedHelper.HexedMenuHelper.CreateMenuArray[menuType](hex);
        hex.menuParent = parentHex;

      // If this is one of the actual menu tabs
      }else{
        // Change the hex's colour to blue
        HexedHelper.DelayedAddClass(hex, 250, 'hex-blue', function(){
          // Set this hex up with menu stuff
          HexedHelper.HexedMenuHelper.CreateMenuArray[menuType](hex);
          hex.menuParent = parentHex;
        });
      }
    });
  };

  self.CloseHexedMenuCaller = function() {
    CloseHexedMenu();
  };

  var CloseHexedMenu = function (){
    var menuHexes = Array.prototype.slice.call(HexedHelper.hexedGrid.getElementsByClassName('hex-menu'));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridTop.getElementsByClassName('hex-menu')));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridBottom.getElementsByClassName('hex-menu')));

    for(var i = 0; i < menuHexes.length; i++){
      // console.dir(menuHexes.length);
      HexedHelper.DelayedRemoveClass(menuHexes[i], 250, 'hex-menu');

      HexedHelper.AddClass(menuHexes[i], 'hex-spin');
      HexedHelper.DelayedRemoveClass(menuHexes[i], 250, 'hex-blue');

      // (function(index){
      //   menuHexes[index].menuParent = null;
      // })(i);

      HexedHelper.HTMLGenerator.RemoveHTMLRecursively(menuHexes[i]);

      menuHexes[i].menuParent = null;
      if(! menuHexes[i].classList.contains('hex-border')) {
        AddResourceIconToHex(menuHexes[i]);
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
      }else{
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], HexedHelper.HexedMenuHelper.MenuEventHandlers.BorderHexClickBehaviour);
      }
    }
    HexedHelper.EnableAllHexes();
  };

  var AddResourceIconToHex = function(theHex) {
    var snapHex = HexedHelper.hexedGridSnapshot[theHex.r][theHex.c];
    var iconStr = HexedHelper.ReturnProperHexIcon(snapHex);
    if(iconStr !== "") {
      HexedHelper.HTMLGenerator.AddIconToHex(theHex, iconStr);
    }
  };

  var PrepareHexedMenuForSubMenu = function() {
    console.log('preppp');
    var menuHexes = Array.prototype.slice.call(HexedHelper.hexedGrid.getElementsByClassName('hex-menu'));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridTop.getElementsByClassName('hex-menu')));
    menuHexes = menuHexes.concat(Array.prototype.slice.call(HexedHelper.hexedGridBottom.getElementsByClassName('hex-menu')));

    for(var i = 0; i < menuHexes.length; i++){
      // console.dir(menuHexes.length);
      HexedHelper.RemoveClass(menuHexes[i], 'hex-menu');

      HexedHelper.AddClass(menuHexes[i], 'hex-spin');
      HexedHelper.DelayedRemoveClass(menuHexes[i], 250, 'hex-blue');

      (function(index){
        HexedHelper.HTMLGenerator.RemoveHTMLRecursively(menuHexes[index]);
        AddResourceIconToHex(menuHexes[index]);
      })(i);

      if(! menuHexes[i].classList.contains('hex-border')) {
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
      }else{
        HexedHelper.ChangeClickListenerForHex(menuHexes[i], function(){});
      }
    }
  };

  var IsMenuEnabled = function() {
    if(HexedHelper.animatingHexes === 0 && HexedHelper.waitingForServerTurnExecution === false) {
      return true;
    }else{
      return false
    }
  };

  self.ShowSweetPromptWindow = function(title, text, placeholder, callback) {
    swal(
      {
        title: title,   
        text: text,   
        type: "input",  
        customClass: "hex-menu-swal", 
        showCancelButton: true,   
        closeOnConfirm: false,   
        animation: "slide-from-top",   
        inputPlaceholder: placeholder 
      }, 
      function(inputValue){
        if (inputValue === false) return false; 
        if (inputValue === "") {     
          swal.showInputError("Input can not be empty!");     
          return false   
        }    
        if(callback) {
          callback(inputValue);
        }
      }
    );
  };
};


module.exports = HexedMenuHelper;
},{}],4:[function(require,module,exports){

var SocketHelper = function(HexedHelper) {
	var self = this;

	self.player = {
		player_id : null,
		troop_count : null,
		new_turn: null,
        food: null,
        wood: null,
        stone: null
	};

	self.socket = io.connect('http://87.62.245.186:3000'); // localhost:3000
    // self.socket.clickedHex = null;
    self.socket.on('ClientConnectResponse', function(message){
        console.dir(message);
    });
    self.socket.on('JoinTheActionResponse', function(message){
        var msgObj = JSON.parse(message);
        // HexedHelper.HTMLGenerator.GenerateHexMap(msgObj.hexMap.columns, msgObj.hexMap.rows, msgObj.player.hexedGridSnapshot);

        HexedHelper.knownColumns = msgObj.hexMap.columns;
        HexedHelper.knownRows = msgObj.hexMap.rows;
        HexedHelper.rowOffset = msgObj.hexMap.row_offset;

        self.player.player_id = msgObj.player.player_id;
        self.player.troop_count = msgObj.player.troop_count;
        self.player.new_turn = msgObj.player.new_turn;
        self.player.food = msgObj.player.food;
        self.player.wood = msgObj.player.wood;
        self.player.stone = msgObj.player.stone;
        self.player.iron = msgObj.player.iron;

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
            troop_count: self.player.troop_count,
            this_turn: self.player.new_turn,
            player_food: self.player.food,
            player_wood: self.player.wood,
            player_stone: self.player.stone,
            player_iron: self.player.iron

        });

        HexedHelper.UpdateHexGridFromNewSnapshot(msgObj.player.hexedGridSnapshot);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    self.socket.on('ClientTurnExecutedResponse', function(message){
       	var msgObj = JSON.parse(message);

        HexedHelper.rowOffset = (msgObj.mainAffectedHex) ? msgObj.mainAffectedHex.row_offset : HexedHelper.rowOffset;

        self.player.troop_count = msgObj.player.troop_count;
        self.player.new_turn = msgObj.player.new_turn;
        self.player.food = msgObj.player.food;
        self.player.wood = msgObj.player.wood;
        self.player.stone = msgObj.player.stone;

        // Find the hex
        console.log("The new grid is...::");
        console.dir(msgObj.hexGridData);

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
        	troop_count: self.player.troop_count,
        	this_turn: self.player.new_turn,
            player_food: self.player.food,
            player_wood: self.player.wood,
            player_stone: self.player.stone,
            player_iron: self.player.iron
        });

        // Remove the spinner(s) if any are present
        var bbb = HexedHelper.hexedGrid.getElementsByClassName("fa-spinner");
        for (var i = 0; i < bbb.length; i++) {
            HexedHelper.HTMLGenerator.RemoveHTMLRecursively(bbb[i].parentElement);
        }

        HexedHelper.UpdateHexGridFromNewSnapshot(msgObj.hexGridData);

    	// Enable input stuff
    	HexedHelper.waitingForServerTurnExecution = false;

    });
    self.socket.on('ResourceUpdateNotification', function(message){
        var msgObj = JSON.parse(message);

        self.player.food = msgObj.resources.food;
        self.player.wood = msgObj.resources.wood;
        self.player.stone = msgObj.resources.stone;
        self.player.iron = msgObj.resources.iron;

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
            troop_count: self.player.troop_count,
            this_turn: self.player.new_turn,
            player_food: self.player.food,
            player_wood: self.player.wood,
            player_stone: self.player.stone,
            player_iron: self.player.iron
        });
    });
    


    self.SendJoinTheActionRequest = function() {
        var message = JSON.stringify({});
        self.socket.emit('JoinTheActionRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    }
    self.SendAttackHexRequest = function(theHex, troops){
    	var message = JSON.stringify({
	      hex : {
	        r: theHex.r,
	        c: theHex.c
	      },
	      troop_count: troops
	    });
        console.log({
            r: theHex.r,
            c: theHex.c
          });
        self.socket.emit('ClientAttackHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendClientOracleHexRequest = function(theHex){
    	var message = JSON.stringify({
	      hex : {
	        r: theHex.r,
	        c: theHex.c
	      }
	    });

        self.socket.emit('ClientOracleHexRequest', message);

    	// Disable input stuff
    	HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendRecruitTroopsForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('RecruitTroopsForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendRemoveTroopsFromHexRequest = function(theHex, amount) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            },
            amount: amount
        });
        self.socket.emit('RemoveTroopsFromHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendAddTroopsFromHexRequest = function(theHex, amount) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            },
            amount: amount
        });
        self.socket.emit('AddTroopsFromHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildAppleYardForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildAppleYardForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildWoodCutterForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildWoodCutterForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildStoneCutterForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildStoneCutterForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildIronSmelterForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildIronSmelterForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildLookoutTowerForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildLookoutTowerForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    



    self.socket.on('DebugChangePlayerResponse', function(message){
        console.dir(message);
       	var msgObj = JSON.parse(message);

        self.player.player_id = msgObj.player.player_id;
        self.player.troop_count = msgObj.player.troop_count;
        self.player.new_turn = msgObj.player.new_turn;

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
        	troop_count: self.player.troop_count,
        	this_turn: self.player.new_turn,
            player_food: self.player.food
        });

        document.getElementById("debug_player").innerHTML = self.player.player_id;
        document.getElementById("debug_turns").innerHTML = self.player.new_turn + "/" +msgObj.server.new_turn;

        HexedHelper.UpdateHexGridFromNewSnapshot(msgObj.player.hexedGridSnapshot);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;

        swal("Changed to player #" + msgObj.player.player_id);
    });
    self.socket.on('DebugGetServerHexedGridResponse', function(message){
       	var msgObj = JSON.parse(message);

        console.dir(msgObj);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    self.socket.on('DebugGetServerSnaphotResponse', function(message){
       	var msgObj = JSON.parse(message);

        console.dir(msgObj);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    self.socket.on('DebugGetServerTimelineResponse', function(message){
       	var msgObj = JSON.parse(message);

        console.dir(msgObj);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    

    self.SendDebugChangePlayerRequest = function(player_id){
    	message = JSON.stringify({
    		player_id: player_id
    	});
    	self.socket.emit('DebugChangePlayerMessage', message);

    	// Disable input stuff
    	HexedHelper.waitingForServerTurnExecution = true;

    	// Add popup with loading that cannot be closed
    	// (also make a response logic to close it again)
    };
    self.SendDebugGetServerSnaphot = function(turn){
    	message = JSON.stringify({
    		turn: turn
    	});
    	self.socket.emit('DebugGetServerSnaphot', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendDebugGetServerHexedGrid = function(){
    	message = JSON.stringify({
    	});
    	self.socket.emit('DebugGetServerHexedGrid', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendDebugGetServerTimeline = function(){
    	message = JSON.stringify({
    	});
    	self.socket.emit('DebugGetServerTimeline', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };

};

module.exports = SocketHelper;
},{}],5:[function(require,module,exports){
/***************************
This is JavaScript (JS), the programming language that powers the web (and this is a comment, which you can delete).

To use this file, link it to your markup by placing a <script> in the <body> of your HTML file:

  <body>
    <script src="script.js"></script>

replacing "script.js" with the name of this JS file.

Learn more about JavaScript at

https://developer.mozilla.org/en-US/Learn/JavaScript
***************************/


/*
 *        **** TODO / Tanker **** 
 *

 får ikke fjernet hex-spin, når man skal klikke??

 *   *** Generelt ***
 *
 * Griddet skal nok have 1/2 extra hexes
 * i hver kant som er dækket af noget
 * fog of war lignende..
 *  Dvs. at index 0 skal være dummy hexes
 *    samme for sidste index..
 *    både i rækker og kolonner
 *
 *   *** Animations ***
 *
 * Der er stadig noget bug-værk over de der hex-spin
 *  Skal fjerne timeouts fra alle de hexes som bliver
 *  påvirket når man klikker.
 *
 *   *** Ny idé?? ***
 * Lav flip effekt rundt omkring den hex man klikker
 * med en function (int count) som tæller hvor mange
 * hexes der skal flippes.
 *
 * NEDENSTÅENDE MANGLER STADIG
 *
 * Skrive setTimeout brugen om, sådan at jeg altid
 * fjerner den timeout som er igang, når en ny effekt
 * skal overwrite.
 *  Måske der kommer nogle problemer med flip, hvor
 *  tager udgangspunkt i en vinkel som ikke er lige?
*/
var _hexedHelper = require("./HexedHelper.js");
var HexedHelper = new _hexedHelper();

window.onload = function () {

  var hex = document.getElementsByClassName('hex');

  $('#startup-modal').openModal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    in_duration: 300, // Transition in duration
    out_duration: 200, // Transition out duration
    starting_top: '4%', // Starting top style attribute
    ending_top: '10%', // Ending top style attribute
    ready: function() {}, // Callback for Modal open
    complete: function() { HexedHelper.MySocket.SendJoinTheActionRequest(); } // Callback for Modal close
  });

  // document.getElementById('player0').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.CloseHexedMenuCaller();
  //   HexedHelper.MySocket.SendDebugChangePlayerRequest(0);
  // }, false);
  // document.getElementById('player1').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.CloseHexedMenuCaller();
  //   HexedHelper.MySocket.SendDebugChangePlayerRequest(1);
  // }, false);
  // document.getElementById('player2').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.CloseHexedMenuCaller();
  //   HexedHelper.MySocket.SendDebugChangePlayerRequest(2);
  // }, false);

  // document.getElementById('getHexGrid').addEventListener('click', function () {
  //   HexedHelper.MySocket.SendDebugGetServerHexedGrid();
  // }, false);
  // document.getElementById('getTimeline').addEventListener('click', function () {
  //   HexedHelper.MySocket.SendDebugGetServerTimeline();
  // }, false);
  // document.getElementById('getSnapshot').addEventListener('click', function () {
  //   HexedHelper.HexedMenuHelper.ShowSweetPromptWindow("Get server snapshot", 
  //     "Which turn do you need to check?", 
  //     "Turn number", 
  //     function(inputVal){
  //       // If this is not a anumber
  //       if(! /^\d+$/.test(inputVal)) {
  //         swal.showInputError("Please enter a number.");     
  //         return false  
  //       }
  //       var inputNum = parseInt(inputVal);

  //       // Close the prompt window
  //       swal.close();
        
  //       HexedHelper.MySocket.SendDebugGetServerSnaphot(inputNum);
        
  //     }.bind(this)
  //   );
  // }, false);

  // [].forEach.call(document.querySelectorAll('.hex'), function (el) {

  //   el.addEventListener('mouseover', function () {
  //     if (!el.classList.contains('hex-green')) {
  //       el.classList.add('hex-green');
  //     }
  //   }, false);

  //   el.addEventListener('mouseleave', function () {
  //     if (el.classList.contains('hex-green')) {
  //       el.classList.remove('hex-green');
  //     }
  //   }, false);
    
  //   HexedHelper.AddListenerToHex(el, 'click', HexedHelper.HexedMenuHelper.MenuEventHandlers.DefaultHexClickBehaviour);
    
  //   el.addEventListener('webkitAnimationEnd', function () {
  //     HexedHelper.RemoveClass(el, 'hex-spin');
  //   }, false);
  //   el.addEventListener('animationend', function () {
  //     HexedHelper.RemoveClass(el, 'hex-spin');
  //   }, false);
  // });
};




},{"./HexedHelper.js":2}]},{},[5]);
