
var SocketHelper = function(HexedHelper) {
	var self = this;

	self.player = {
		player_id : null,
		troop_count : null,
		new_turn: null,
        food: null,
        wood: null,
        stone: null
	};

	self.socket = io.connect('http://87.62.245.186:3000'); // localhost:3000
    // self.socket.clickedHex = null;
    self.socket.on('ClientConnectResponse', function(message){
        console.dir(message);
    });
    self.socket.on('JoinTheActionResponse', function(message){
        var msgObj = JSON.parse(message);
        // HexedHelper.HTMLGenerator.GenerateHexMap(msgObj.hexMap.columns, msgObj.hexMap.rows, msgObj.player.hexedGridSnapshot);

        HexedHelper.knownColumns = msgObj.hexMap.columns;
        HexedHelper.knownRows = msgObj.hexMap.rows;
        HexedHelper.rowOffset = msgObj.hexMap.row_offset;

        self.player.player_id = msgObj.player.player_id;
        self.player.troop_count = msgObj.player.troop_count;
        self.player.new_turn = msgObj.player.new_turn;
        self.player.food = msgObj.player.food;
        self.player.wood = msgObj.player.wood;
        self.player.stone = msgObj.player.stone;
        self.player.iron = msgObj.player.iron;

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
            troop_count: self.player.troop_count,
            this_turn: self.player.new_turn,
            player_food: self.player.food,
            player_wood: self.player.wood,
            player_stone: self.player.stone,
            player_iron: self.player.iron

        });

        HexedHelper.UpdateHexGridFromNewSnapshot(msgObj.player.hexedGridSnapshot);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    self.socket.on('ClientTurnExecutedResponse', function(message){
       	var msgObj = JSON.parse(message);

        HexedHelper.rowOffset = (msgObj.mainAffectedHex) ? msgObj.mainAffectedHex.row_offset : HexedHelper.rowOffset;

        self.player.troop_count = msgObj.player.troop_count;
        self.player.new_turn = msgObj.player.new_turn;
        self.player.food = msgObj.player.food;
        self.player.wood = msgObj.player.wood;
        self.player.stone = msgObj.player.stone;

        // Find the hex
        console.log("The new grid is...::");
        console.dir(msgObj.hexGridData);

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
        	troop_count: self.player.troop_count,
        	this_turn: self.player.new_turn,
            player_food: self.player.food,
            player_wood: self.player.wood,
            player_stone: self.player.stone,
            player_iron: self.player.iron
        });

        // Remove the spinner(s) if any are present
        var bbb = HexedHelper.hexedGrid.getElementsByClassName("fa-spinner");
        for (var i = 0; i < bbb.length; i++) {
            HexedHelper.HTMLGenerator.RemoveHTMLRecursively(bbb[i].parentElement);
        }

        HexedHelper.UpdateHexGridFromNewSnapshot(msgObj.hexGridData);

    	// Enable input stuff
    	HexedHelper.waitingForServerTurnExecution = false;

    });
    self.socket.on('ResourceUpdateNotification', function(message){
        var msgObj = JSON.parse(message);

        self.player.food = msgObj.resources.food;
        self.player.wood = msgObj.resources.wood;
        self.player.stone = msgObj.resources.stone;
        self.player.iron = msgObj.resources.iron;

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
            troop_count: self.player.troop_count,
            this_turn: self.player.new_turn,
            player_food: self.player.food,
            player_wood: self.player.wood,
            player_stone: self.player.stone,
            player_iron: self.player.iron
        });
    });
    


    self.SendJoinTheActionRequest = function() {
        var message = JSON.stringify({});
        self.socket.emit('JoinTheActionRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    }
    self.SendAttackHexRequest = function(theHex, troops){
    	var message = JSON.stringify({
	      hex : {
	        r: theHex.r,
	        c: theHex.c
	      },
	      troop_count: troops
	    });
        console.log({
            r: theHex.r,
            c: theHex.c
          });
        self.socket.emit('ClientAttackHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendClientOracleHexRequest = function(theHex){
    	var message = JSON.stringify({
	      hex : {
	        r: theHex.r,
	        c: theHex.c
	      }
	    });

        self.socket.emit('ClientOracleHexRequest', message);

    	// Disable input stuff
    	HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendRecruitTroopsForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('RecruitTroopsForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendRemoveTroopsFromHexRequest = function(theHex, amount) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            },
            amount: amount
        });
        self.socket.emit('RemoveTroopsFromHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendAddTroopsFromHexRequest = function(theHex, amount) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            },
            amount: amount
        });
        self.socket.emit('AddTroopsFromHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildAppleYardForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildAppleYardForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildWoodCutterForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildWoodCutterForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildStoneCutterForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildStoneCutterForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildIronSmelterForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildIronSmelterForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendBuildLookoutTowerForHexRequest = function(theHex) {
        var message = JSON.stringify({
            hex : {
                r: theHex.r,
                c: theHex.c
            }
        });
        self.socket.emit('BuildLookoutTowerForHexRequest', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    



    self.socket.on('DebugChangePlayerResponse', function(message){
        console.dir(message);
       	var msgObj = JSON.parse(message);

        self.player.player_id = msgObj.player.player_id;
        self.player.troop_count = msgObj.player.troop_count;
        self.player.new_turn = msgObj.player.new_turn;

        HexedHelper.HexedMenuHelper.UpdateStatsUIData({
        	troop_count: self.player.troop_count,
        	this_turn: self.player.new_turn,
            player_food: self.player.food
        });

        document.getElementById("debug_player").innerHTML = self.player.player_id;
        document.getElementById("debug_turns").innerHTML = self.player.new_turn + "/" +msgObj.server.new_turn;

        HexedHelper.UpdateHexGridFromNewSnapshot(msgObj.player.hexedGridSnapshot);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;

        swal("Changed to player #" + msgObj.player.player_id);
    });
    self.socket.on('DebugGetServerHexedGridResponse', function(message){
       	var msgObj = JSON.parse(message);

        console.dir(msgObj);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    self.socket.on('DebugGetServerSnaphotResponse', function(message){
       	var msgObj = JSON.parse(message);

        console.dir(msgObj);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    self.socket.on('DebugGetServerTimelineResponse', function(message){
       	var msgObj = JSON.parse(message);

        console.dir(msgObj);

        // Enable input stuff
        HexedHelper.waitingForServerTurnExecution = false;
    });
    

    self.SendDebugChangePlayerRequest = function(player_id){
    	message = JSON.stringify({
    		player_id: player_id
    	});
    	self.socket.emit('DebugChangePlayerMessage', message);

    	// Disable input stuff
    	HexedHelper.waitingForServerTurnExecution = true;

    	// Add popup with loading that cannot be closed
    	// (also make a response logic to close it again)
    };
    self.SendDebugGetServerSnaphot = function(turn){
    	message = JSON.stringify({
    		turn: turn
    	});
    	self.socket.emit('DebugGetServerSnaphot', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendDebugGetServerHexedGrid = function(){
    	message = JSON.stringify({
    	});
    	self.socket.emit('DebugGetServerHexedGrid', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };
    self.SendDebugGetServerTimeline = function(){
    	message = JSON.stringify({
    	});
    	self.socket.emit('DebugGetServerTimeline', message);

        // Disable input stuff
        HexedHelper.waitingForServerTurnExecution = true;
    };

};

module.exports = SocketHelper;